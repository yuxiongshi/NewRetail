//
//  YQDefine.h
//  NewRetail
//
//  Created by yuqin on 2019/6/18.
//  Copyright © 2019 yuqin. All rights reserved.
//

#ifndef YQDefine_h
#define YQDefine_h


//判断iPhoneX，Xs（iPhoneX，iPhoneXs）
#define IS_IPHONE_X ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) && YES : NO)
//判断iPhoneXr
#define IS_IPHONE_Xr ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(828, 1792), [[UIScreen mainScreen] currentMode].size) && YES : NO)
//判断iPhoneXsMax
#define IS_IPHONE_Xs_Max ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1242, 2688), [[UIScreen mainScreen] currentMode].size)&& YES : NO)

//判断iPhoneX所有系列
//判断iphoneX系列  iPhone X/XS/XR/XS Max底部都会有安全距离
//#define HBISIphoneX (IS_IPHONE_X || IS_IPHONE_Xr || IS_IPHONE_Xs_Max)
#define HBISIphoneX \
({BOOL isPhoneX = NO;\
if (@available(iOS 11.0, *)) {\
isPhoneX = [[UIApplication sharedApplication] delegate].window.safeAreaInsets.bottom > 0.0;\
}\
(isPhoneX);})


#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define NaviBarHeight                       44.0f
#define StatusBarHeight                     (HBISIphoneX?44.0f:20.0f)
#define NavigationStatusBarHeight           (HBISIphoneX?88.0f:64.0f)
#define TopOfStatusBar_IPhoneX              (HBISIphoneX?24.0f:0.0f)
#define Botoom_IPhoneX                      (HBISIphoneX?34.0f:0.0f)
#define TableView_Height_IPhoneX            ScreenHeight-NavigationStatusBarHeight-Botoom_IPhoneX


#define YQ_IMAGE(a) [UIImage imageNamed:a]
//得到整形的字符值
#define StringFromInt(x) [NSString stringWithFormat:@"%d",x]
#define StringFromLongInt(x) [NSString stringWithFormat:@"%ld",x]
#define Head_PlaceHolder YQ_IMAGE(@"head_placeHolder")

#endif /* YQDefine_h */
