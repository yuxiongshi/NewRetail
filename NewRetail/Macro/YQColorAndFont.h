//
//  YQColorAndFont.h
//  NewRetail
//
//  Created by yuqin on 2019/6/18.
//  Copyright © 2019 yuqin. All rights reserved.
//

#ifndef YQColorAndFont_h
#define YQColorAndFont_h

// RGB颜色方法
#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define MainBlackColor                      UIColorFromRGB(0x232629)//主题标签黑色
#define MainGrayColor                       UIColorFromRGB(0x707070)//描述文字灰
#define MainBackgroundColor                 UIColorFromRGB(0xeeeeee)//背景灰色
#define MainLabelBlackColor                 UIColorFromRGB(0x232629)//label黑色
#define MainNavBarColor                     UIColorFromRGB(0xffffff)//导航栏白色
#define LineColor                           UIColorFromRGB(0xd1d6e2)//分割线颜色
#define COLOR_Text_Gay                      UIColorFromRGB(0x919191)//文字灰色
#define WhiteColor                          UIColorFromRGB(0xffffff)//白色
#define MainRedTextColor                    UIColorFromRGB(0xE13134)
#define TableViewBackGroundColor            UIColorFromRGB(0xf5f5f5)
#define color_333333                        UIColorFromRGB(0x333333)
#define MainThemeColor                      UIColorFromRGB(0xe95e4d)//主题红
#define COLOR_Text_Blue                     UIColorFromRGB(0x507daf)
#define TabBarHeight                        (HBISIphoneX?82.0f:49.0f)
#define kTheTitleBackgroundColor            UIColorFromRGB(0x808080)
#define MainLabelGrayColor                  UIColorFromRGB(0x747E8C)//label灰色
#define F1LineColor                         UIColorFromRGB(0xf1f1f1)//F1的分割线
#define kColor80                            UIColorFromRGB(0x808080)//80



#define YQ_Font(a) [UIFont systemFontOfSize:a]
#define XCFONT(x) [UIFont systemFontOfSize:x]
#define Font_15 [UIFont systemFontOfSize:15]
#define Font_16 [UIFont systemFontOfSize:16]
#define kDescribeFont FIT(15)
#define kNormalButtonFont FIT(15)


#define kMargin_left FIT(16)
#define kMargin_right FIT(16)
#define kMargin_top FIT(16)
#define kCommitButtonHeight FIT(45)
#define kLabelHeight FIT(40)
#define kCellDescribeLabelFont FIT(15)
#define kSpace FIT(10)
#define kTextFieldFont FIT(15)

//#define kSetFontButton  [UIFont systemFontOfSize:FIT(15)]



#endif /* YQColorAndFont_h */
