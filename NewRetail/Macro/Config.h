//
//  Config.h
//  NewRetail
//
//  Created by yuqin on 2019/6/18.
//  Copyright © 2019 yuqin. All rights reserved.
//

#ifndef Config_h
#define Config_h

#define UIScreenWidth CGRectGetWidth([[UIScreen mainScreen] bounds])
#define PERCENTW_ADAPTATION  UIScreenWidth/414.0f
#define FIT(f)  f*PERCENTW_ADAPTATION
#define FIT2(f)  (f/2)*PERCENTW_ADAPTATION
#define FIT3(f)  (f/3)*PERCENTW_ADAPTATION

#define KUserSingleton [UserInfo sharedUserInfo]
#define Rect(x, y, w, h)                    CGRectMake(x, y, w, h)
#define isStringNull(x)       (!x || [x isKindOfClass:[NSNull class]]||x.length == 0||x == nil)
#define isEmptyString(x)      (isStringNull(x) || [x isEqual:@""] || [x isEqual:@"(null)"] || [x isEqual:@"[null]"] || [x isEqual:@"null"] || [x isEqual:@"<null>"])

#define isEmptyObject(object)  [NSObject isNullOrNilWithObject:object]
#define YQ_joiningTogetherUrl(a) [NSString stringWithFormat:@"%@%@",BaseHttpUrl,a]
#define YQ_PUSH(vc) [self.navigationController pushViewController:vc animated:YES]


#define isEmptyArray(array) (array == nil || [array isKindOfClass:[NSNull class]] || [array count] == 0)
//#define isEmptyDict(dict) [dict isKindOfClass:[NSNull class]] || [dict isEqual:[NSNull null]] || dict == nil
#define isEmptyDict(dict) [dict isKindOfClass:[NSNull class]] || [dict isEqual:[NSNull null]]
#define kDefaults [NSUserDefaults standardUserDefaults]
#define CommitButtonImg YQ_IMAGE(@"btnImg")
#define PlaceHolder YQ_IMAGE(@"placeHolder")


#define TimerInvalidateNoti  @"TimerInvalidateNoti"
#define InfoModelShare [YQPersonInformationModel sharedYQPersonInformationModel]

//改名称通知
#define ModifyName @"ModifyName"


//app（程序版本号）
#define app_Version [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]
//app名称
#define app_Name [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"]
//app包名
#define app_BundleId [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"]

// 定义宏
#define WeakSelf(o)  __weak typeof(o) weakSelf = o;
#define FormatString(...) [NSString stringWithFormat:__VA_ARGS__]


#define DEFINE_SINGLETON_FOR_HEADER(className) \
\
+ (className *)shared##className;

#define DEFINE_SINGLETON_FOR_CLASS(className) \
\
+ (className *)shared##className { \
static className *shared##className = nil; \
static dispatch_once_t onceToken; \
dispatch_once(&onceToken, ^{ \
shared##className = [[self alloc] init]; \
}); \
return shared##className; \
}

#pragma mark ----------------- noDataView -----------
#define YQ_NoDataImg(tableView) tableView.emptyDataSetSource = self;\
tableView.emptyDataSetDelegate = self\

#define NoDataView_Nor @"noDataImg_nor"
#define NoDataView_order @"noDataImg_order"

#define YQ_ImageForEmptyDataSet(a,image) - (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView {\
return [UIImage imageNamed:image];\
}\
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView {\
NSString *title = a;\
NSDictionary *attributes = @{\
NSFontAttributeName:[UIFont boldSystemFontOfSize:15.0f],\
NSForegroundColorAttributeName:[UIColor darkGrayColor]\
};\
return [[NSAttributedString alloc] initWithString:title attributes:attributes];\
}\
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView\
{\
    return YES;\
}\


#define weakify(var) \
try {} @catch (...) {} \
__weak __typeof__(var) var ## _weak = var

#define strongify(var) \
try {} @catch (...) {} \
__strong __typeof__(var) var = var ## _weak


//设备版本号
#define DeviceValue [[[UIDevice currentDevice] systemVersion] floatValue]

#ifdef DEBUG
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#define DLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
//#define DLog(...)
#endif

#endif /* Config_h */
