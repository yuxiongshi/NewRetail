//
//  AppDelegate.h
//  NewRetail
//
//  Created by yuqin on 2019/6/18.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarController.h"

#define TheApp           ([UIApplication sharedApplication])
#define TheAppDel        ((AppDelegate*)TheApp.delegate)

#define BaseHttpUrl      TheAppDel.requestUrl
#define DevBaseHttpUrl @"http://192.168.1.55:8952/"

//测试环境
#define TestBaseHttpUrl @"http://192.168.1.55:8952/"
//本地环境
#define UatBaseHttpUrl @"http://192.168.1.101/"
//生产环境
#define ProductBaseHttpUrl @"https://app.coinwin.vip:8443/"


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) TabBarController* rootTabBarController;
@property (nonatomic, copy) NSString* requestUrl;


@end

