//
//  AppDelegate.m
//  NewRetail
//
//  Created by yuqin on 2019/6/18.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "AppDelegate.h"
#import "YQMallVC.h"
#import "GFNavigationController.h"
#import "TabBarController.h"
#import "YQLoginVC.h"
#import "YQStartFigureVC.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    self.requestUrl = DevBaseHttpUrl;
    //先检测版本版本更新
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"app/version") appendParameters:@{@"type":@"2"} successBlock:^(id responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            //转成数值
            //在线上的
            NSArray * lineArr = [responseObject[@"data"][@"version"] componentsSeparatedByString:@"."];
            //本地的版本
            NSArray *localArr = [app_Version componentsSeparatedByString:@"."];
            NSInteger currentVersionInt = 0;
            if (localArr.count == 3)//默认版本号1.0.0类型
            {
                currentVersionInt = [localArr[0] integerValue]*100 + [localArr[1] integerValue]*10 + [localArr[2] integerValue];
            }
            
            NSInteger lineVersionInt = 0;
            if (lineArr.count == 3)//默认版本号1.0.0类型
            {
                lineVersionInt = [lineArr[0] integerValue]*100 + [lineArr[1] integerValue]*10 + [lineArr[2] integerValue];
            }
            
            
            if (lineVersionInt > currentVersionInt) {
                //版本更新
                //跳转到下载
                [JXTAlertView showAlertViewWithTitle:@"温馨提示" message:@"是否更新版本" cancelButtonTitle:@"否" otherButtonTitle:@"去更新" cancelButtonBlock:^(NSInteger buttonIndex) {
                    
                } otherButtonBlock:^(NSInteger buttonIndex) {
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms-services://?action=download-manifest&url=https%3A%2F%2Fwww.dibaqu.com%2Fshow%2Fplist%2F401266695257194496%3F1564037440"]];
                }];
            }else {
//                [LCProgressHUD showFailure:responseObject[@"msg"]];
            }
        }
    } failureBlock:^(NSError *error) {
        
    }];
    
    //启动留白问题
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    YQStartFigureVC *vc = [[YQStartFigureVC alloc] init];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
