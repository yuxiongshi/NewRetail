//
//  SZSCPropertyModel.m
//  GBKGame
//
//  Created by fanhongbin on 2018/6/12.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "HBBaseListModel.h"

@implementation HBBaseListModel

+(HBBaseListModel *)modelWithJson:(id)json{
    HBBaseListModel * model = [[HBBaseListModel alloc]init];
    if ([json isKindOfClass:[NSDictionary class]]) {
        [model mj_setKeyValues:json];
        id object=json[@"data"];
        if ([object isKindOfClass:[NSArray class]] ) {
            model.dataArr=object;
        }else{
            model.data=object;
        }
    }else{
        model.msg = @"数据格式有误";
    }
    return model;
}
@end

