//
//  SZSCPropertyModel.h
//  GBKGame
//
//  Created by fanhongbin on 2018/6/12.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "HBBaseModel.h"

@interface HBBaseListModel : HBBaseModel

@property (nonatomic,strong) NSMutableArray* dataList;
@property (nonatomic,assign) NSInteger totalPage;
@property (nonatomic,assign) NSInteger pageCurrent;

+(HBBaseListModel *)modelWithJson:(id)json;
@end


