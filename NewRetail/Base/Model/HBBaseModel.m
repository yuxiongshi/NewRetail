//
//  HBBaseModel.m
//  GBKGame
//
//  Created by fanhongbin on 2018/6/13.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "HBBaseModel.h"

@implementation HBBaseModel

+ (BOOL)propertyIsOptional:(NSString *)propertyName
{
    return YES;
}

-(void)setValue:(id)value forUndefinedKey:(NSString *)key{
    
}

+(HBBaseModel *)modelWithJson:(id)json{
    HBBaseModel * model = [[HBBaseModel alloc]init];
    if ([json isKindOfClass:[NSDictionary class]]) {
        [model mj_setKeyValues:json];
        id object=json[@"data"];
        if ([object isKindOfClass:[NSArray class]] ) {
            model.dataArr=object;
        }else{
            model.data=object;
        }
    }else{
        model.msg = @"数据格式有误";
    }
    return model;
}
@end
