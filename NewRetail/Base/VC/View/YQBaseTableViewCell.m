//
//  YQBaseTableViewCell.m
//  NewRetail
//
//  Created by yuqin on 2019/6/21.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseTableViewCell.h"

@implementation YQBaseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupTableViewCellView];
    }
    return self;
}

- (void)setupTableViewCellView {
    
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
