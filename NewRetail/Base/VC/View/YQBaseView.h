//
//  YQBaseView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQBaseView : UIView

- (void)setupView;

@end

NS_ASSUME_NONNULL_END
