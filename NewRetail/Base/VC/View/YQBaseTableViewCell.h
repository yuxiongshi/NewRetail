//
//  YQBaseTableViewCell.h
//  NewRetail
//
//  Created by yuqin on 2019/6/21.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQBaseTableViewCell : UITableViewCell

- (void)setupTableViewCellView;

@end

NS_ASSUME_NONNULL_END
