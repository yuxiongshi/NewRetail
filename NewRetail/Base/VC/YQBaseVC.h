//
//  YQBaseVC.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQBaseVC : UIViewController

@property (nonatomic, strong) UIImageView *headView;
@property (nonatomic,strong) UIButton *btnLeft;
@property (nonatomic,strong) UIButton *btnRight;

//点击右边按钮跳转页面
- (void)pushAction;

@end

NS_ASSUME_NONNULL_END
