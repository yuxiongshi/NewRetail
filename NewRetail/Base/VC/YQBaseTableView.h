//
//  YQBaseTableView.h
//  NewRetail
//
//  Created by yuqin on 2019/6/28.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQBaseTableView : YQBaseVC

@property (nonatomic, strong) UITableView *tableView;

@end

NS_ASSUME_NONNULL_END
