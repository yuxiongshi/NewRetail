//
//  YQCustomCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQCustomCell : UITableViewCell

- (void)setupTheView;

@end

NS_ASSUME_NONNULL_END
