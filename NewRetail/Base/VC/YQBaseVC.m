//
//  YQBaseVC.m
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"

@interface YQBaseVC ()

@end

@implementation YQBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = MainBackgroundColor;
    [self setupUI];
}

- (void)setupUI {
    
    [self.view addSubview:self.headView];
    [self.headView addSubview:self.btnLeft];
    [self.headView addSubview:self.btnRight];
    
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.mas_equalTo(0);
        make.height.mas_equalTo(NavigationStatusBarHeight);
    }];
    
    [self.btnLeft mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.bottom.mas_equalTo(-FIT(16));
        make.size.mas_equalTo(CGSizeMake(FIT(200), FIT(21)));
    }];
    
    [self.btnRight mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(16));
        make.size.mas_equalTo(CGSizeMake(FIT(200), FIT(21)));
    }];
    
}

- (UIImageView *)headView {
    if (_headView == nil) {
        _headView = [[UIImageView alloc] initWithFrame:Rect(0, 0,ScreenWidth,NavigationStatusBarHeight)];
        _headView.image = YQ_IMAGE(@"navgationBar");
        _headView.userInteractionEnabled = YES;
    }
    return _headView;
}

- (UIButton *)btnLeft {
    if (!_btnLeft) {
        _btnLeft = [YQViewFactory buttonWithTitle:@"" image:YQ_IMAGE(@"back") titleColor:WhiteColor fontSize:FIT(19) userBold:YES target:self sel:@selector(backAction)];
        [_btnLeft setIconInLeftWithSpacing:FIT(10)];
        _btnLeft.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _btnLeft;
}

- (UIButton *)btnRight {
    if (!_btnRight) {
        _btnRight = [YQViewFactory buttonWithTitle:@"" image:YQ_IMAGE(@"") titleColor:WhiteColor fontSize:FIT(15) userBold:NO target:self sel:@selector(pushAction)];
        [_btnRight setHidden:YES];
        _btnRight.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _btnRight;
}


- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushAction {
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self setNeedsStatusBarAppearanceUpdate];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:YES];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc {
    NSLog(@"走咯");
}

@end
