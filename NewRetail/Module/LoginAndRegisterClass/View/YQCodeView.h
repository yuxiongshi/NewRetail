//
//  YQCodeView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQCodeView : YQBaseView

/**
 输入验证码标题
 */
@property (nonatomic, strong) UILabel *theTitleOne;

/**
 显示号码
 */
@property (nonatomic, strong) UILabel *teleNumLab;

/**
 输入框
 */
@property (nonatomic, strong) UITextField *codeTF;

/**
 倒计时
 */
@property (nonatomic, strong) UIButton *cutDownBtn;

/**
  分割线
 */
@property (nonatomic, strong) UIView *bottomLineView;

/**
 登录按钮
 */
@property (nonatomic, strong) UIButton *loginBtn;

/**
 描述
 */
@property (nonatomic, strong) UILabel *theTitleTwo;

/**
 登录或者注册
 */
@property (nonatomic, copy) void (^LoginOrRegisterBlock) (void);

/**
 再次发送
 */
@property (nonatomic, copy) void (^SendCodeBlock) (void);

@end

NS_ASSUME_NONNULL_END
