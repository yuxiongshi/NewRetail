//
//  YQNewLoginView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQNewLoginView : YQBaseView

/**
 底部logo
 */
@property (nonatomic, strong) UILabel *bottomLab;

/**
 密码登录
 */
@property (nonatomic, strong) UIButton *pwdLoginBtn;

/**
 获取验证码
 */
@property (nonatomic, strong) UIButton *loadCodeBtn;

/**
 描述
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 分割线
 */
@property (nonatomic, strong) UIView *bottomLineView;

/**
 左边按钮
 */
@property (nonatomic, strong) UIButton *leftCountryBtn;

/**
 输入框
 */
@property (nonatomic, strong) UITextField *teleTF;

/**
 抬头
 */
@property (nonatomic, strong) UILabel *theTitleOneLab;

@property (nonatomic, copy) void (^LoadCodeBlock) (void);//获取验证码

@property (nonatomic, copy) void (^PwdLoginBlock) (void);//点击密码登录

@end

NS_ASSUME_NONNULL_END
