//
//  YQRegisterView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQRegisterView.h"

@interface YQRegisterView ()<UITextFieldDelegate>

@end

@implementation YQRegisterView

- (void)setupView {
    
    [self addSubview:self.theTitleLab];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.top.mas_equalTo(FIT(108));
    }];
    NSArray *titleArr = @[@"用户名",@"6-16位字母+数字的组合",@"邀请码"];
    for (NSInteger i = 0; i < 3; i++) {
        UITextField *threeTF = [YQViewFactory textFieldWithPlaceholderText:titleArr[i] textColor:MainBlackColor fontSize:FIT(16) userBold:NO];
        //设置样式 以及键盘样式
        threeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:threeTF];
        threeTF.delegate = self;
        [threeTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(FIT(45));
            make.right.mas_equalTo(-FIT(45));
            make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(57)*i+FIT(90));
            make.height.mas_equalTo(FIT(17));
        }];
        if (i == 0) {
            self.nameTF = threeTF;
        }else if (i == 1) {
            self.pwdTF = threeTF;
            [self.pwdTF mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(-FIT(65));
            }];
            
            UIButton *rightBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"hide") target:self sel:@selector(hodeOrShowPwd:)];
            [rightBtn setImage:YQ_IMAGE(@"show") forState:UIControlStateSelected];
            [self addSubview:rightBtn];
            [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.pwdTF.mas_right).mas_equalTo(0);
                make.centerY.mas_equalTo(self.pwdTF.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(FIT(18), FIT(10)));
            }];
            self.pwdTF.secureTextEntry = YES;
        }else {
            self.invitationCodeTF = threeTF;
        }
        
        //分割线
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = F1LineColor;
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(FIT(45));
            make.right.mas_equalTo(-FIT(45));
            make.top.equalTo(threeTF.mas_bottom).mas_equalTo(FIT(15));
            make.height.mas_equalTo(1);
        }];
    }
    
    [self addSubview:self.loginBtn];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.right.mas_equalTo(-FIT(45));
        make.bottom.mas_equalTo(-FIT(64));
        make.height.mas_equalTo(kCommitButtonHeight);
    }];
    
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(24) userBold:YES];
        _theTitleLab.text = @"设置用户名与密码";
    }
    return _theTitleLab;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [YQViewFactory buttonWithTitle:@"登录" titleColor:WhiteColor fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(loginAction)];
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"commit_nor") forState:UIControlStateNormal];
        _loginBtn.enabled = NO;
        
    }
    return _loginBtn;
}

//点击隐藏或者显示
- (void)hodeOrShowPwd:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        //默认隐藏 点击显示
        self.pwdTF.secureTextEntry = NO;
    }else {
        self.pwdTF.secureTextEntry = YES;
    }
}

- (void)loginAction {
    //登录
    //判断密码是否符合要求
    if (![LimitTextFieldNum judgePassWordLegal:self.pwdTF.text]) {
        [LCProgressHUD showFailure:@"请输入6-16位密码"];
        return;
    }
    if (self.LoginBlock) {
        self.LoginBlock();
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (!isEmptyString(self.nameTF.text) && !isEmptyString(self.pwdTF.text) && !isEmptyString(self.invitationCodeTF.text)) {
        //如果都填写了就更改背景颜色并且可以点击
        _loginBtn.enabled = YES;
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"login_btn") forState:UIControlStateNormal];
        [_loginBtn setTitle:@"进入天赐艾" forState:UIControlStateNormal];
    }else {
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"commit_nor") forState:UIControlStateNormal];
        _loginBtn.enabled = NO;
        [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    }
}

@end
