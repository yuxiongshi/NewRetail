//
//  YQNewLoginView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQNewLoginView.h"

@implementation YQNewLoginView

- (void)setupView {
    //从底部开始写
    [self addSubview:self.bottomLab];
    [self addSubview:self.pwdLoginBtn];
    [self addSubview:self.loadCodeBtn];
    [self addSubview:self.theTitleLab];
    [self addSubview:self.bottomLineView];
    [self addSubview:self.leftCountryBtn];
    [self addSubview:self.teleTF];
    [self addSubview:self.theTitleOneLab];
    
    
    [self.bottomLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-(Botoom_IPhoneX+FIT(15)));
        make.height.mas_equalTo(FIT(15));
    }];
    
    [self.pwdLoginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.bottom.equalTo(self.bottomLab.mas_top).mas_equalTo(-FIT(200));
        make.size.mas_equalTo(CGSizeMake(FIT(80), FIT(20)));
    }];

    [self.loadCodeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.bottom.equalTo(self.pwdLoginBtn.mas_top).mas_equalTo(-FIT(FIT(35)));
        make.size.mas_equalTo(CGSizeMake(FIT(290), kCommitButtonHeight));
    }];

    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.bottom.equalTo(self.loadCodeBtn.mas_top).mas_equalTo(-FIT(45));
    }];

    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.bottom.equalTo(self.theTitleLab.mas_top).mas_equalTo(-FIT(12));
        make.right.mas_equalTo(-FIT(48));
        make.height.mas_equalTo(1);
    }];

    [self.leftCountryBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(48));
        make.bottom.equalTo(self.bottomLineView.mas_top).mas_equalTo(-FIT(18));
        make.size.mas_equalTo(CGSizeMake(FIT(55), FIT(15)));
    }];
    [self.leftCountryBtn layoutIfNeeded];
    [self.leftCountryBtn setIconInRightWithSpacing:5];

    [self.teleTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftCountryBtn.mas_right).mas_equalTo(FIT(30));
        make.bottom.equalTo(self.bottomLineView.mas_top).mas_equalTo(-FIT(18));
        make.right.mas_equalTo(-FIT(60));
        make.height.mas_equalTo(FIT(17));
    }];

    [self.theTitleOneLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(44));
        make.bottom.equalTo(self.leftCountryBtn.mas_top).mas_equalTo(-FIT(60));
        make.size.mas_equalTo(CGSizeMake(FIT(200), FIT(70)));
    }];
    
}

- (UILabel *)bottomLab {
    if (!_bottomLab) {
        _bottomLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:FIT(14) userBold:NO];
        _bottomLab.text = @"天赐艾ATC新零售平台";
        [_bottomLab setAttributedTextColorWithBeforeString:@"天赐艾ATC" beforeColor:UIColorFromRGB(0xcccccc) afterString:@"新零售平台" afterColor:MainRedTextColor];
    }
    return _bottomLab;
}

- (UIButton *)pwdLoginBtn {
    if (!_pwdLoginBtn) {
        _pwdLoginBtn = [YQViewFactory buttonWithTitle:@"密码登录" titleColor:kColor80 fontSize:FIT(16) userBold:NO target:self sel:@selector(pushPwdLoginView)];
    }
    return _pwdLoginBtn;
}

- (UIButton *)loadCodeBtn {
    if (!_loadCodeBtn) {
        _loadCodeBtn = [YQViewFactory buttonWithTitle:@"获取验证码" titleColor:WhiteColor fontSize:FIT(16) userBold:NO target:self sel:@selector(pushLoadCodeView)];
        [_loadCodeBtn setBackgroundImage:YQ_IMAGE(@"login_btn") forState:UIControlStateNormal];
    }
    return _loadCodeBtn;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:kColor80 textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _theTitleLab.text = @"未注册的手机号验证后自动创建账户";
    }
    return _theTitleLab;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = F1LineColor;
    }
    return _bottomLineView;
}

- (UIButton *)leftCountryBtn {
    if (!_leftCountryBtn) {
        _leftCountryBtn = [YQViewFactory buttonWithTitle:@"+86" titleColor:MainBlackColor fontSize:FIT(16) userBold:NO target:self sel:@selector(chooseCountryAction)];
        [_leftCountryBtn setImage:YQ_IMAGE(@"down_arrow") forState:UIControlStateNormal];
    }
    return _leftCountryBtn;
}

- (UITextField *)teleTF {
    if (!_teleTF) {
        _teleTF = [YQViewFactory textFieldWithPlaceholderText:@"请输入手机号" textColor:MainBlackColor fontSize:FIT(16) userBold:NO];
        _teleTF.keyboardType = UIKeyboardTypeNumberPad;
        _teleTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    }
    return _teleTF;
}

- (UILabel *)theTitleOneLab {
    if (!_theTitleOneLab) {
        _theTitleOneLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(28) userBold:YES];
        _theTitleOneLab.text = @"你好，\n欢迎来到天赐艾";
        _theTitleOneLab.numberOfLines = 0;
    }
    return _theTitleOneLab;
}

- (void)pushPwdLoginView {
    //i跳转到登录页面
    if (self.PwdLoginBlock) {
        self.PwdLoginBlock();
    }
}

- (void)pushLoadCodeView {
    //跳转到获取验证码页面
    if (self.LoadCodeBlock) {
        self.LoadCodeBlock();
    }
}

- (void)chooseCountryAction {
    //点击选择国家区号
    //暂未开放
}

@end
