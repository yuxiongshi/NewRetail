//
//  YQPWDLoginView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQPWDLoginView.h"
@interface YQPWDLoginView ()<UITextFieldDelegate>

@end
@implementation YQPWDLoginView

- (void)setupView {
    [self addSubview:self.theTitleLab];
    [self addSubview:self.forgetPwdBtn];
    [self addSubview:self.loginBtn];
    [self addSubview:self.bottomLab];
    [self addSubview:self.errorLab];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(108));
        make.left.mas_equalTo(FIT(45));
    }];
    
    NSArray *titleArr = @[@"手机号码",@"6-16位字母+数字的组合"];
    for (NSInteger i = 0; i < 2; i++) {
        UITextField *threeTF = [YQViewFactory textFieldWithPlaceholderText:titleArr[i] textColor:MainBlackColor fontSize:FIT(16) userBold:NO];
        //设置样式 以及键盘样式
        threeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:threeTF];
        threeTF.delegate = self;
        [threeTF mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(FIT(45));
            make.right.mas_equalTo(-FIT(45));
            make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(65)*i+FIT(60));
            make.height.mas_equalTo(FIT(17));
        }];
        if (i == 0) {
            self.nameTF = threeTF;
            self.nameTF.keyboardType = UIKeyboardTypeNumberPad;
        }else if (i == 1) {
            self.pwdTF = threeTF;
            [self.pwdTF mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(-FIT(65));
            }];
            
            UIButton *rightBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"hide") target:self sel:@selector(hodeOrShowPwd:)];
            [rightBtn setImage:YQ_IMAGE(@"show") forState:UIControlStateSelected];
            [self addSubview:rightBtn];
            [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.pwdTF.mas_right).mas_equalTo(0);
                make.centerY.mas_equalTo(self.pwdTF.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(FIT(18), FIT(10)));
            }];
            self.pwdTF.secureTextEntry = YES;
        }
        //分割线
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = F1LineColor;
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(FIT(45));
            make.right.mas_equalTo(-FIT(45));
            make.top.equalTo(threeTF.mas_bottom).mas_equalTo(FIT(15));
            make.height.mas_equalTo(1);
        }];
    }
    
    [self.forgetPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(45));
        make.top.equalTo(self.pwdTF.mas_bottom).mas_equalTo(FIT(30));
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.right.mas_equalTo(-FIT(45));
        make.top.equalTo(self.forgetPwdBtn.mas_bottom).mas_equalTo(FIT(21));
        make.height.mas_equalTo(kCommitButtonHeight);
    }];
    
    [self.bottomLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-(Botoom_IPhoneX+FIT(15)));
    }];
    
    [self.errorLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.top.equalTo(self.nameTF.mas_bottom).mas_equalTo(FIT(30));
    }];
    
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(28) userBold:NO];
        _theTitleLab.text = @"密码登录";
    }
    return _theTitleLab;
}

- (UIButton *)forgetPwdBtn {
    if (!_forgetPwdBtn) {
        _forgetPwdBtn = [YQViewFactory buttonWithTitle:@"忘记了？找回密码" titleColor:MainBlackColor fontSize:FIT(12) userBold:NO target:self sel:@selector(forgetPWDAction)];
    }
    return _forgetPwdBtn;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [YQViewFactory buttonWithTitle:@"登录" titleColor:WhiteColor fontSize:FIT(16) userBold:NO target:self sel:@selector(loginOrRegisterAction)];
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"login_btn") forState:UIControlStateNormal];
    }
    return _loginBtn;
}

- (UILabel *)bottomLab {
    if (!_bottomLab) {
        _bottomLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:FIT(14) userBold:NO];
        _bottomLab.text = @"登录代表你已同意天赐艾用户协议、隐私政策";
        [_bottomLab setAttributedTextColorWithBeforeString:@"登录代表你已同意天赐艾" beforeColor:UIColorFromRGB(0xcccccc) afterString:@"用户协议、隐私政策" afterColor:MainRedTextColor];
    }
    return _bottomLab;
}

- (UILabel *)errorLab {
    if (!_errorLab) {
        _errorLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _errorLab.text = @"用户名或密码错误";
        _errorLab.hidden = YES;
    }
    return _errorLab;
}


//点击隐藏或者显示
- (void)hodeOrShowPwd:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        //默认隐藏 点击显示
        self.pwdTF.secureTextEntry = NO;
    }else {
        self.pwdTF.secureTextEntry = YES;
    }
}

- (void)forgetPWDAction {
    //忘记密码
    if (self.ForgetBlock) {
        self.ForgetBlock();
    }
}

- (void)loginOrRegisterAction {
    if (isEmptyString(self.nameTF.text)) {
        [LCProgressHUD showMessage:@"请输入手机号码"];
        return;
    }
    
    if (![LimitTextFieldNum validateCellPhoneNumber:self.nameTF.text]) {
        [LCProgressHUD showFailure:@"请输入正确手机号码"];
        return;
    }
    
    if (isEmptyString(self.pwdTF.text)) {
        [LCProgressHUD showMessage:@"请输入密码"];
        return;
    }
    
    if (![LimitTextFieldNum judgePassWordLegal:self.pwdTF.text]) {
        [LCProgressHUD showFailure:@"请输入6-16位字母+数字组合"];
        return;
    }
    
    //登录
    if (self.LoginBlock) {
        self.LoginBlock();
    }
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    self.errorLab.hidden = YES;
    return YES;
}

@end
