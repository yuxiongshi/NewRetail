//
//  YQLoginView.h
//  NewRetail
//
//  Created by yuqin on 2019/6/22.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^LoginBlock) (NSDictionary *dict,NSString *pwdStr);
typedef void (^RegisterAndForgetBlock) (NSInteger tag);

@interface YQLoginView : UIView

/**
 背景图片
 */
@property (nonatomic, strong) UIImageView *backgroundImg;

/**
 log
 */
@property (nonatomic, strong) UIImageView *logImg;

/**
 账户
 */
@property (nonatomic, strong) UITextField *teleTF;

/**
 密码
 */
@property (nonatomic, strong) UITextField *pwdTF;

/**
 登录按钮
 */
@property (nonatomic, strong) UIButton *loginBtn;

/**
 忘记密码
 */
@property (nonatomic, strong) UIButton *forgetPwdBtn;

/**
 注册账号
 */
@property (nonatomic, strong) UIButton *registerBtn;

/**
 y密码的显示跟隐藏
 */
@property (nonatomic, strong) UIButton *rightBtn;

@property (nonatomic, copy) LoginBlock block;
@property (nonatomic, copy) RegisterAndForgetBlock registerBlock;


@end

NS_ASSUME_NONNULL_END
