//
//  YQCodeView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQCodeView.h"

@implementation YQCodeView

- (void)setupView {
    [self addSubview:self.theTitleOne];
    [self addSubview:self.teleNumLab];
    [self addSubview:self.codeTF];
    [self addSubview:self.cutDownBtn];
    [self addSubview:self.bottomLineView];
    [self addSubview:self.loginBtn];
    [self addSubview:self.theTitleTwo];
    
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.top.mas_equalTo(FIT(108));
    }];
    
    [self.teleNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(FIT(16));
    }];
    
    [self.cutDownBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(45));
        make.top.equalTo(self.teleNumLab.mas_bottom).mas_equalTo(FIT(54));
        make.size.mas_equalTo(CGSizeMake(FIT(81), FIT(27)));
    }];
    
    [self.codeTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.right.equalTo(self.cutDownBtn.mas_left).mas_equalTo(-FIT(10));
        make.top.equalTo(self.teleNumLab.mas_bottom).mas_equalTo(FIT(57));
        make.height.mas_equalTo(FIT(18));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.right.mas_equalTo(-FIT(45));
        make.top.equalTo(self.codeTF.mas_bottom).mas_equalTo(FIT(15));
        make.height.mas_equalTo(1);
    }];
    
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(45));
        make.right.mas_equalTo(-FIT(45));
        make.top.equalTo(self.bottomLineView.mas_bottom).mas_equalTo(FIT(21));
    }];
    
    [self.theTitleTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self.loginBtn.mas_bottom).mas_equalTo(FIT(14));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(15)));
    }];
    
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(24) userBold:YES];
        _theTitleOne.text = @"输入验证码";
    }
    return _theTitleOne;
}

- (UILabel *)teleNumLab {
    if (!_teleNumLab) {
        _teleNumLab = [YQViewFactory labelWithTextColor:kColor80 textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
    }
    return _teleNumLab;
}

- (UITextField *)codeTF {
    if (!_codeTF) {
        _codeTF = [YQViewFactory textFieldWithPlaceholderText:@"请输入验证码" textColor:MainBlackColor fontSize:FIT(16) userBold:NO];
        _codeTF.keyboardType = UIKeyboardTypeNumberPad;
    }
    return _codeTF;
}

- (UIButton *)cutDownBtn {
    if (!_cutDownBtn) {
        _cutDownBtn = [YQViewFactory buttonWithTitle:@"59s" titleColor:kColor80 fontSize:FIT(12) userBold:NO target:self sel:@selector(selectAgainAction)];
        _cutDownBtn.backgroundColor = WhiteColor;
        _cutDownBtn.layer.borderWidth = 0.5;
        _cutDownBtn.layer.borderColor = kColor80.CGColor;
        _cutDownBtn.layer.cornerRadius = 3;
    }
    return _cutDownBtn;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = F1LineColor;
    }
    return _bottomLineView;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        _loginBtn = [YQViewFactory buttonWithTitle:@"登录" titleColor:WhiteColor fontSize:FIT(16) userBold:NO target:self sel:@selector(loginOrRegisterAction)];
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"login_btn") forState:UIControlStateNormal];
    }
    return _loginBtn;
}

- (UILabel *)theTitleTwo {
    if (!_theTitleTwo) {
        _theTitleTwo = [YQViewFactory labelWithTextColor:kColor80 textAlignment:NSTextAlignmentCenter fontSize:FIT(12) userBold:NO];
        _theTitleTwo.text = @"没收到验证码？倒计时结束后可重新获取";
    }
    return _theTitleTwo;
}


- (void)selectAgainAction {
    //再次点击
    if (self.SendCodeBlock) {
        self.SendCodeBlock();
    }
}

- (void)loginOrRegisterAction {
    //登录或者注册
    if (self.LoginOrRegisterBlock) {
        self.LoginOrRegisterBlock();
    }
}

@end
