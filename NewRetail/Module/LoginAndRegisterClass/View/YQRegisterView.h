//
//  YQRegisterView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQRegisterView : YQBaseView

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 用户名输入框
 */
@property (nonatomic, strong) UITextField *nameTF;

/**
 密码
 */
@property (nonatomic, strong) UITextField *pwdTF;

/**
 邀请码
 */
@property (nonatomic, strong) UITextField *invitationCodeTF;

/**
 登录按钮
 */
@property (nonatomic, strong) UIButton *loginBtn;

/**
 登录点击事件
 */
@property (nonatomic, copy) void (^LoginBlock) (void);

@end

NS_ASSUME_NONNULL_END
