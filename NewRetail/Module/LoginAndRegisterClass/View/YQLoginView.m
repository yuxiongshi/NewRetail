//
//  YQLoginView.m
//  NewRetail
//
//  Created by yuqin on 2019/6/22.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQLoginView.h"

@interface YQLoginView ()<UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *phoneImg;
@property (nonatomic, strong) UIImageView *pwdImg;

@end

@implementation YQLoginView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLoginView];
    }
    return self;
}

- (void)setupLoginView {
    [self addSubview:self.backgroundImg];
    [self.backgroundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.mas_equalTo(0);
        make.height.mas_equalTo(FIT(220));
    }];
    
    [self.backgroundImg addSubview:self.logImg];
    [self.logImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(52));
        make.centerX.equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(100)));
    }];
    self.logImg.layer.cornerRadius = self.backgroundImg.frame.size.width/2.0;
    self.logImg.layer.masksToBounds = YES;
    
    NSDictionary *imgDict = @{@"1":@"icon-phone",
                              @"2":@"icon-code"
                              };
    NSDictionary *placeHolderDict = @{@"1":@"输入手机号",
                                      @"2":@"请输入6-16位密码"
                                      };
    CGFloat TFWitdh = ScreenWidth-FIT(72)-FIT(36);
    CGFloat img_top = FIT(220)+FIT(66);
    //左边的图片
    for (NSInteger j = 0; j < 2; j++) {
        UIImageView *leftImg = [YQViewFactory imageViewWithImage:YQ_IMAGE([imgDict objectForKey:StringFromLongInt(j+1)])];
        leftImg.userInteractionEnabled = YES;
        [self addSubview:leftImg];
        
        //lineView
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = UIColorFromRGB(0xeeeeee);
        [self addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(leftImg.mas_bottom).mas_equalTo(FIT(15));
            make.left.mas_equalTo(FIT(36));
            make.size.mas_equalTo(CGSizeMake(ScreenWidth-FIT(72), 1));
        }];
        
        //输入框
        UITextField *tf = [[UITextField alloc] init];
        tf.delegate = self;
        tf.tag = 10+j;
        tf.borderStyle = UITextBorderStyleNone;
        tf.placeholder = [placeHolderDict objectForKey:StringFromLongInt(j+1)];
        tf.font = YQ_Font(14);
        tf.returnKeyType = UIReturnKeyDone;
        tf.clearButtonMode = UITextFieldViewModeWhileEditing;
        [self addSubview:tf];
        [tf addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
        
        if (j == 0) {
            //img的设置
            self.phoneImg = leftImg;
            leftImg.frame = CGRectMake(FIT(36), img_top, FIT(14), FIT(22));
            //输入框的设置
            self.teleTF = tf;
            tf.keyboardType = UIKeyboardTypeNumberPad;
            [tf mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(leftImg.mas_right).mas_equalTo(FIT(15));
                make.centerY.mas_equalTo(leftImg.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(TFWitdh, FIT(21)));
            }];
        }else {
            self.pwdImg = leftImg;
            leftImg.frame = CGRectMake(FIT(36), img_top + FIT(66)*j, FIT(16), FIT(18));
            self.pwdTF = tf;
            self.pwdTF.secureTextEntry = YES;
            [tf mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(leftImg.mas_right).mas_equalTo(FIT(15));
                make.centerY.mas_equalTo(leftImg.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(TFWitdh-FIT(40), FIT(21)));
            }];
        }
    }

    //显示隐藏密码按钮
    [self addSubview:self.rightBtn];
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.pwdTF.mas_right).mas_equalTo(FIT(10));
        make.centerY.mas_equalTo(self.pwdImg.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(18), FIT(10)));
    }];
    
    //忘记密码/注册账号
    CGFloat foregetAndRegister_W = ScreenWidth/3.0;
    [self addSubview:self.forgetPwdBtn];
    [self.forgetPwdBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(50));
        make.left.mas_equalTo(foregetAndRegister_W);
        make.size.mas_equalTo(CGSizeMake(foregetAndRegister_W/2.0, FIT(15)));
    }];
    
    //中间竖线
    UIView *verticalBarView = [[UIView alloc] init];
    verticalBarView.backgroundColor = MainRedTextColor;
    [self addSubview:verticalBarView];
    [verticalBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.forgetPwdBtn.mas_right).mas_equalTo(0);
        make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(50));
        make.size.mas_equalTo(CGSizeMake(1, FIT(15)));
    }];
    
    //注册账号
    [self addSubview:self.registerBtn];
    [self.registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(50));
        make.left.equalTo(verticalBarView.mas_right).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(foregetAndRegister_W/2.0, FIT(15)));
    }];
    
    //登录
    [self addSubview:self.loginBtn];
    [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(28));
        make.bottom.equalTo(self.forgetPwdBtn.mas_top).mas_equalTo(-FIT(20));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-FIT(56), kCommitButtonHeight));
    }];
}

#pragma mark -dy
- (UIImageView *)backgroundImg {
    if (!_backgroundImg) {
        _backgroundImg = [[UIImageView alloc] initWithImage:YQ_IMAGE(@"backgroundImg")];

        _backgroundImg.userInteractionEnabled = YES;
        _backgroundImg.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backgroundImg;
}

- (UIImageView *)logImg {
    if (!_logImg) {
        _logImg = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"")];
        _logImg.userInteractionEnabled = YES;
        _logImg.backgroundColor = [UIColor clearColor];
    }
    return _logImg;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        //默认隐藏密码
        _rightBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"hide") target:self sel:@selector(hodeOrShowPwd:)];
        [_rightBtn setImage:YQ_IMAGE(@"show") forState:UIControlStateSelected];
    }
    return _rightBtn;
}

- (UIButton *)forgetPwdBtn {
    if (!_forgetPwdBtn) {
        _forgetPwdBtn = [YQViewFactory buttonWithTitle:@"忘记密码" titleColor:MainRedTextColor fontSize:FIT(13) userBold:NO target:self sel:@selector(forgetAndRegisterAction:)];
        _forgetPwdBtn.tag = 100;
        _forgetPwdBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _forgetPwdBtn;
}

- (UIButton *)registerBtn {
    if (!_registerBtn) {
        _registerBtn = [YQViewFactory buttonWithTitle:@"注册账号" titleColor:MainRedTextColor fontSize:FIT(13) userBold:NO target:self sel:@selector(forgetAndRegisterAction:)];
        _registerBtn.tag = 101;
        _registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _registerBtn;
}

- (UIButton *)loginBtn {
    if (!_loginBtn) {
        
        _loginBtn = [YQViewFactory buttonWithTitle:@"登录" titleColor:WhiteColor fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(loginAction)];
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"btnImg") forState:UIControlStateNormal];
        [_loginBtn setBackgroundImage:YQ_IMAGE(@"btnImg") forState:UIControlStateSelected];
    }
    return _loginBtn;
}

//点击隐藏或者显示
- (void)hodeOrShowPwd:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected == YES) {
        //默认隐藏 点击显示
        self.pwdTF.secureTextEntry = NO;
    }else {
        self.pwdTF.secureTextEntry = YES;
    }
}

//点击注册或者修改密码
- (void)forgetAndRegisterAction:(UIButton *)sender {
    //100是忘记密码 101注册账号
    if (self.registerBlock) {
        self.registerBlock(sender.tag);
    }
}
//点击登录
- (void)loginAction {
    //先判断空 再判断格式
    if (isEmptyString(self.teleTF.text)) {
        [LCProgressHUD showFailure:@"请输入手机号码"];
        return;
    }
    
    if (![LimitTextFieldNum validateCellPhoneNumber:self.teleTF.text]) {
        [LCProgressHUD showFailure:@"请输入正确手机号码"];
        return;
    }
    
    if (isEmptyString(self.pwdTF.text)) {
        [LCProgressHUD showFailure:@"请输入密码"];
        return;
    }
    
    if (![LimitTextFieldNum judgePassWordLegal:self.pwdTF.text]) {
        [LCProgressHUD showFailure:@"请输入6-16位密码"];
        return;
    }
    
    NSDictionary *dict = @{@"loginId":self.teleTF.text,
                           @"password":[AppUtil md5:self.pwdTF.text]
                           };
    if (self.block) {
        self.block(dict, self.pwdTF.text);
    }
}

- (void)textFieldDidChange:(UITextField *)textField {
    //10是手机号码 11是密码
    if (self.pwdTF == textField) {
        //密码
        
    }
}

@end
