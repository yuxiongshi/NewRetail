//
//  SZSecurityCodeViewModel.m
//  BTCoin
//
//  Created by Shizi on 2018/5/10.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "SZSecurityCodeViewModel.h"
#import "SZHttpsService.h"

@implementation SZSecurityCodeViewModel

-(void)getSecurityCodeWithParameters:(id)parameters
{
        [[[SZHttpsService sharedSZHttpsService] signalRequestMsgCodeWithParameters:parameters]subscribeNext:^(id x) {
            NSDictionary* responseDictionary=x;
            if ([responseDictionary[@"code"] integerValue] == 0) {
                
                [(RACSubject*)self.otherSignal sendNext:@"SecurityCode"];
            }else{
                
                NSString* errorMessage=responseDictionary[@"msg"];
                [(RACSubject*)self.failureSignal sendNext:errorMessage];
            }
        } error:^(NSError *error) {
            
            [(RACSubject*)self.failureSignal sendNext:error.localizedDescription];
            
        }];
    

}

-(void)getSecurityCodeTokenWithParameters:(id)parameters{
    
    [[[SZHttpsService sharedSZHttpsService] signalRequestMsgCodeTokenWithParameters:parameters]subscribeNext:^(id x) {
        NSDictionary* responseDictionary=x;
        if ([responseDictionary[@"code"] integerValue] == 0) {
            
            [(RACSubject*)self.otherSignal sendNext:@"SecurityCode"];
        }else{
            
            NSString* errorMessage=responseDictionary[@"msg"];
            [(RACSubject*)self.failureSignal sendNext:errorMessage];
        }
    } error:^(NSError *error) {
        
        [(RACSubject*)self.failureSignal sendNext:error.localizedDescription];
        
    }];
    
    
}
-(void)modifyPasswordWithParameters:(NSDictionary*)parameters{
    
    [[[SZHttpsService sharedSZHttpsService] signalSetLoginPwdWithParameters:parameters ]subscribeNext:^(id responseDictionary) {
        
        if ([responseDictionary[@"code"] integerValue] == 0) {
            NSString* msg=responseDictionary[@"msg"];
            [(RACSubject*)self.successSignal sendNext:msg];
        }else{
            NSString* msg=responseDictionary[@"msg"];
            [(RACSubject*)self.failureSignal sendNext:msg];
        }
        
    }error:^(NSError *error) {
        [(RACSubject *) self.failureSignal sendNext:error.localizedDescription];
        
    }];
    
}
@end
