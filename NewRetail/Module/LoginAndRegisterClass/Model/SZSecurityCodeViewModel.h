//
//  SZSecurityCodeViewModel.h
//  GBKTrade
//
//  Created by Shizi on 2018/5/10.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "SZRootViewModel.h"
typedef NS_OPTIONS(NSUInteger, SZSecurityCodeViewType) {
    SZSecurityCodeViewTypeModifyLoginPassword=1,
    SZSecurityCodeViewTypeFindLoginPassword=2,

};
@interface SZSecurityCodeViewModel : SZRootViewModel

-(void)getSecurityCodeWithParameters:(id)parameters;
-(void)getSecurityCodeTokenWithParameters:(id)parameters;
-(void)modifyPasswordWithParameters:(NSDictionary*)parameters;
@property (nonatomic,assign) SZSecurityCodeViewType  securityCodeType;
@property (nonatomic,copy) NSString* mobile;
@property (nonatomic,copy) NSString* areaCode;

@end
