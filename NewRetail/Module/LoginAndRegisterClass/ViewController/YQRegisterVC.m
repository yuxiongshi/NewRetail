//
//  YQRegisterVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQRegisterVC.h"
#import "YQRegisterView.h"

@interface YQRegisterVC ()

@property (nonatomic, strong) YQRegisterView *registerView;

@end

@implementation YQRegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headView.image = YQ_IMAGE(@"");
    self.headView.backgroundColor = WhiteColor;
    [self.iconAndTitleLeftBtn setImage:YQ_IMAGE(@"back_arrow") forState:UIControlStateNormal];
    
    [self.view addSubview:self.registerView];
    [self.registerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
    
}

- (YQRegisterView *)registerView {
    if (!_registerView) {
        _registerView = [[YQRegisterView alloc] init];
        _registerView.backgroundColor = WhiteColor;
        //登录
        WeakSelf(self);
        _registerView.LoginBlock = ^{
            NSDictionary *parameters = @{@"validateKey":weakSelf.validateKey,
                                         @"nickName":weakSelf.registerView.nameTF.text,
                                         @"password":[AppUtil md5:weakSelf.registerView.pwdTF.text],
                                         @"referralCode":weakSelf.registerView.invitationCodeTF.text
                                         };
            [LCProgressHUD showLoading:@"登录中..."];
            //登录
            [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"users2/register") appendParameters:nil bodyParameters:parameters successBlock:^(id responseObject) {
                BaseModel * base = [BaseModel modelWithJson:responseObject];
                if (base.code == 0) {
                    [LCProgressHUD hide];
                    UserInfo * userInfo = [UserInfo sharedUserInfo];
                    [userInfo mj_setKeyValues:base.data];
                    [UserInfo sharedUserInfo].bIsLogin = YES;
                    //记录token
                    //记录账号、密码
                    [kDefaults setObject:[responseObject objectForKey:@"data"]  forKey:@"token"];
                    [kDefaults synchronize];
                    
                    NSLog(@"===== %@",[kDefaults objectForKey:@"token"]);
                    
//                    [LCProgressHUD showSuccess:@"登录成功"];
                    //跳转首页
                    TheAppDel.window.rootViewController =[TabBarController singletonTabBarController];
                }else {
                    [UserInfo sharedUserInfo].bIsLogin = NO;
                    [LCProgressHUD showFailure:responseObject[@"msg"]];
                }
            } failureBlock:^(NSError *error) {
                [UserInfo sharedUserInfo].bIsLogin = NO;
            }];
        };
    }
    return _registerView;
}

@end
