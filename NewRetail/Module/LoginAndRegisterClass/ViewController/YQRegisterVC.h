//
//  YQRegisterVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQRegisterVC : CustomViewController

@property (nonatomic, copy) NSString *validateKey;//防止直接访问发送验证码

@end

NS_ASSUME_NONNULL_END
