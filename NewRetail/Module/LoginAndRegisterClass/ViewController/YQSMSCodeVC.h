//
//  YQSMSCodeVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQSMSCodeVC : CustomViewController

@property (nonatomic, copy) NSString *teleNum;
@property (nonatomic, copy) NSString *isRegister;//是否已注册，已注册按钮显示登录，未注册按钮显示下一步
@property (nonatomic, copy) NSString *validateKey;//防止直接访问发送验证码

@end

NS_ASSUME_NONNULL_END
