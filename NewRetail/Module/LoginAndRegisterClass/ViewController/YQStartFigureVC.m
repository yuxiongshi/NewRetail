//
//  YQStartFigureVC.m
//  KingRetail
//
//  Created by yuqin on 2019/7/23.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQStartFigureVC.h"

@interface YQStartFigureVC ()

@end

@implementation YQStartFigureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.headView setHidden:YES]; 
    NSString *imgStr;
    if (HBISIphoneX) {
        //X以上
        imgStr = @"XS_up";
    }else {
        imgStr = @"noX";
    }
    self.view.layer.contents = (id)[UIImage imageNamed:imgStr].CGImage;
    
    
    NSString *tokenString = [kDefaults objectForKey:@"token"];
    //    NSLog(@"tokenString = %@",tokenString);
    //判断token是否失效
    if (![[NSUserDefaults standardUserDefaults]boolForKey:@"firstLaunch"]) {
        [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstLaunch"];
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"everLaunch"];
        GuideViewController *vc = [[GuideViewController alloc] init];
        TheAppDel.window.rootViewController = vc;
        
    }else {
        if (!isEmptyString(tokenString)) {
            //当前无网络 默认进入登录
            [SZHTTPSReqManager get:YQ_joiningTogetherUrl(@"token") appendParameters:nil successBlock:^(id responseObject) {
                if ([responseObject[@"data"] boolValue] == YES) {
                    TheAppDel.rootTabBarController = [[TabBarController alloc] init];
                    TheAppDel.window.rootViewController = TheAppDel.rootTabBarController;                    
                }else {
                    
                    [self loginPush];
                }
            } failureBlock:^(NSError *error) {
                [self loginPush];
            }];
            
        }else {
            
            [self loginPush];
        }
    }
}

- (void)loginPush {
    YQLoginVC *loginVC = [YQLoginVC new];
    GFNavigationController * loginNav = [[GFNavigationController alloc] initWithRootViewController:loginVC];
    TheAppDel.window.rootViewController =loginNav;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
