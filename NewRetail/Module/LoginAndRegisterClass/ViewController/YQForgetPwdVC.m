//
//  YQForgetPwdVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/15.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQForgetPwdVC.h"
#import "YQPWDLoginView.h"
#import "GSForgetPwdVC.h"

@interface YQForgetPwdVC ()

@property (nonatomic,strong) YQPWDLoginView *loginView;

@end

@implementation YQForgetPwdVC
#pragma mar- 这个类名取错了0.0
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.headView.image = YQ_IMAGE(@"");
    self.headView.backgroundColor = WhiteColor;
    [self.iconAndTitleLeftBtn setImage:YQ_IMAGE(@"back_arrow") forState:UIControlStateNormal];
    
    [self.view addSubview:self.loginView];
    [self.loginView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom).offset(0);
        make.bottom.mas_equalTo(0);
    }];
}

- (YQPWDLoginView *)loginView {
    if (!_loginView) {
        _loginView = [[YQPWDLoginView alloc] init];
        _loginView.backgroundColor = WhiteColor;
        //忘记密码
        WeakSelf(self);
        _loginView.ForgetBlock = ^{
            YQ_PUSH([GSForgetPwdVC new]);
        };
        
        _loginView.LoginBlock = ^{
            //发送成功之后再跳转
            //先判断手机号码是否正确
            //先判断空 再判断格式
            
            NSDictionary *parameters = @{@"loginId":weakSelf.loginView.nameTF.text,
                                         @"password":[AppUtil md5:weakSelf.loginView.pwdTF.text],
                                         };
            [LCProgressHUD showLoading:@"登陆中..."];
            [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"users/login") appendParameters:nil bodyParameters:parameters successBlock:^(id responseObject) {
                BaseModel *base = [BaseModel modelWithJson:responseObject];
                if (base.code == 0) {
                    [LCProgressHUD hide];
                    UserInfo * userInfo = [UserInfo sharedUserInfo];
                    [userInfo mj_setKeyValues:base.data];
                    [UserInfo sharedUserInfo].bIsLogin = YES;
                    //记录token
                    //记录账号、密码
                    [kDefaults setObject:[responseObject[@"data"] objectForKey:@"token"]  forKey:@"token"];
                    [kDefaults synchronize];
                    
                    NSLog(@"===== %@",[kDefaults objectForKey:@"token"]);
                    
                    [LCProgressHUD showSuccess:@"登录成功"];
                    //跳转首页
                    TheAppDel.window.rootViewController =[TabBarController singletonTabBarController];
                }else {
                    [LCProgressHUD hide];
                    [UserInfo sharedUserInfo].bIsLogin = NO;
                    weakSelf.loginView.errorLab.hidden = NO;
                }
            } failureBlock:^(NSError *error) {
                [LCProgressHUD hide];
            }];
        };
        
    }
    return _loginView;
}

@end
