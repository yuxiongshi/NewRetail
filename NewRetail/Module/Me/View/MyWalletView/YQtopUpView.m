//
//  YQtopUpView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQtopUpView.h"

@interface YQtopUpView ()

@property (nonatomic, strong) UILabel *theTitleLab;

@end

@implementation YQtopUpView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupMoneyButton];
    }
    return self;
}

- (void)setupMoneyButton {
    
    [self addSubview:self.theTitleLab];
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(56)));
    }];
    
    NSArray *titleArr = @[@"350元",@"700元",@"1500元",@"2200元",@"4400元",@"8800元"];
    NSInteger maxLine = 3;
    CGFloat kspace = FIT(12);
    CGFloat button_w = (ScreenWidth-2*kMargin_left-3*kspace)/3.0;
    CGFloat button_h = FIT(64);
    for (NSInteger i = 0; i < 6; i++) {
        UIButton *moneyBtn = [YQViewFactory buttonWithTitle:titleArr[i] titleColor:MainBlackColor fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(chooseAction:)];
        moneyBtn.tag = 10+i;
        //f7d6ad
        [moneyBtn setTitleColor:UIColorFromRGB(0xd7933c) forState:UIControlStateSelected];
        [moneyBtn setBackgroundImage:YQ_IMAGE(@"wallet_nor") forState:UIControlStateNormal];
        [moneyBtn setBackgroundImage:YQ_IMAGE(@"cz") forState:UIControlStateSelected];
        [self addSubview:moneyBtn];
        if (i == 3) {
            moneyBtn.selected = YES;
            //创建一个左上角label
            UILabel *recommendLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(11) userBold:NO];
            recommendLab.layer.cornerRadius = 3;
            recommendLab.layer.masksToBounds = YES;
//            recommendLab.adjustsFontSizeToFitWidth = YES;
            recommendLab.text = @"推荐";
            recommendLab.backgroundColor = UIColorFromRGB(0xe69e40);
            [moneyBtn addSubview:recommendLab];
            [recommendLab mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(0);
                make.top.mas_equalTo(0);
                make.size.mas_equalTo(CGSizeMake(FIT(36), FIT(16)));
            }];
        }
        [moneyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kMargin_left+kspace*(i%3)+button_w*(i%3));
            make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(button_h*(i/3)+FIT(15)*(i/3));
            make.size.mas_equalTo(CGSizeMake(button_w, button_h));
        }];
    }
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = @"充值金额";
    }
    return _theTitleLab;
}

- (void)chooseAction:(UIButton *)button {
    NSArray *titleArr = @[@"350元",@"700元",@"1500元",@"2200元",@"4400元",@"8800元"];
    if (self.ChooseButtonBlock) {
        self.ChooseButtonBlock(titleArr[button.tag-10]);
    }
    //选中
    for (NSInteger i = 0; i < 6; i++) {
        if (button.tag == 10+i) {
            button.selected = YES;
            continue;
        }
        UIButton *btn = (UIButton *)[self viewWithTag:i+10];
        btn.selected = NO;
    }
    
}

@end
