//
//  YQMyTransferHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTransferHeadView : UIView

/**
 奖励余额
 */
@property (nonatomic, strong) UILabel *rewardBalanceLab;

/**
 可用余额
 */
@property (nonatomic, strong) UILabel *AvailableBalance;

/**
 图片
 */
@property (nonatomic, strong) UIImageView *arrowImg;

@end

NS_ASSUME_NONNULL_END
