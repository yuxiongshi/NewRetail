//
//  YQNotPayingView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQNotPayingView.h"

@implementation YQNotPayingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    [self addSubview:self.desLab];
    [self addSubview:self.theTitleLab];
    
    [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(kMargin_left);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(50)));
    }];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.desLab.mas_bottom).mas_equalTo(FIT(5));
        make.left.mas_equalTo(kMargin_left);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(14)));
    }];
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:FIT(14) userBold:NO];
        _desLab.numberOfLines = 0;
        _desLab.text = @"1、请务必在有效时间内用以下付款信息完成付款，完成后点击“标记为已付款”按钮，否则则有可能造成资金损失";
    }
    return _desLab;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:FIT(15) userBold:NO];
        _theTitleLab.text = @"我的付款信息";
    }
    return _theTitleLab;
}

@end
