//
//  YQTopUpBottomView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQTopUpBottomView.h"

@implementation YQTopUpBottomView
{
    NSString *_nameStr;//名称
    NSString *_bankStr;//银行名称、微信号、支付号
    NSString *_branchStr;//支行
    NSString *_cardNum;//银行卡号
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupBottomView];
    }
    return self;
}

- (void)setupBottomView {
//    [self addSubview:self.theTitleLab];
//    [self addSubview:self.bottomLineView];
    [self addSubview:self.headView];
    [self addSubview:self.notPayingHead];
    [self addSubview:self.nameLab];
    [self addSubview:self.bankLab];
    [self addSubview:self.codeBtn];
    [self addSubview:self.lookCodeLab];
    [self addSubview:self.branchLab];
    [self addSubview:self.cardNumLab];
    [self addSubview:self.copyNameBtn];
    [self addSubview:self.copyBankBtn];
    [self addSubview:self.copyBankNumBtn];
    [self addSubview:self.desLab];
        
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(54)));

    }];
    
    [self.notPayingHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(75)));
    }];
    
    [self.nameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.headView.hidden == YES?self.notPayingHead.mas_bottom:self.headView.mas_bottom).mas_equalTo(15);
    }];
    
    [self.copyNameBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.nameLab.mas_right).mas_equalTo(FIT(8));
        make.top.equalTo(self.nameLab.mas_top);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
//    [self.bankLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(kMargin_left);
//        make.top.equalTo(self.nameLab.mas_bottom).mas_equalTo(0);
//        make.size.mas_equalTo(CGSizeMake((ScreenWidth-2*kMargin_left)/2.0, FIT(44)));
//    }];
    
    [self.bankLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.nameLab.mas_bottom).mas_equalTo(15);
    }];
    
    [self.copyBankBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.bankLab.mas_right).mas_equalTo(FIT(8));
        make.top.equalTo(self.bankLab.mas_top);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
    [self.lookCodeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(13));
        make.size.mas_equalTo(CGSizeMake(FIT(90), FIT(12)));
    }];
    
    [self.codeBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.lookCodeLab.mas_top).mas_equalTo(-FIT(8));
        make.centerX.mas_equalTo(self.lookCodeLab.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
//    [self.branchLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(kMargin_left);
//        make.top.equalTo(self.bankLab.mas_bottom).mas_equalTo(0);
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(44)));
//    }];
    
    [self.branchLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.bankLab.mas_bottom).mas_equalTo(15);
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(44)));
    }];
    
//    [self.cardNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(kMargin_left);
//        make.top.equalTo(self.branchLab.mas_bottom).mas_equalTo(0);
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(44)));
//    }];
    
    [self.cardNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.branchLab.mas_bottom).mas_equalTo(15);
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(44)));
    }];
    
    [self.copyBankNumBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cardNumLab.mas_right).mas_equalTo(FIT(8));
        make.top.equalTo(self.cardNumLab.mas_top);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
}

- (YQOrderBankInforView *)headView {
    if (!_headView) {
        _headView = [[YQOrderBankInforView alloc] init];
        _headView.backgroundColor = WhiteColor;
    }
    return _headView;
}

- (YQNotPayingView *)notPayingHead {
    if (!_notPayingHead) {
        _notPayingHead = [[YQNotPayingView alloc] init];
        _notPayingHead.backgroundColor = WhiteColor;
        _notPayingHead.hidden = YES;
    }
    return _notPayingHead;
}

- (UILabel *)nameLab {
    if (!_nameLab) {
        _nameLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
    }
    return _nameLab;
}

- (UILabel *)bankLab {
    if (!_bankLab) {
        _bankLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        
    }
    return _bankLab;
}

- (UILabel *)branchLab {
    if (!_branchLab) {
        _branchLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
    }
    return _branchLab;
}

- (UILabel *)cardNumLab {
    if (!_cardNumLab) {
        _cardNumLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
    }
    return _cardNumLab;
}

- (UILabel *)lookCodeLab {
    if (!_lookCodeLab) {
        _lookCodeLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentRight fontSize:FIT(12) userBold:NO];
        _lookCodeLab.text = @"点击查看收款码";
        _lookCodeLab.hidden = YES;
    }
    return _lookCodeLab;
}

- (UIButton *)codeBtn {
    if (!_codeBtn) {
        _codeBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"my_code") target:self sel:@selector(codeAction)];
        _codeBtn.hidden = YES;
    }
    return _codeBtn;
}

- (UIButton *)copyNameBtn {
    if (!_copyNameBtn) {
        _copyNameBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"me_copy") target:self sel:@selector(copyAction:)];
        _copyNameBtn.tag = 10;
        _copyNameBtn.hidden = YES;
    }
    return _copyNameBtn;
}

- (UIButton *)copyBankBtn {
    if (!_copyBankBtn) {
        _copyBankBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"me_copy") target:self sel:@selector(copyAction:)];
        _copyBankBtn.tag = 11;
        _copyBankBtn.hidden = YES;
    }
    return _copyBankBtn;
}

- (UIButton *)copyBankNumBtn {
    if (!_copyBankNumBtn) {
        _copyBankNumBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"me_copy") target:self sel:@selector(copyAction:)];
        _copyBankNumBtn.tag = 12;
        _copyBankNumBtn.hidden = YES;
    }
    return _copyBankNumBtn;
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:FIT(14) userBold:NO];
        _desLab.numberOfLines = 2;
        _desLab.hidden = YES;
        _desLab.text = @"2、公式收款信息可能会更改，请每次付款以本页面的信息为准。";
    }
    return _desLab;
}

- (void)copyAction:(UIButton *)sender {
    //10、11、12 姓名 银行 卡号 只有在公司收款信息中显示  (当状态为待付款)
//    NSLog(@"self.nameLab.text = %@",self.nameLab.text);
    NSArray *titleArr = @[self.nameLab.text,self.bankLab.text,self.cardNumLab.text];
    UIPasteboard *paste = [UIPasteboard generalPasteboard];
    [paste setString:titleArr[sender.tag-10]];
    if (paste == nil) {
        [LCProgressHUD showFailure:@"复制失败"];
    }else {
        [LCProgressHUD showSuccess:@"已复制到粘贴板"];
    }
}

- (void)codeAction {
    //点击展开popView
    if (self.LookBlock) {
        self.LookBlock();
    }
}

- (void)loadDataWithModel:(YQAvertiseShowModel *)model index:(NSString *)index {//充值或者提现页面的View
    NSInteger payway = StringToLongInt(index);

    if (payway == 1) {
        //微信
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.realName] bankOrWechat:[NSString stringWithFormat:@"微信号：%@",model.payAccount] branch:@"" cardNum:@""];
    }else if (payway == 2) {
        //支付宝
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.realName] bankOrWechat:[NSString stringWithFormat:@"支付宝账号：%@",model.payAccount] branch:@"" cardNum:@""];
    }else if (payway == 3) {
        //银奖卡
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.bankUserName] bankOrWechat:[NSString stringWithFormat:@"银 行：%@",model.bankName] branch:[NSString stringWithFormat:@"支 行：%@",model.bankAddress] cardNum:[NSString stringWithFormat:@"卡 号：%@",model.bankNum]];
    }
}

- (void)loadMeInfoOrderDetailWithModel:(YQMyOrderDetailModel *)model payWay:(NSString *)payWay {//上面本人的信息
    NSInteger payway = [payWay integerValue];
    if (payway == 1) {
        //微信
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.wechatName] bankOrWechat:[NSString stringWithFormat:@"微信号：%@",model.wechatAccount] branch:@"" cardNum:@""];
    }else if (payway == 2) {
        //支付宝
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.alipayName] bankOrWechat:[NSString stringWithFormat:@"支付宝账号：%@",model.alipayAccount] branch:@"" cardNum:@""];
    }else if (payway == 3) {
        //银奖卡
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.bankRealName] bankOrWechat:[NSString stringWithFormat:@"银 行：%@",model.bankName] branch:[NSString stringWithFormat:@"支 行：%@",model.bankAddress] cardNum:[NSString stringWithFormat:@"卡 号：%@",model.bankNum]];
    }
    [self showBottomLabelWithPayway:payWay];
}

- (void)loadCompanyOrderDetailWithModel:(YQMyOrderDetailModel *)model payWay:(NSString *)payWay {
    NSInteger payway = [payWay integerValue];
    if (payway == 1) {
        //微信
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.companyAccountName] bankOrWechat:[NSString stringWithFormat:@"微信号：%@",model.companyWechatAccount] branch:@"" cardNum:@""];
    }else if (payway == 2) {
        //支付宝
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.companyAccountName] bankOrWechat:[NSString stringWithFormat:@"支付宝账号：%@",model.companyAliAccount] branch:@"" cardNum:@""];
    }else if (payway == 3) {
        //银奖卡
        [self loadLabelTextWithNameLab:[NSString stringWithFormat:@"姓 名：%@",model.companyAccountName] bankOrWechat:[NSString stringWithFormat:@"银 行：%@",model.companyBankName] branch:[NSString stringWithFormat:@"支 行：%@",model.companyBankAddress] cardNum:[NSString stringWithFormat:@"卡 号：%@",model.companyBankNum]];
    }
    [self showBottomLabelWithPayway:payWay];
}

- (void)showBottomLabelWithPayway:(NSString *)payway {
//    [self addSubview:self.desLab];
    if ([payway integerValue] == 1 || [payway integerValue] == 2) {
        //wechat alipay
        
        [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kMargin_left);
            make.top.equalTo(self.bankLab.mas_bottom).mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(56)));
        }];
    }else {
        [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kMargin_left);
            make.top.equalTo(self.cardNumLab.mas_bottom).mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(56)));
        }];
    }
}

- (void)loadLabelTextWithNameLab:(NSString *)name bankOrWechat:(NSString *)wechat branch:(NSString *)branch cardNum:(NSString *)cardNum {
    
    self.nameLab.text = name;
    self.bankLab.text = wechat;
    self.branchLab.text = branch;
    self.cardNumLab.text = cardNum;
}

@end
