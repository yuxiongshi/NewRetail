//
//  YQNoPaymentHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQNoPaymentHeadView : UIView

/**
 背景图
 */
@property (nonatomic, strong) UIImageView *backImgView;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleOne;

/**
 背景试图
 */
@property (nonatomic, strong) UIView *backView;

/**
 标题
 */
@property (nonatomic, strong) UILabel *biaotiLab;

/**
 预充值金额
 */
@property (nonatomic, strong) UILabel *preloadedLab;



@property (nonatomic, copy) void (^BackBlock) (void);

@end

NS_ASSUME_NONNULL_END
