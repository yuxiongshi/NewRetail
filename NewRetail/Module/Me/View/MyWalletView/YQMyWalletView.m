//
//  YQMyWalletView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyWalletView.h"

@interface YQMyWalletView()

@property (nonatomic, strong) UILabel *theTitleOne;
@property (nonatomic, strong) UILabel *theTitleTwo;
@property (nonatomic, strong) UILabel *theTitleThree;
@property (nonatomic, strong) UIImageView *backgroundImg;

@end

@implementation YQMyWalletView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupWalletView];
    }
    return self;
}

- (void)setupWalletView {
    //UI加载顺序从上往下 从左往右
    [self addSubview:self.backgroundImg];
    [self.backgroundImg addSubview:self.backBtn];
    [self.backgroundImg addSubview:self.theTitleLab];
    [self.backgroundImg addSubview:self.theTitleOne];
    [self.backgroundImg addSubview:self.balanceLab];
    [self.backgroundImg addSubview:self.transferBtn];
    [self.backgroundImg addSubview:self.detailBtn];
    [self.backgroundImg addSubview:self.theTitleTwo];
    [self.backgroundImg addSubview:self.theTitleThree];
    [self.backgroundImg addSubview:self.rewardBalanceLab];
    [self.backgroundImg addSubview:self.freezeBalanceLab];
    
    
    [self.backgroundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(42), NavigationStatusBarHeight-StatusBarHeight));
    }];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(21)));
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backgroundImg.mas_centerX);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(25));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(15)));
    }];
    
    [self.balanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backgroundImg.mas_centerX);
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(FIT(16));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(25)));
    }];
    
    //奖励余额 、冻结余额 为1/3
    [self.theTitleTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(68));
        make.top.equalTo(self.balanceLab.mas_bottom).mas_equalTo(FIT(34));
        make.size.mas_equalTo(CGSizeMake(FIT(75), FIT(15)));
    }];
    
    [self.theTitleThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(68));
        make.top.equalTo(self.balanceLab.mas_bottom).mas_equalTo(FIT(34));
        make.size.mas_equalTo(CGSizeMake([UILabel getWidthWithText:@"冻结余额" height:FIT(15) font:kDescribeFont]+FIT(5), FIT(15)));
    }];
    
    //划转、详情
    [self.transferBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.theTitleTwo.mas_top).mas_equalTo(FIT(5));
        make.left.equalTo(self.theTitleTwo.mas_right).mas_equalTo(FIT(5));
        make.size.mas_equalTo(CGSizeMake(FIT(30), FIT(15)));
    }];
    
    [self.detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.theTitleThree.mas_top).mas_equalTo(FIT(5));
        make.left.equalTo(self.theTitleThree.mas_right).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(FIT(30), FIT(15)));
    }];
    
    [self.rewardBalanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.theTitleTwo.mas_bottom).mas_equalTo(FIT(16));
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(25)));
    }];
    
    [self.freezeBalanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.theTitleThree.mas_bottom).mas_equalTo(FIT(16));
        make.right.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(25)));
    }];
}

- (UIImageView *)backgroundImg {
    if (!_backgroundImg) {
        _backgroundImg = [[UIImageView alloc] init];
        _backgroundImg.image = YQ_IMAGE(@"wallet_bg");
        _backgroundImg.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImg.userInteractionEnabled = YES;
    }
    return _backgroundImg;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
        _backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _backBtn;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        _theTitleLab.text = @"我的钱包";
    }
    return _theTitleLab;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _theTitleOne.text = @"可用余额";
    }
    return _theTitleOne;
}

- (UILabel *)balanceLab {
    if (!_balanceLab) {
        _balanceLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(24) userBold:YES];
        _balanceLab.text = @"￥8,000.00";
    }
    return _balanceLab;
}

- (UIButton *)transferBtn {
    if (!_transferBtn) {
        _transferBtn = [YQViewFactory buttonWithTitle:@"划转" titleColor:UIColorFromRGB(0xFFFD7C) fontSize:FIT(14) userBold:NO target:self sel:@selector(detailAction:)];
        _transferBtn.tag = 100;
    }
    return _transferBtn;
}

- (UIButton *)detailBtn {
    if (!_detailBtn) {
        _detailBtn = [YQViewFactory buttonWithTitle:@"详情" titleColor:UIColorFromRGB(0xFFFD7C) fontSize:FIT(14) userBold:NO target:self sel:@selector(detailAction:)];
        _detailBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _detailBtn.tag = 101;
    }
    return _detailBtn;
}

- (UILabel *)theTitleTwo {
    if (!_theTitleTwo) {
        _theTitleTwo = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _theTitleTwo.text = @"奖励余额";
    }
    return _theTitleTwo;
}

- (UILabel *)rewardBalanceLab {
    if (!_rewardBalanceLab) {
        _rewardBalanceLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(24) userBold:YES];
        _rewardBalanceLab.text = @"￥8,000.00";
    }
    return _rewardBalanceLab;
}

- (UILabel *)theTitleThree {
    if (!_theTitleThree) {
        _theTitleThree = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleThree.text = @"冻结余额";
    }
    return _theTitleThree;
}

- (UILabel *)freezeBalanceLab {
    if (!_freezeBalanceLab) {
        _freezeBalanceLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(24) userBold:YES];
        _freezeBalanceLab.text = @"￥8,000.00";
    }
    return _freezeBalanceLab;
}

#pragma mark - 点击事件
- (void)detailAction:(UIButton *)sender {
    if (self.TransferBlock) {
        self.TransferBlock(sender.tag);
    }
}

- (void)backAction {
    //返回
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
