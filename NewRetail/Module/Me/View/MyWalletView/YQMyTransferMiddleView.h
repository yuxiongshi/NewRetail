//
//  YQMyTransferMiddleView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTransferMiddleView : UIView

/**
 划转数量
 */
@property (nonatomic, strong) UILabel *transferNumLab;

/**
 手续费
 */
@property (nonatomic, strong) UILabel *poundageLab;

/**
 数量
 */
@property (nonatomic, strong) UITextField *transferTF;

/**
 CNY
 */
@property (nonatomic, strong) UILabel *cnyLab;

@property (nonatomic, strong) UIView *lineView_H;

/**
  全部按钮
 */
@property (nonatomic, strong) UIButton *commitBtn;

/**
 底线
 */
@property (nonatomic, strong) UIView *bottomLineView;

/**
 可用余额
 */
@property (nonatomic, strong) UILabel *balanceLab;

/**
  block
 */
@property (nonatomic, copy) void (^CommitAllBlock) (void);

@end

NS_ASSUME_NONNULL_END
