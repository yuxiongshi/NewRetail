//
//  YQTwoButtonView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQTwoButtonView : UIView

/**
 充值
 */
@property (nonatomic, strong) UIButton *upMoneyBtn;

/**
 提现
 */
@property (nonatomic, strong) UIButton *withdrawalBtn;

@property (nonatomic, copy) void (^SelectBlock) (NSInteger tag);

@end

NS_ASSUME_NONNULL_END
