//
//  YQOrderBankInforView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQOrderBankInforView : UIView

@property (nonatomic, strong) UILabel *theTitleLab;//标题
@property (nonatomic, strong) UIView *bottomLineView;

@end

NS_ASSUME_NONNULL_END
