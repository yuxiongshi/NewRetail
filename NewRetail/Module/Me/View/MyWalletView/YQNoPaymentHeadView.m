//
//  YQNoPaymentHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQNoPaymentHeadView.h"

@implementation YQNoPaymentHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupHeadView];
    }
    return self;
}

- (void)setupHeadView {
    [self addSubview:self.backImgView];
    [self.backImgView addSubview:self.backBtn];
    [self.backImgView addSubview:self.theTitleOne];
    [self.backImgView addSubview:self.backView];
    [self.backView addSubview:self.biaotiLab];
    [self.backView addSubview:self.preloadedLab];
    
    
    [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(44), FIT(44)));
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(18)));
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(10));
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(FIT(25));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-FIT(20), FIT(80)));
    }];

    [self.biaotiLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(10));
        make.centerX.mas_equalTo(self.backView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(12)));
    }];
    
    [self.preloadedLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backView.mas_centerX);
        make.top.equalTo(self.biaotiLab.mas_bottom).mas_equalTo(FIT(14));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(18)));
    }];
}

- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"no_fk")];
        _backImgView.userInteractionEnabled = YES;
    }
    return _backImgView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
    }
    return _backBtn;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = WhiteColor;
        _backView.layer.cornerRadius = 3;
        _backView.layer.masksToBounds = YES;
    }
    return _backView;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(19) userBold:NO];
        _theTitleOne.text = @"订单详情";
    }
    return _theTitleOne;
}

- (UILabel *)biaotiLab {
    if (!_biaotiLab) {
        _biaotiLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xc0c0c0) textAlignment:NSTextAlignmentCenter fontSize:FIT(12) userBold:NO];
        _biaotiLab.text = @"预充值金额";
    }
    return _biaotiLab;
}

- ( UILabel *)preloadedLab {
    if (!_preloadedLab) {
        _preloadedLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:FIT(24) userBold:NO];
    }
    return _preloadedLab;
}

- (void)backAction {
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
