//
//  YQMyOrderheadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyOrderheadView.h"
#import "YQMyOrderDetailCell.h"

@interface YQMyOrderheadView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIView *backView1;
@property (nonatomic, strong) UIView *backView2;
@property (nonatomic, strong) UITableView *bottomTB;
@property (nonatomic, strong) NSArray *titleArr;//左边的标题数组
@property (nonatomic, strong) NSArray *desArr;//右边的值数组
@property (nonatomic, strong) YQMyOrderDetailModel *detailModel;

@end
@implementation YQMyOrderheadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupHeadView];
    }
    return self;
}

- (void)setupHeadView {
    [self addSubview:self.backImgView];
    [self.backImgView addSubview:self.backBtn];
    [self.backImgView addSubview:self.theTitleOne];
    [self.backImgView addSubview:self.statusLab];
    [self.backImgView addSubview:self.backView1];
    [self.backView1 addSubview:self.theTitleTwo];
    [self.backView1 addSubview:self.preloadedLab];
    [self.backView1 addSubview:self.theTitleThree];
    [self.backView1 addSubview:self.actualValueLab];
    [self.backImgView addSubview:self.backView2];
    [self.backView2 addSubview:self.bottomTB];
    
    [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(44), FIT(44)));
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(18)));
    }];
    
    [self.statusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(FIT(25));
    }];
    
    [self.backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.statusLab.mas_bottom).mas_equalTo(FIT(26));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(74)));
    }];
    
//    CGFloat label_W = (ScreenWidth-4*kMargin_left)/2.0;
    [self.theTitleTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(10));
        make.top.mas_equalTo(FIT(16));
        make.size.mas_equalTo(CGSizeMake(FIT(80), FIT(14)));
    }];
    
    [self.preloadedLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerX.mas_equalTo(self.theTitleTwo.mas_centerX);
        make.top.equalTo(self.theTitleTwo.mas_bottom).mas_equalTo(FIT(10));
        make.height.mas_equalTo(FIT(18));
    }];
    
    [self.theTitleThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(10));
        make.top.mas_equalTo(FIT(16));
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(14)));
    }];
    
    [self.actualValueLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.centerX.mas_equalTo(self.theTitleThree.mas_centerX);
        make.top.equalTo(self.theTitleThree.mas_bottom).mas_equalTo(FIT(10));
        make.height.mas_equalTo(FIT(18));
    }];
    
//    [self.backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.mas_equalTo(kMargin_left);
//        make.top.equalTo(self.backView1.mas_bottom).mas_equalTo(FIT(10));
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(30)*self.titleArr.count));
//    }];
    
    [self.bottomTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"order_back")];
        _backImgView.userInteractionEnabled = YES;
    }
    return _backImgView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
    }
    return _backBtn;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(19) userBold:NO];
        _theTitleOne.text = @"订单详情";
    }
    return _theTitleOne;
}

- (UILabel *)statusLab {
    if (!_statusLab) {
        _statusLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(24) userBold:YES];
        _statusLab.text = @"已成功";
    }
    return _statusLab;
}

- (UIView *)backView1 {
    if (!_backView1) {
        _backView1 = [[UIView alloc] init];
        _backView1.backgroundColor = WhiteColor;
        _backView1.layer.cornerRadius = 5;
        _backView1.layer.masksToBounds = YES;
    }
    return _backView1;
}

- (UILabel *)theTitleTwo {
    if (!_theTitleTwo) {
        _theTitleTwo = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleTwo.text = @"预充值金额";
    }
    return _theTitleTwo;
}

- (UILabel *)theTitleThree {
    if (!_theTitleThree) {
        _theTitleThree = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _theTitleThree.text = @"实际充值金额";
    }
    return _theTitleThree;
}

- (UILabel *)preloadedLab {
    if (!_preloadedLab) {
        _preloadedLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xFF8F34) textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        _preloadedLab.text = @"1500";
    }
    return _preloadedLab;
}

- (UILabel *)actualValueLab {
    if (!_actualValueLab) {
        _actualValueLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        _actualValueLab.text = @"1500";
    }
    return _actualValueLab;
}

- (UIView  *)backView2 {
    if (!_backView2) {
        _backView2 = [[UIView alloc] init];
        _backView2.backgroundColor = WhiteColor;
        _backView2.layer.cornerRadius = 5;
        _backView2.layer.masksToBounds = YES;
    }
    return _backView2;
}

- (UITableView *)bottomTB {
    if (!_bottomTB) {
        _bottomTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _bottomTB.scrollEnabled = NO;
        _bottomTB.bounces = NO;
        _bottomTB.delegate = self;
        _bottomTB.dataSource = self;
        _bottomTB.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_bottomTB registerClass:[YQMyOrderDetailCell class] forCellReuseIdentifier:YQ_MyOrderDetailCell];
    }
    return _bottomTB;
}

- (void)loadOrderHeadViewWithModel:(YQMyOrderDetailModel *)model {
    _detailModel = model;
    [self.bottomTB reloadData];
    //根据status区分头部显示
    /**
     1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
     */
    NSInteger status = [model.orderStatus integerValue];
    NSDictionary *titleDict = @{@"1":@"未付款",
                                @"2":@"确认中",
                                @"3":@"已成功",
                                @"4":@"用户取消",
                                @"5":@"支付超时"
                                };
    self.statusLab.text = [titleDict objectForKey:model.orderStatus];
    if (status == 4 || status == 5) {
        [self loadDataWithModel:model];
        self.titleArr = @[@"订单号",@"下单时间",@"取消时间"];
        self.desArr = @[model.orderNum,model.createTime,model.cancelTime];
    }else if (status == 3) {
        self.titleArr = @[@"订单号",@"下单时间",@"完成时间"];
        self.desArr = @[model.orderNum,model.createTime,model.updateTime];
        self.preloadedLab.text = model.cashNum;
        self.actualValueLab.text = model.realCNYNum;
    }else if (status == 2) {
        self.titleArr = @[@"订单号",@"下单时间"];
        self.desArr = @[model.orderNum,model.createTime];
        [self loadDataWithModel:model];
    }
    [self.backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.backView1.mas_bottom).mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(30)*self.titleArr.count));
    }];
}

- (void)loadDataWithModel:(YQMyOrderDetailModel *)model {
    [self.preloadedLab setHidden:YES];
    [self.actualValueLab setHidden:YES];
    self.theTitleThree.textColor = UIColorFromRGB(0xFF8F34);
    self.theTitleThree.font = YQ_Font(FIT(18));
    self.theTitleThree.text = model.cashNum;
    //更新backView1的高度
    [self.backView1 mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(FIT(46));
    }];
}

#pragma mark - 代理
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(30);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQMyOrderDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_MyOrderDetailCell forIndexPath:indexPath];
    cell.theTitleLab.text = self.titleArr[indexPath.row];
    cell.desLab.text = self.desArr[indexPath.row];
    if (indexPath.row == 0) {
        //copyblock
        cell.CopyBlock = ^{
            UIPasteboard *paste = [UIPasteboard generalPasteboard];
            [paste setString:self.desArr[0]];
            if (paste == nil) {
                [LCProgressHUD showFailure:@"复制失败"];
            }else {
                [LCProgressHUD showSuccess:@"已复制到粘贴板"];
            }
        };
    }else {
        //更新frame
        [cell.copyBtn removeFromSuperview];
        [cell.desLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-FIT(10));
        }];
        [cell.desLab.superview layoutIfNeeded];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (void)backAction {
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
