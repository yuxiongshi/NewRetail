//
//  YQMyWalletView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyWalletView : UIView

/**
 可用余额
 */
@property (nonatomic, strong) UILabel *balanceLab;

/**
 划转
 */
@property (nonatomic, strong) UIButton *transferBtn;

/**
 详情
 */
@property (nonatomic, strong) UIButton *detailBtn;

/**
 奖励余额
 */
@property (nonatomic, strong) UILabel *rewardBalanceLab;

/**
 冻结余额
 */
@property (nonatomic, strong) UILabel *freezeBalanceLab;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 title
 */
@property (nonatomic, strong) UILabel *theTitleLab;

@property (nonatomic, copy) void (^TransferBlock) (NSInteger tag);
//@property (nonatomic, copy) void (^DetailBlock) (void);
@property (nonatomic, copy) void (^BackBlock) (void);

@end

NS_ASSUME_NONNULL_END
