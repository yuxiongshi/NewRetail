//
//  YQOrderBankInforView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQOrderBankInforView.h"

@implementation YQOrderBankInforView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    [self addSubview:self.theTitleLab];
    [self addSubview:self.bottomLineView];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(53)));
    }];
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_offset(0);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = @"绑定信息";
    }
    return _theTitleLab;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = UIColorFromRGB(0xf1f1f1);
    }
    return _bottomLineView;
}

@end
