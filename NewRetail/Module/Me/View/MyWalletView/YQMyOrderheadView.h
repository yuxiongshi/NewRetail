//
//  YQMyOrderheadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YQMyOrderDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderheadView : UIView

/**
 背景图
 */
@property (nonatomic, strong) UIImageView *backImgView;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleOne;

/**
 是否成功支付
 */
@property (nonatomic, strong) UILabel *statusLab;

/**
 预充值金额标题
 */
@property (nonatomic, strong) UILabel *theTitleTwo;

/**
 预充值金额
 */
@property (nonatomic, strong) UILabel *preloadedLab;

/**
 实际充值金额
 */
@property (nonatomic, strong) UILabel *actualValueLab;

/**
 实际充值金额标题
 */
@property (nonatomic, strong) UILabel *theTitleThree;

@property (nonatomic, copy) void (^BackBlock) (void);

- (void)loadOrderHeadViewWithModel:(YQMyOrderDetailModel *)model;

@end

NS_ASSUME_NONNULL_END
