//
//  YQNotPayingView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQNotPayingView : UIView

/**
 描述
 */
@property (nonatomic, strong) UILabel *desLab;

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

@end

NS_ASSUME_NONNULL_END
