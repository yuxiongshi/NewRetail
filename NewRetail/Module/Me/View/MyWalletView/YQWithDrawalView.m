//
//  YQWithDrawalView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQWithDrawalView.h"

@implementation YQWithDrawalView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    [self addSubview:self.theTitleLab];
    [self addSubview:self.moneyTF];
    [self addSubview:self.bottomLineView];
    [self addSubview:self.poundageLab];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(54)));
    }];
    
    [self.moneyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(27)));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.moneyTF.mas_bottom).mas_equalTo(FIT(15));
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(1);
    }];
    
    [self.poundageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.bottomLineView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(30)));
    }];
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = @"提现金额";
    }
    return _theTitleLab;
}

- (UITextField *)moneyTF {
    if (!_moneyTF) {
        _moneyTF = [YQViewFactory textFieldWithPlaceholderText:@"提现金额" textColor:MainBlackColor fontSize:FIT(24) userBold:NO];
        _moneyTF.clearButtonMode = UITextFieldViewModeAlways;
        _moneyTF.keyboardType = UIKeyboardTypeDecimalPad;
    }
    return _moneyTF;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = F1LineColor;
    }
    return _bottomLineView;
}

- (UILabel *)poundageLab {
    if (!_poundageLab) {
        _poundageLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xbfbfbf) textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
    }
    return _poundageLab;
}

@end
