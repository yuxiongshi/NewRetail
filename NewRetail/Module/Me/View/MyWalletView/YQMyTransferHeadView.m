//
//  YQMyTransferHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTransferHeadView.h"

@implementation YQMyTransferHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupHeadView];
    }
    return self;
}

- (void)setupHeadView {
    [self addSubview:self.rewardBalanceLab];
    [self addSubview:self.arrowImg];
    [self addSubview:self.AvailableBalance];
    CGFloat label_W = ScreenWidth/3.0;
    [self.rewardBalanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(label_W, FIT(20)));
    }];
    
    [self.arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.centerX.mas_equalTo(self.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(32), FIT(8)));
    }];
    
    [self.AvailableBalance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(label_W, FIT(20)));
    }];
    
}

- (UILabel *)rewardBalanceLab {
    if (!_rewardBalanceLab) {
        _rewardBalanceLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:FIT(21) userBold:YES];
        _rewardBalanceLab.text = @"奖励余额";
    }
    return _rewardBalanceLab;
}

- (UIImageView *)arrowImg {
    if (!_arrowImg) {
        _arrowImg = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"arrow_transfer")];
    }
    return _arrowImg;
}

- (UILabel *)AvailableBalance {
    if (!_AvailableBalance) {
        _AvailableBalance = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(21) userBold:YES];
        _AvailableBalance.text = @"可用余额";
    }
    return _AvailableBalance;
}

@end
