//
//  YQTopUpBottomView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YQAvertiseShowModel.h"
#import "YQMyOrderDetailModel.h"
#import "YQOrderBankInforView.h"
#import "YQNotPayingView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQTopUpBottomView : UIView

//银行卡显示4个  支付宝 微信2个

//@property (nonatomic, strong) UILabel *theTitleLab;//标题
//@property (nonatomic, strong) UIView *bottomLineView;

//默认是显示YQOrderBankInforView  当未付款点击进入是改成显示YQNotPayingView
/**
 显示头部
 */
@property (nonatomic, strong) YQOrderBankInforView *headView;

/**
 显示头部
 */
@property (nonatomic, strong) YQNotPayingView *notPayingHead;


/**
 姓名
 */
@property (nonatomic, strong) UILabel *nameLab;

/**
 银行
 */
@property (nonatomic, strong) UILabel *bankLab;

/**
 支行
 */
@property (nonatomic, strong) UILabel *branchLab;

/**
 卡号
 */
@property (nonatomic, strong) UILabel *cardNumLab;

/**
 二维码
 */
@property (nonatomic, strong) UIButton *codeBtn;

//copy
@property (nonatomic, strong) UIButton *copyNameBtn;

//copy
@property (nonatomic, strong) UIButton *copyBankBtn;

//copy
@property (nonatomic, strong) UIButton *copyBankNumBtn;

//底部描述
@property (nonatomic, strong) UILabel *desLab;

//顶部描述
@property (nonatomic, strong) UILabel *topDesLab;

/**
 点击查看收款码
 */
@property (nonatomic, strong) UILabel *lookCodeLab;

@property (nonatomic, copy) void (^LookBlock) (void);


- (void)loadDataWithModel:(YQAvertiseShowModel *)model index:(NSString *)index;

/**
 给个人支付信息赋值

 @param model 内容
 @param payWay 支付方式
 */
- (void)loadMeInfoOrderDetailWithModel:(YQMyOrderDetailModel *)model payWay:(NSString *)payWay;

/**
 给公司支付信息赋值

 @param model 内容
 @param payWay 支付方式
 */
- (void)loadCompanyOrderDetailWithModel:(YQMyOrderDetailModel *)model payWay:(NSString *)payWay;

@end

NS_ASSUME_NONNULL_END
