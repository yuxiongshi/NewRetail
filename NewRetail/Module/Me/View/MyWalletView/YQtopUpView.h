//
//  YQtopUpView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQtopUpView : UIView

@property (nonatomic, copy) void (^ChooseButtonBlock) (NSString *amount);

@end

NS_ASSUME_NONNULL_END
