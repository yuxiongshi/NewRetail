//
//  YQWithDrawalView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQWithDrawalView : UIView

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 提现金额
 */
@property (nonatomic, strong) UITextField *moneyTF;

/**
 分割线
 */
@property (nonatomic, strong) UIView *bottomLineView;

/**
 手续费
 */
@property (nonatomic, strong) UILabel *poundageLab;

@end

NS_ASSUME_NONNULL_END
