//
//  YQMyTransferMiddleView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTransferMiddleView.h"

@implementation YQMyTransferMiddleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupMiddleView];
    }
    return self;
}

- (void)setupMiddleView {
    [self addSubview:self.transferNumLab];
    [self addSubview:self.poundageLab];
    [self addSubview:self.transferTF];
    [self addSubview:self.cnyLab];
    [self addSubview:self.lineView_H];
    [self addSubview:self.commitBtn];
    [self addSubview:self.bottomLineView];
    [self addSubview:self.balanceLab];
    
    [self.transferNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(FIT(30));
        make.size.mas_equalTo(CGSizeMake((ScreenWidth-2*kMargin_left)/2.0, FIT(15)));
    }];
    
    [self.poundageLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.mas_equalTo(FIT(30));
        make.size.mas_equalTo(CGSizeMake((ScreenWidth-2*kMargin_left)/2.0, FIT(15)));
    }];
    
    [self.transferTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(20));
        make.top.equalTo(self.transferNumLab.mas_bottom).mas_equalTo(FIT(30));
        make.size.mas_equalTo(CGSizeMake(FIT(200), FIT(30)));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(8));
        make.top.equalTo(self.transferTF.mas_bottom).mas_equalTo(FIT(15));
        make.right.mas_equalTo(-FIT(8));
        make.height.mas_equalTo(FIT(1));
    }];
    
    [self.commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.equalTo(self.bottomLineView.mas_top).mas_equalTo(-FIT(12));
        make.size.mas_equalTo(CGSizeMake(FIT(45), FIT(15)));
    }];
    
    [self.lineView_H mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.bottomLineView.mas_top).mas_equalTo(-FIT(12));
        make.right.equalTo(self.commitBtn.mas_left).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(1, FIT(15)));
    }];
    
    [self.cnyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.lineView_H.mas_left).mas_equalTo(0);
        make.bottom.equalTo(self.bottomLineView.mas_top).mas_equalTo(-FIT(12));
        make.size.mas_equalTo(CGSizeMake(FIT(45), FIT(15)));
    }];
    
    [self.balanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomLineView.mas_bottom).mas_equalTo(FIT(5));
        make.left.mas_equalTo(kMargin_left);
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(FIT(15));
    }];
    
}

#pragma mark - init

- (UILabel *)transferNumLab {
    if (!_transferNumLab) {
        _transferNumLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x2974B3) textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _transferNumLab.text = @"划转数量";
    }
    return _transferNumLab;
}

- (UILabel *)poundageLab {
    if (!_poundageLab) {
        _poundageLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _poundageLab.text = @"手续费 4%";
    }
    return _poundageLab;
}

- (UITextField *)transferTF {
    if (!_transferTF) {
        _transferTF = [YQViewFactory textFieldWithPlaceholderText:@"请输入划转数量" textColor:MainBlackColor fontSize:FIT(25) userBold:YES];
//        _transferTF.text = @"300";
    }
    return _transferTF;
}

- (UILabel *)cnyLab {
    if (!_cnyLab) {
        _cnyLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _cnyLab.text = @"CNY";
    }
    return _cnyLab;
}

- (UIView *)lineView_H {
    if (!_lineView_H) {
        _lineView_H = [[UIView alloc] init];
        _lineView_H.backgroundColor = UIColorFromRGB(0xcccccc);
    }
    return _lineView_H;
}

- (UIButton *)commitBtn {
    if (!_commitBtn) {
        _commitBtn = [YQViewFactory buttonWithTitle:@"全部" titleColor:UIColorFromRGB(0x2974B3) fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(showAllCny)];
        _commitBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _commitBtn;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = UIColorFromRGB(0xf5f5f5);
    }
    return _bottomLineView;
}

- (UILabel *)balanceLab {
    if (!_balanceLab) {
        _balanceLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
//        _balanceLab.text = @"可用 3298.8783 CNY";
    }
    return _balanceLab;
}

- (void)showAllCny {
    //全部
    if (self.CommitAllBlock) {
        self.CommitAllBlock();
    }
}

@end
