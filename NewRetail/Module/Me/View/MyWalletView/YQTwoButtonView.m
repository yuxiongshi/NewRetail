//
//  YQTwoButtonView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQTwoButtonView.h"

@implementation YQTwoButtonView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupBottomView];
    }
    return self;
}

- (void)setupBottomView {
    [self addSubview:self.upMoneyBtn];
    [self addSubview:self.withdrawalBtn];
    
    [self.upMoneyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(FIT(80), FIT(50)));
    }];
    
    [self.withdrawalBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(FIT(80), FIT(50)));
    }];
    
}

- (UIButton *)upMoneyBtn {
    if (!_upMoneyBtn) {
        _upMoneyBtn = [YQViewFactory buttonWithTitle:@"充值" titleColor:MainRedTextColor fontSize:FIT(15) userBold:NO target:self sel:@selector(twoBtnAction:)];
        _upMoneyBtn.tag = 200;
    }
    return _upMoneyBtn;
}

- (UIButton *)withdrawalBtn {
    if (!_withdrawalBtn) {
        _withdrawalBtn = [YQViewFactory buttonWithTitle:@"提现" titleColor:MainRedTextColor fontSize:FIT(15) userBold:NO target:self sel:@selector(twoBtnAction:)];
        _withdrawalBtn.tag = 201;
    }
    return _withdrawalBtn;
}

- (void)twoBtnAction:(UIButton *)sender {
    if (self.SelectBlock) {
        self.SelectBlock(sender.tag);
    }
}

@end
