//
//  YQPassCardHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQPassCardHeadView.h"

@implementation YQPassCardHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupPassCardView];
    }
    return self;
}

- (void)setupPassCardView {
    [self addSubview:self.backgroundImg];
    [self.backgroundImg addSubview:self.backBtn];
    [self.backgroundImg addSubview:self.theTitleLab];
    [self.backgroundImg addSubview:self.theTitleOne];
    [self.backgroundImg addSubview:self.balanceLab];
    [self.backgroundImg addSubview:self.rateLab];
    [self.backgroundImg addSubview:self.GBKAndCNYRateLab];
    
    [self.backgroundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(42), NavigationStatusBarHeight-StatusBarHeight));
    }];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(21)));
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backgroundImg.mas_centerX);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(25));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(15)));
    }];
    
    [self.balanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.backgroundImg.mas_centerX);
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(FIT(16));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(30)));
    }];
    
    [self.rateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.balanceLab.mas_bottom).mas_equalTo(FIT(38));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(14)));
    }];
    
    [self.GBKAndCNYRateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.rateLab.mas_bottom).mas_equalTo(FIT(14));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(14)));
    }];
    
}

- (UIImageView *)backgroundImg {
    if (!_backgroundImg) {
        _backgroundImg = [[UIImageView alloc] init];
        _backgroundImg.image = YQ_IMAGE(@"wallet_bg");
        _backgroundImg.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImg.userInteractionEnabled = YES;
    }
    return _backgroundImg;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
        _backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _backBtn;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        _theTitleLab.text = @"我的通证";
    }
    return _theTitleLab;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:YES];
        _theTitleOne.text = @"GBK";
    }
    return _theTitleOne;
}

- (UILabel *)balanceLab {
    if (!_balanceLab) {
        _balanceLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(36) userBold:YES];
        _balanceLab.text = @"￥8,000.00";
    }
    return _balanceLab;
}

- (UILabel *)rateLab {
    if (!_rateLab) {
        _rateLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(15) userBold:NO];
        _rateLab.text = @"交易所汇率 1 GBK≈2.5 CNY";
    }
    return _rateLab;
}

- (UILabel *)GBKAndCNYRateLab {
    if (!_GBKAndCNYRateLab) {
        _GBKAndCNYRateLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(15) userBold:NO];
        _GBKAndCNYRateLab.text = @"新零售抵扣比例 1 GBK≈3.1 CNY";
    }
    return _GBKAndCNYRateLab;
}

- (void)backAction {
    //返回
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
