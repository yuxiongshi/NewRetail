//
//  YQTransferBindingView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQTransferBindingView : UIView

/**
 币种
 */
@property (nonatomic, strong) UILabel *rewardBalanceLab;

/**
 gbk
 */
@property (nonatomic, strong) UILabel *AvailableBalance;

/**
 图片
 */
@property (nonatomic, strong) UIImageView *arrowImg;

@end

NS_ASSUME_NONNULL_END
