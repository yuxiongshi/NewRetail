//
//  YQTransferBindingView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQTransferBindingView.h"

@implementation YQTransferBindingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupHeadView];
    }
    return self;
}

- (void)setupHeadView {
    [self addSubview:self.rewardBalanceLab];
    [self addSubview:self.arrowImg];
    [self addSubview:self.AvailableBalance];
    CGFloat label_W = ScreenWidth/3.0;
    [self.rewardBalanceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(25));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(label_W, FIT(20)));
    }];
    
    [self.AvailableBalance mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(25));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(40), FIT(20)));
    }];
    
    [self.arrowImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.right.equalTo(self.AvailableBalance.mas_left).mas_equalTo(-FIT(10));
        make.size.mas_equalTo(CGSizeMake(FIT(30), FIT(27)));
    }];
    
    
    
}

- (UILabel *)rewardBalanceLab {
    if (!_rewardBalanceLab) {
        _rewardBalanceLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:YES];
        _rewardBalanceLab.text = @"币种";
    }
    return _rewardBalanceLab;
}

- (UIImageView *)arrowImg {
    if (!_arrowImg) {
        _arrowImg = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"gbk_icon")];
    }
    return _arrowImg;
}

- (UILabel *)AvailableBalance {
    if (!_AvailableBalance) {
        _AvailableBalance = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:FIT(18) userBold:YES];
        _AvailableBalance.text = @"GBK";
    }
    return _AvailableBalance;
}

@end
