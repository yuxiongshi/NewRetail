//
//  YQBindingView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQBindingView : UIView

//背景图
@property (nonatomic, strong) UIImageView *backgroundImg;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 title
 */
@property (nonatomic, strong) UILabel *theTitleLab;

@property (nonatomic, copy) void (^BackBlock) (void);

@end

NS_ASSUME_NONNULL_END
