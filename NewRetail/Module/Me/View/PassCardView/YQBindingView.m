//
//  YQBindingView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBindingView.h"

@implementation YQBindingView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupBingdingView];
    }
    return self;
}

- (void)setupBingdingView {
    [self addSubview:self.backgroundImg];
    [self.backgroundImg addSubview:self.backBtn];
    [self.backgroundImg addSubview:self.theTitleLab];
    
    [self.backgroundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(42), NavigationStatusBarHeight-StatusBarHeight));
    }];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(21)));
    }];
    
}

- (UIImageView *)backgroundImg {
    if (!_backgroundImg) {
        _backgroundImg = [[UIImageView alloc] init];
        _backgroundImg.image = YQ_IMAGE(@"bangding");
        _backgroundImg.contentMode = UIViewContentModeScaleAspectFill;
        _backgroundImg.userInteractionEnabled = YES;
    }
    return _backgroundImg;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
        _backBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _backBtn;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        _theTitleLab.text = @"绑定";
    }
    return _theTitleLab;
}

- (void)backAction {
    //返回
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
