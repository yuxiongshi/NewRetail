//
//  YQPassCardHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQPassCardHeadView : UIView

//背景图
@property (nonatomic, strong) UIImageView *backgroundImg;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 title
 */
@property (nonatomic, strong) UILabel *theTitleLab;


/**
 GBK
 */
@property (nonatomic, strong) UILabel *theTitleOne;

/**
 可用余额
 */
@property (nonatomic, strong) UILabel *balanceLab;

/**
 汇率
 */
@property (nonatomic, strong) UILabel *rateLab;

/**
 抵扣比例
 */
@property (nonatomic, strong) UILabel *GBKAndCNYRateLab;

@property (nonatomic, copy) void (^BackBlock) (void);

@end

NS_ASSUME_NONNULL_END
