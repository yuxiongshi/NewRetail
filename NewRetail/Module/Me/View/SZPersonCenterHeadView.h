//
//  SZPersonCenterHeadView.h
//  GBKTrade
//
//  Created by Shizi on 2018/6/7.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^BtnSelectBlock) (void);

@interface SZPersonCenterHeadView : UIView

@property(nonatomic,strong) UIImageView* backgroundImageView;
@property(nonatomic, strong) UIButton* rightBtn;
@property (nonatomic, strong) BtnSelectBlock selectBlock;
@property (nonatomic, strong) UIView *backView;
@property(nonatomic, strong)UIButton* headBtn;//头像
@property (nonatomic, strong) UILabel *nickNameLab;//名称
@property (nonatomic, strong) UILabel *phoneNumLab;//电话号码
@property (nonatomic, strong) UILabel *authStatusLab;//是否完善
@property (nonatomic, strong) UIButton *roleBtn;//角色

@end
