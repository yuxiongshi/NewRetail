//
//  YQMyTeamBottomView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTeamBottomView : YQBaseView

@property (nonatomic, strong) UILabel *desLab;

@property (nonatomic, strong) UIView *middleView;

@property (nonatomic, strong) UILabel *downLoadLab;

@property (nonatomic, strong) UILabel *referralCodeLab;

@property (nonatomic, copy) void (^TeamCopyBlock) (NSInteger tag);

@end

NS_ASSUME_NONNULL_END
