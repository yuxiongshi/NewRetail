//
//  YQInvitationView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQInvitationView.h"

@implementation YQInvitationView

- (void)setupView {
    
    [self addSubview:self.backImgView];
    [self.backImgView addSubview:self.backBtn];
    [self.backImgView addSubview:self.theTitleOne];
    
    
    [self.backImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(StatusBarHeight);
        make.size.mas_equalTo(CGSizeMake(FIT(44), FIT(44)));
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.centerY.mas_equalTo(self.backBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(18)));
    }];
}



- (UIImageView *)backImgView {
    if (!_backImgView) {
        _backImgView = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"Invite")];
        _backImgView.userInteractionEnabled = YES;
    }
    return _backImgView;
}

- (UIButton *)backBtn {
    if (!_backBtn) {
        _backBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"back") target:self sel:@selector(backAction)];
    }
    return _backBtn;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(19) userBold:NO];
        _theTitleOne.text = @"邀请好友";
    }
    return _theTitleOne;
}

- (void)backAction {
    if (self.BackBlock) {
        self.BackBlock();
    }
}

@end
