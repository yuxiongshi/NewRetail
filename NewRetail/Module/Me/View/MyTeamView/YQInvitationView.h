//
//  YQInvitationView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQInvitationView : YQBaseView

/**
 背景图
 */
@property (nonatomic, strong) UIImageView *backImgView;

/**
 返回按钮
 */
@property (nonatomic, strong) UIButton *backBtn;

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleOne;

@property (nonatomic, copy) void (^BackBlock) (void);


@end

NS_ASSUME_NONNULL_END
