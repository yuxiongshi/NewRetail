//
//  YQMyTeamBottomView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTeamBottomView.h"
#import "YQDrawRectView.h"

@interface YQMyTeamBottomView ()

@property (nonatomic, strong) YQDrawRectView *backView;

@end

@implementation YQMyTeamBottomView

-  (void)setupView {
    [self addSubview:self.desLab];
    [self addSubview:self.middleView];
    
    [_desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(25));
        make.right.mas_equalTo(-FIT(25));
        make.top.mas_equalTo(FIT(40));
        make.height.mas_equalTo(FIT(50));
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.desLab.mas_bottom).mas_equalTo(FIT(40));
        make.height.mas_equalTo(FIT(12));
    }];
    
    CGFloat button_H = FIT(50);
    CGFloat ButtonSpace = FIT(25);
    for (NSInteger i = 0; i < 2; i++) {
        UIButton *twoBtn = [YQViewFactory buttonWithTitle:@"复制" titleColor:WhiteColor fontSize:FIT(18) userBold:NO target:self sel:@selector(teamCopyAction:)];
        twoBtn.tag = 10+i;
        [twoBtn setBackgroundImage:YQ_IMAGE(@"right_copy_btn") forState:UIControlStateNormal];
        [self addSubview:twoBtn];
        [twoBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.middleView.mas_bottom).mas_equalTo(ButtonSpace*(i+1)+button_H*i);
            make.right.mas_equalTo(-kMargin_right);
            make.size.mas_equalTo(CGSizeMake(FIT(100), button_H));
        }];
        
        self.backView = [[YQDrawRectView alloc] init];
        self.backView.backgroundColor = UIColorFromRGB(0xf5f5f5);
        [self addSubview:self.backView];
        [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kMargin_left);
            make.top.mas_equalTo(twoBtn.mas_top);
            make.right.equalTo(twoBtn.mas_left).mas_equalTo(8);
            make.height.mas_equalTo(button_H);
        }];
        
        UILabel *twoLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:NO];
        [self.backView addSubview:twoLab];
        [twoLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kMargin_left);
            make.right.mas_equalTo(-kMargin_right);
            make.top.bottom.mas_equalTo(0);
        }];
        if (i == 0) {
            self.downLoadLab = twoLab;
        }else {
            self.referralCodeLab = twoLab;
        }
    }
    
}

- (void)teamCopyAction:(UIButton *)sender {
    if (self.TeamCopyBlock) {
        self.TeamCopyBlock(sender.tag);
    }
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x808080) textAlignment:NSTextAlignmentLeft fontSize:FIT(14) userBold:NO];
        _desLab.numberOfLines = 0;
        _desLab.text = @"奖励每天23点至24点之间会结算一次，并发放至“我的钱包”；每天结算奖励后会对等级进行一次升级判断。";
    }
    return _desLab;
}

- (UIView *)middleView {
    if (!_middleView) {
        _middleView = [[UIView alloc] init];
        _middleView.backgroundColor = UIColorFromRGB(0xf5f5f5);
    }
    return _middleView;
}

@end
