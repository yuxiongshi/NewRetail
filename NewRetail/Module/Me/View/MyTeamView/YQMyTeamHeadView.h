//
//  YQMyTeamHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTeamHeadView : YQBaseView

/**
 背景
 */
@property (nonatomic, strong) UIImageView *headIcon;

/**
  直接推荐
 */
@property (nonatomic, strong) UILabel *zjRecommended;

/**
 间接推荐
 */
@property (nonatomic, strong) UILabel *indirectRecommended;

/**
 我的等级
 */
@property (nonatomic, strong) UILabel *myLevel;

@property (nonatomic, strong) UILabel *theTitleOne;
@property (nonatomic, strong) UILabel *theTitleTwo;
@property (nonatomic, strong) UILabel *theTitleThree;

@end

NS_ASSUME_NONNULL_END
