//
//  YQMyTeamHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTeamHeadView.h"

@implementation YQMyTeamHeadView

- (void)setupView {
    self.backgroundColor = WhiteColor;
    [self addSubview:self.headIcon];
    [self.headIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    NSArray *titleArr = @[@"直接推荐",@"间接推荐",@"我的等级"];
    CGFloat kLabel_With = ScreenWidth/3.0;
    for (NSInteger i = 0; i < 3; i++) {
        UILabel *lab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(18) userBold:YES];
        lab.text = titleArr[i];
        [self.headIcon addSubview:lab];
        [lab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLabel_With*i);
            make.top.mas_equalTo(FIT(45));
            make.size.mas_equalTo(CGSizeMake(kLabel_With, FIT(18)));
        }];
        
        UILabel *contentLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentCenter fontSize:FIT(25) userBold:YES];
        [self.headIcon addSubview:contentLab];
        contentLab.text = @"6人";
        if (i == 0) {
            self.zjRecommended = contentLab;
        }else if (i == 1) {
            self.indirectRecommended = contentLab;
        }else {
            self.myLevel = contentLab;
        }
        [contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(kLabel_With*i);
            make.top.equalTo(lab.mas_bottom).mas_equalTo(FIT(14));
            make.size.mas_equalTo(CGSizeMake(kLabel_With, FIT(24)));
        }];
        
    }
    
}

- (UIImageView *)headIcon {
    if (!_headIcon) {
        _headIcon = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"team_bg")];
        _headIcon.userInteractionEnabled = YES;
        _headIcon.contentMode = UIViewContentModeScaleToFill;
    }
    return _headIcon;
}

@end
