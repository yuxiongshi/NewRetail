//
//  YQAboutUsHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQAboutUsHeadView.h"

@interface YQAboutUsHeadView ()

@property (nonatomic, strong) UIImageView *appIconBtn;
@property (nonatomic, strong) UILabel *appNameLab;



@end

@implementation YQAboutUsHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = TableViewBackGroundColor;
        [self setupHeadBackView];
    }
    return self;
}

- (void)setupHeadBackView {
    
    [self addSubview:self.appIconBtn];
    [self addSubview:self.appNameLab];
    [self addSubview:self.describeLab];
    
    
    [self.appIconBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.mas_equalTo(FIT(35));
        make.size.mas_equalTo(CGSizeMake(FIT(60), FIT(60)));
    }];
    
    [self.appNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.appIconBtn.mas_bottom).mas_equalTo(FIT(10));
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(15)));
    }];
    
    [self.describeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(40));
        make.top.equalTo(self.appNameLab.mas_bottom).mas_equalTo(FIT(20));
        make.right.mas_equalTo(-FIT(40));
        make.height.mas_equalTo([UILabel getLabelHeightWithText:self.describeLab.text width:ScreenWidth-FIT(80) font:kDescribeFont]);
    }];
    
}

- (UIImageView *)appIconBtn {
    if (!_appIconBtn) {
        _appIconBtn = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"app_icon")];
        _appIconBtn.layer.cornerRadius = 3;
        _appIconBtn.layer.masksToBounds = YES;
    }
    return _appIconBtn;
}

- (UILabel *)appNameLab {
    if (!_appNameLab) {
        _appNameLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _appNameLab.text = @"天赐艾";
    }
    return _appNameLab;
}

- (UILabel *)describeLab {
    if (!_describeLab) {
        _describeLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x808080) textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _describeLab.numberOfLines = 0;
        _describeLab.text = @"天赐艾是一个把互联网实物消费和互联网金融理财相互结合的新零售平台，同事引进区块链通证技术，让通证权益和购物权益进行结合。愉快的购物，轻松的赚钱";
    }
    return _describeLab;
}


@end
