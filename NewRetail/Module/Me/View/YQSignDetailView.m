//
//  YQSignDetailView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQSignDetailView.h"

@implementation YQSignDetailView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupDetailView];
    }
    return self;
}

- (void)setupDetailView {
    [self addSubview:self.desLab];
    [self addSubview:self.createTimeLab];
    
    [self.createTimeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(25));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_right, FIT(40)));
    }];
    
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _desLab.numberOfLines = 0;
    }
    return _desLab;
}

- (UILabel *)createTimeLab {
    if (!_createTimeLab) {
        _createTimeLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _createTimeLab.numberOfLines = 2;
    }
    return _createTimeLab;
}

- (void)loadDetailViewWithModel:(YQSysSignModel *)model {
    self.createTimeLab.text = [NSString stringWithFormat:@"天赐艾\n%@",[NSString getTimeFromTimestampWithTime:model.createTime formatter:@"yyyy-mm-dd HH:mm:ss"]];
    self.desLab.text = model.content;
    [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(FIT(20));
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(80));
//        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, [UILabel getLabelHeightWithText:model.content width:ScreenWidth-2*kMargin_left font:kDescribeFont]));
    }];
}

@end
