//
//  SZPersonCenterHeadView.m
//  GBKTrade
//
//  Created by Shizi on 2018/6/7.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "SZPersonCenterHeadView.h"
#import "UIButton+WebCache.h"
@interface SZPersonCenterHeadView ()

@property(nonatomic, strong)UILabel* promptLab;

@end

@implementation SZPersonCenterHeadView



- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setSubView];
    }
    return self;
}

-(void)setSubView{
    
    UIImageView* backgroundImageView = [[UIImageView alloc]init];
    backgroundImageView.image=[UIImage imageNamed:@"me_head_icon"];
    backgroundImageView.contentMode=UIViewContentModeScaleAspectFill;
    backgroundImageView.clipsToBounds=YES;
    backgroundImageView.userInteractionEnabled=YES;
    self.backgroundImageView=backgroundImageView;
    [self addSubview:backgroundImageView];
    [backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(0));
        make.top.mas_equalTo(FIT(0));
        make.width.mas_equalTo(UIScreenWidth);
        make.height.mas_equalTo(FIT(81)+NavigationStatusBarHeight);
        
    }];
    
    
    UIButton* headerBtn=[UIButton new];
    [backgroundImageView addSubview:headerBtn];
    self.headBtn=headerBtn;
    [headerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(backgroundImageView.mas_centerY);
        make.width.height.mas_equalTo(FIT(60));
        make.left.mas_equalTo(FIT(18));

    }];
    [self.headBtn setEnlargeEdgeWithTop:0 right:FIT3(400)+FIT3(49) bottom:0 left:0];
    self.headBtn.layer.cornerRadius = FIT(30);
    self.headBtn.layer.masksToBounds = YES;
    
    //角色
    UIButton *roleBtn = [YQViewFactory buttonWithTitle:@"" titleColor:WhiteColor fontSize:FIT(12) userBold:NO target:self sel:@selector(noSelectAction)];
    [backgroundImageView addSubview:roleBtn];
    [roleBtn setBackgroundColor:UIColorFromRGB(0xF5BE60)];
    roleBtn.layer.cornerRadius = FIT(18)/2.0;
    self.roleBtn = roleBtn;
    [roleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headBtn.mas_right).mas_equalTo(-FIT(30));
        make.bottom.mas_equalTo(self.headBtn.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(FIT(60), FIT(18)));
    }];
    

    UILabel *nickNameLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentLeft fontSize:FIT(20) userBold:NO];
    [backgroundImageView addSubview:nickNameLab];
    self.nickNameLab = nickNameLab;
    [nickNameLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headBtn.mas_right).mas_equalTo(FIT(30));
        make.right.mas_equalTo(-FIT(30));
        make.top.mas_equalTo(self.headBtn.mas_top);
        make.height.mas_equalTo(FIT(20));
    }];
//
    UILabel* promptLab=[UILabel new];
    promptLab.textColor = WhiteColor;
    promptLab.font = YQ_Font(kDescribeFont);
    promptLab.text=NSLocalizedString(@"登录后查看更多信息", nil);
    self.phoneNumLab = promptLab;
    [backgroundImageView addSubview:promptLab];
    [promptLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerBtn.mas_right).offset(FIT(30));
        make.right.mas_equalTo(-FIT(30));
        make.bottom.equalTo(self.headBtn.mas_bottom).mas_equalTo(-FIT(10));
        make.height.mas_equalTo(FIT(20));
    }];
    

    UIButton* rightBtn=[[UIButton alloc]init];
    [rightBtn setImage:[UIImage imageNamed:@"push_white"] forState:UIControlStateNormal];
    self.rightBtn=rightBtn;
    [backgroundImageView addSubview:rightBtn];
    [rightBtn setEnlargeEdgeWithTop:FIT(20.5) right:FIT(20) bottom:FIT(20.5) left:FIT(20)];
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(FIT(-16));
        make.centerY.equalTo(headerBtn.mas_centerY);
        make.height.mas_equalTo(FIT(25));
        make.width.mas_equalTo(FIT(25));
    }];
    [rightBtn addTarget:self action:@selector(pushNextVC) forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *authStatus = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentRight fontSize:FIT(15) userBold:NO];
    [backgroundImageView addSubview:authStatus];
    self.authStatusLab = authStatus;
    [authStatus mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(rightBtn.mas_left).mas_equalTo(-FIT(10));
        make.centerY.equalTo(headerBtn.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(50), FIT(20)));
    }];
    
  //开发同事注意：个人资料页面未实名认证、未设置交易密码、未添加收货地址、未添加付款与收款时，该处会一直显示“待完善”
    
    if ([InfoModelShare.isPaymentWay boolValue] == true
        && [InfoModelShare.isTradePassword boolValue] == true
        && [InfoModelShare.isSetAddress boolValue] == true
        && [InfoModelShare.authStatus integerValue] == 3) {
        authStatus.text = @"";
    }else {
        authStatus.text = @"待完善";
    }
    
}

- (void)noSelectAction {
    //不点
}

- (void)pushNextVC {
    if (self.selectBlock) {
        self.selectBlock();
    }
}

//-  (void)setInfoModel:(YQPersonInformationModel *)infoModel {
//    [self.headBtn sd_setImageWithURL:[NSURL URLWithString:infoModel.avatar] forState:UIControlStateNormal placeholderImage:Head_PlaceHolder];
//    self.loginNameLab.text = infoModel.phoneNum;
//    [self.loginBtn setTitle:infoModel.nickName forState:UIControlStateNormal];
//}


@end
