//
//  YQAboutUsHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQAboutUsHeadView : UIView

@property (nonatomic, strong) UILabel *describeLab;

@end

NS_ASSUME_NONNULL_END
