//
//  YQSignDetailView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YQSysSignModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQSignDetailView : UIView

/**
  详情描述
 */
@property (nonatomic, strong) UILabel *desLab;

/**
  发布时间
 */
@property (nonatomic, strong) UILabel *createTimeLab;

- (void)loadDetailViewWithModel:(YQSysSignModel *)model;

@end

NS_ASSUME_NONNULL_END
