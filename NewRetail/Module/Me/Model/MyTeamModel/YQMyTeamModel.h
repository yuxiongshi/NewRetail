//
//  YQMyTeamModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTeamModel : NSObject

/**
 直推奖
 */
@property (nonatomic, copy) NSString *directBonus;

/**
 间接奖
 */
@property (nonatomic, copy) NSString *inDirectBonus;

/**
 辅导奖
 */
@property (nonatomic, copy) NSString *coachBonus;

/**
 业绩奖
 */
@property (nonatomic, copy) NSString *achievementBonus;

/**
 管理奖
 */
@property (nonatomic, copy) NSString *administrationBonus;

/**
 全部
 */
@property (nonatomic, copy) NSString *allBonus;

/**
 角色 1:店员,2:主管,3:店长,4:区域代理,5:市代理
 */
@property (nonatomic, copy) NSString *role;

/**
 直接推荐
 */
@property (nonatomic, copy) NSString *fristFriend;

/**
 间接推荐
 */
@property (nonatomic, copy) NSString *secondFriend;

/**
 推荐地址
 */
@property (nonatomic, copy) NSString *downloadUrl;


@end

NS_ASSUME_NONNULL_END
