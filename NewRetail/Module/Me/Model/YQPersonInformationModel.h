//
//  YQPersonInformationModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQPersonInformationModel : NSObject

DEFINE_SINGLETON_FOR_HEADER(YQPersonInformationModel)

/**
 电话号码
 */
@property (nonatomic, copy) NSString *phoneNum;

/**
 nickName
 */
@property (nonatomic, copy) NSString *nickName;

/**
 loginId
 */
@property (nonatomic, copy) NSString *loginId;

/**
 是否设置地址
 */
@property (nonatomic, copy) NSString *isSetAddress;

/**
 头像
 */
@property (nonatomic, copy) NSString *avatar;

/**
 身份认证  实名认证状态（0：未认证，1：认证中，2：认证失败，3，认证通过）
 */
@property (nonatomic, copy) NSString *authStatus;

/**
 是否设置支付方式
 */
@property (nonatomic, copy) NSString *isPaymentWay;

/**
 是否设置交易密码
 */
@property (nonatomic, copy) NSString *isTradePassword;

/**
 用户等级
 */
@property (nonatomic, copy) NSString *role;

/**
 邀请码
 */
@property (nonatomic, copy) NSString *referralCode;

@end

NS_ASSUME_NONNULL_END
