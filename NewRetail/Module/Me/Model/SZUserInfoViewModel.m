//
//  SZUserInfoViewModel.m
//  GBKTrade
//
//  Created by sumrain on 2018/7/27.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "SZUserInfoViewModel.h"

@interface SZUserInfoViewModel()

@end


@implementation SZUserInfoViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.titleArr=@[
                        @[NSLocalizedString(@"头像", nil)],
                        @[NSLocalizedString(@"用户名", nil),NSLocalizedString(@"手机号码", nil)],
                        @[NSLocalizedString(@"身份认证", nil),NSLocalizedString(@"登录密码", nil),NSLocalizedString(@"交易密码", nil),NSLocalizedString(@"收货地址", nil),NSLocalizedString(@"付款与收款", nil)]];
    }
    return self;
}

@end

