//
//  YQSysSignModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQSysSignModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 title
 */
@property (nonatomic, copy) NSString *title;

/**
 content
 */
@property (nonatomic, copy) NSString *content;

/**
 createTime
 */
@property (nonatomic, copy) NSString *createTime;

/**
 updateTime
 */
@property (nonatomic, copy) NSString *updateTime;

/**
 remark
 */
@property (nonatomic, copy) NSString *remark;

@end

NS_ASSUME_NONNULL_END
