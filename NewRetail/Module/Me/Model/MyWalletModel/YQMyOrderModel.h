//
//  YQMyOrderModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 userId
 */
@property (nonatomic, copy) NSString *userId;

/**
 cashType
 */
@property (nonatomic, copy) NSString *cashType;

/**
 支付方式
 */
@property (nonatomic, copy) NSString *payWayId;

/**
 订单号
 */
@property (nonatomic, copy) NSString *orderNum;

/**
 预充值金额
 */
@property (nonatomic, copy) NSString *cashNum;

/**
 1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
 */
@property (nonatomic, copy) NSString *orderStatus;

/**
 实际到账金额
 */
@property (nonatomic, copy) NSString *realCnyNum;

/**
 取消时间
 */
@property (nonatomic, copy) NSString *cancelTime;

/**
 创建时间
 */
@property (nonatomic, copy) NSString *createTime;

@end

NS_ASSUME_NONNULL_END
