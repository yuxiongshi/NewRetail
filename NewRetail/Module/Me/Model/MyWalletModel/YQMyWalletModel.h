//
//  YQMyWalletModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyWalletModel : NSObject

/**
 余额
 */
@property (nonatomic, copy) NSString *balance;

/**
 冻结余额
 */
@property (nonatomic, copy) NSString *freezeBalance;

/**
 健康基金
 */
@property (nonatomic, copy) NSString *healthBalance;

/**
 提现冻结
 */
@property (nonatomic, copy) NSString *frozenCashBal;

/**
 GBK余额
 */
@property (nonatomic, copy) NSString *gbkBalance;

/**
 新零售的汇率
 */
@property (nonatomic, copy) NSString *gbkCNYRate;

/**
 交易所汇率
 */
@property (nonatomic, copy) NSString *tradeGBKToCNYRate;

/**
 手续费
 */
@property (nonatomic, copy) NSString *serviceCharge;

/**
 奖励
 */
@property (nonatomic, copy) NSString *rewardBalance;


@end

NS_ASSUME_NONNULL_END
