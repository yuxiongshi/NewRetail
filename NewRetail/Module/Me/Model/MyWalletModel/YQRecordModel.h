//
//  YQRecordModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQRecordModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 userId
 */
@property (nonatomic, copy) NSString *userId;

/**
 金额
 */
@property (nonatomic, copy) NSString *amount;

/**
 opType
 */
@property (nonatomic, copy) NSString *opType;

/**
 收支用途
 */
@property (nonatomic, copy) NSString *transType;

/**
 收支用途
 */
@property (nonatomic, copy) NSString *remark;

/**
 createTime
 */
@property (nonatomic, copy) NSString *createTime;

/**
 createBy
 */
@property (nonatomic, copy) NSString *createBy;

@end

NS_ASSUME_NONNULL_END
