//
//  YQMyOrderDetailModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderDetailModel : NSObject

/**
 Id
 */
@property (nonatomic, copy) NSString *id;

/**
 真实姓名
 */
@property (nonatomic, copy) NSString *bankRealName;

/**
 银行名称
 */
@property (nonatomic, copy) NSString *bankName;

/**
 银行卡号
 */
@property (nonatomic, copy) NSString *bankNum;

/**
 备注
 */
@property (nonatomic, copy) NSString *remark;

/**
 银行地址
 */
@property (nonatomic, copy) NSString *bankAddress;

/**
 支付宝姓名
 */
@property (nonatomic, copy) NSString *alipayName;

/**
 支付宝账号
 */
@property (nonatomic, copy) NSString *alipayAccount;

/**
 支付宝收款码
 */
@property (nonatomic, copy) NSString *alipayUrl;

/**
 微信姓名
 */
@property (nonatomic, copy) NSString *wechatName;

/**
 微信账号
 */
@property (nonatomic, copy) NSString *wechatAccount;

/**
 微信收款码
 */
@property (nonatomic, copy) NSString *wechatUrl;

/**
 预充值金额
 */
@property (nonatomic, copy) NSString *cashNum;

/**
 支付方式
 */
@property (nonatomic, copy) NSString *paymentWay;

/**
 公司名称
 */
@property (nonatomic, copy) NSString *companyAccountName;

/**
 公司银行卡账号
 */
@property (nonatomic, copy) NSString *companyBankNum;

/**
 公司支付宝账号
 */
@property (nonatomic, copy) NSString *companyAliAccount;

/**
 公司银行卡名称
 */
@property (nonatomic, copy) NSString *companyBankName;

/**
 公司银行卡地址
 */
@property (nonatomic, copy) NSString *companyBankAddress;

/**
 公司支付宝收款码
 */
@property (nonatomic, copy) NSString *companyAliUrl;

/**
 公司微信账号
 */
@property (nonatomic, copy) NSString *companyWechatAccount;

/**
 公司微信收款码
 */
@property (nonatomic, copy) NSString *companyWechatUrl;

/**
 1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
 */
@property (nonatomic, copy) NSString *orderStatus;

/**
 实际到账金额
 */
@property (nonatomic, copy) NSString *realCNYNum;

/**
 取消时间
 */
@property (nonatomic, copy) NSString *cancelTime;

/**
 完成时间
 */
@property (nonatomic, copy) NSString *updateTime;

/**
 创建时间
 */
@property (nonatomic, copy) NSString *createTime;

/**
 服务费
 */
@property (nonatomic, copy) NSString *serviceChange;

/**
 订单号
 */
@property (nonatomic, copy) NSString *orderNum;


@end

NS_ASSUME_NONNULL_END
