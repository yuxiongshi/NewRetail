//
//  PhotoChooseView.h
//  GBKTrade
//
//  Created by admin on 2019/4/24.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^SelectedBtnActionBlock) (NSInteger tag);
typedef void (^AssignmentBtnBlock) (NSString * _Nonnull urlStr,NSInteger tag);

NS_ASSUME_NONNULL_BEGIN

@interface PhotoChooseView : UIView

@property (strong, nonatomic) UIButton *positivePhotoBtn;//上传正面照片
@property (strong, nonatomic) UIButton *reversePhotoBtn;//上传反面照片
@property (strong, nonatomic) UIButton *handheldPhotoBtn;//上传手持正面照片
@property (strong, nonatomic) UIButton *titleBtn;//文字表述
@property (strong, nonatomic) UIButton *commitBtn;

@property (copy, nonatomic) SelectedBtnActionBlock btnBlock;
@property (copy, nonatomic) AssignmentBtnBlock urlBlock;


@end

NS_ASSUME_NONNULL_END
