//
//  BaseTableView.m
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "BaseTableView.h"
#import "ResetPWDCell.h"

@interface BaseTableView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *resetTB;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *placeArr;
@property (nonatomic, strong) NSMutableArray *valueArr;

@property (nonatomic, strong) UITextField *pwdTF;
@property (nonatomic, strong) UITextField *surePwdTF;
@property (nonatomic, strong) UITextField *codeTF;


@end

@implementation BaseTableView
{
    NSString *SMSCodeStr;
    NSString *setPWD;
    NSString *surePWD;
    NSString *titleStr;//区分入口
}
- (NSArray *)titleArr {
    if (!_titleArr) {
        _titleArr = @[@[@"设置密码："],@[@"确认密码："],@[@"短信验证码："]];
    }
    return _titleArr;
}

- (NSArray *)placeArr {
    if (!_placeArr) {
        _placeArr = @[@[@"输入6-16位数字与字母组合"],@[@"确认输入的密码一致"],@[@"输入6位数验证码"]];
    }
    return _placeArr;
}

- (NSMutableArray *)valueArr {
    if (!_valueArr) {
        _valueArr = [NSMutableArray arrayWithObjects:@"0",@"0",@"0", nil];
    }
    return _valueArr;
}

- (void)distinguishWithString:(NSString *)titleString {
    titleStr = titleString;
    [self.resetTB reloadData];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupUIWithCell];
    }
    return self;
}

- (void)setupUIWithCell{
    if (!_resetTB) {
        _resetTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _resetTB.scrollEnabled = NO;
        _resetTB.delegate = self;
        _resetTB.dataSource = self;
        _resetTB.sectionHeaderHeight = FIT(15);
        _resetTB.sectionFooterHeight = FIT(0);
        _resetTB.tableFooterView = [[UIView alloc] init];
        [_resetTB registerNib:[UINib nibWithNibName:@"ResetPWDCell" bundle:nil] forCellReuseIdentifier:@"ResetPWDCell"];
        [self addSubview:_resetTB];
        [_resetTB mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
            make.leading.trailing.bottom.equalTo(self);
        }];
    }
    
    UIButton *commitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [commitBtn setBtnWithTitle:@"提交" withBackGroundColor:@"0xE13134"];
    [commitBtn setNormalBtnWithBorderColor:@"" withcornerRadius:5.f];
    [_resetTB addSubview:commitBtn];
    [commitBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_offset(@16);
        make.width.mas_offset(@(ScreenWidth-32));
        make.height.mas_equalTo(kCommitButtonHeight);
        make.top.mas_offset(@(50*4+40));
    }];
    [commitBtn addTarget:self action:@selector(commitAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)commitAction {
    //首先判断空  再判断设置的密码是否与确认密码相同
//    NSString *valueStr;
//    for (int i = 0;i < self.valueArr.count;i++) {
//        valueStr = self.valueArr[i];
//        if ([valueStr isEqualToString:@"0"] || isEmptyString(valueStr)) {
//            [LCProgressHUD showMessage:@"请认真填写每一项"];
//            return;
//        }
//   }
    
    if (isEmptyString(self.pwdTF.text)) {
        [LCProgressHUD showMessage:@"请设置密码"];
        return;
    }
    
    //判断是否包含数字+英文
    BOOL isRight = [LimitTextFieldNum judgePassWordLegal:self.pwdTF.text];
    if (!isRight) {
        [LCProgressHUD showMessage:@"输入6-16位数字与字母组合"];
        return;
    }
    
    if (isEmptyString(self.surePwdTF.text)) {
        [LCProgressHUD showMessage:@"请重复输入密码"];
        return;
    }
    
    //确认密码
    if (![self.surePwdTF.text isEqualToString:self.pwdTF.text]) {
        [LCProgressHUD showMessage:@"密码不一致，请重新填写"];
        return;
    }
    
    if (isEmptyString(self.codeTF.text)) {
        [LCProgressHUD showMessage:@"请输入验证码"];
        return;
    }
    
    NSDictionary *paramters;
    NSString *urlStr;
    if ([titleStr integerValue] == 2) {
        //交易密码
        paramters = @{@"messageCode":self.codeTF.text,@"tradepassword":[AppUtil md5:self.pwdTF.text]};
        urlStr = [NSString stringWithFormat:@"%@users/tradepassword",BaseHttpUrl];
    }else {
        paramters = @{@"messageCode":self.codeTF.text,@"password":[AppUtil md5:self.pwdTF.text]};
        urlStr = [NSString stringWithFormat:@"%@users/password",BaseHttpUrl];
    }
    
    [[SZHTTPSRsqManager sharedSZHTTPSRsqManager] postRequestWithUrlString:urlStr appendParameters:nil bodyParameters:paramters successBlock:^(id responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            //请求成功
            if (self.pushBlock) {
                self.pushBlock();
            }
        }else if ([responseObject[@"code"] integerValue] == -1) {
            [LCProgressHUD showMessage:responseObject[@"msg"]];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.titleArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.titleArr[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(50);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(15);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ResetPWDCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ResetPWDCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell loadCellWithDesLab:self.titleArr placeArr:self.placeArr withIndex:indexPath codeType:titleStr];
    if (indexPath.section == 0) {
        self.pwdTF = cell.inputTF;
    }else if (indexPath.section == 1) {
        self.surePwdTF = cell.inputTF;
    }else {
        self.codeTF = cell.inputTF;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
