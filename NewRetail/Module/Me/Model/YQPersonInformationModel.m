//
//  YQPersonInformationModel.m
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQPersonInformationModel.h"

@implementation YQPersonInformationModel

DEFINE_SINGLETON_FOR_CLASS(YQPersonInformationModel)

-(NSString *)phoneNum{
    
    if ([_phoneNum isMobliePhone]) {
        return [_phoneNum stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    return _phoneNum;
    
}

@end
