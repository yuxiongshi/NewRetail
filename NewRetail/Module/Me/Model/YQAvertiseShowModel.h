//
//  YQAvertiseShowModel.h
//  GBKTrade
//
//  Created by admin on 2019/5/15.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQAvertiseShowModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 用户ID
 */
@property (nonatomic,copy) NSString *userId;

/**
 收款方式（1：微信支付，2：支付宝支付，3：银行卡支付）
 */
@property (nonatomic,copy) NSString *paymentWay;

/**
 支付方式真实姓名
 */
@property (nonatomic,copy) NSString *realName;

/**
 支付宝或微信收款账号
 */
@property (nonatomic,copy) NSString *payAccount;

/**
 收款码图片URL
 */
@property (nonatomic,copy) NSString *pictureUrl;

/**
 银行卡开户人姓名
 */
@property (nonatomic,copy) NSString *bankUserName;

/**
 银行类型
 */
@property (nonatomic,copy) NSString *bankName;

/**
 支行地址
 */
@property (nonatomic,copy) NSString *bankAddress;

/**
 银行卡帐号
 */
@property (nonatomic,copy) NSString *bankNum;

/**
 创建时间
 */
@property (nonatomic,copy) NSString *createTime;

/**
 更新时间
 */
@property (nonatomic,copy) NSString *updateTime;

/**
 
 */
@property (nonatomic,copy) NSString *remark;

@end

NS_ASSUME_NONNULL_END
