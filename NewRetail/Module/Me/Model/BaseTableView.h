//
//  BaseTableView.h
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^CommitBlock) (NSDictionary * _Nullable paramters);

typedef void (^PushCtlBlock) (void);

NS_ASSUME_NONNULL_BEGIN

@interface BaseTableView : UIView

@property (nonatomic, copy) CommitBlock block;
@property (nonatomic, copy) PushCtlBlock pushBlock;

- (void)distinguishWithString:(NSString *)titleString;
@end

NS_ASSUME_NONNULL_END
