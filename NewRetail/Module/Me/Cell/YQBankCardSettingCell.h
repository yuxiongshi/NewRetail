//
//  YQBankCardSettingCell.h
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YQAvertiseShowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQBankCardSettingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *bankLab;
@property (weak, nonatomic) IBOutlet UITextField *describtionLab;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@property (strong, nonatomic) QCCountdownButton *sendBtn;
//@property (nonatomic, strong) NSTimer *timers;

//设置银行卡的控制器
- (void)loadCellDataWithDataArr:(NSArray *)dataArr withPlaceHoderArr:(NSArray *)placeArr withShowModel:(YQAvertiseShowModel *)model IndexPath:(NSIndexPath *)indexPath;



@end

NS_ASSUME_NONNULL_END
