//
//  ResetPWDCell.m
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "ResetPWDCell.h"
@interface ResetPWDCell ()<UITextFieldDelegate>

@property (nonatomic, strong) NSTimer *timers;
@property (nonatomic, assign) NSInteger second;
@property (nonatomic, copy) NSString *codeType;

@end

@implementation ResetPWDCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.describeLab.adjustsFontSizeToFitWidth = YES;
    self.SMSCode.layer.borderWidth = 0.5;
    self.SMSCode.layer.borderColor = UIColorFromRGB(0x355191).CGColor;
    self.SMSCode.layer.masksToBounds = YES;
    [self.SMSCode addTarget:self action:@selector(reservedMsg) forControlEvents:UIControlEventTouchUpInside];
}

- (void)reservedMsg {
    NSString *urlStr = [NSString stringWithFormat:@"%@users/tokenMsg",BaseHttpUrl];
    [[SZHTTPSRsqManager sharedSZHTTPSRsqManager] getRequestWithUrlString:urlStr appendParameters:@{@"msgType":[self.codeType integerValue] == 1?@"2":@"5"} successBlock:^(id responseObject) {
        
    } failureBlock:^(NSError *error) {
        
    }];
    //点击之后倒计时
    _second = 59;
    _timers = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeFieMethod) userInfo:nil repeats:YES];
}

- (void)timeFieMethod {
    [self.SMSCode setTitle:[NSString stringWithFormat:@"%ld",_second] forState:UIControlStateNormal];
    _second--;
    if (_second == 0) {
        [_timers invalidate];
        [self.SMSCode setTitle:@"短信验证码" forState:UIControlStateNormal];
    }
}

- (void)loadCellWithDesLab:(NSArray *)titleArr placeArr:(NSArray *)placeArr withIndex:(NSIndexPath *)indexPath codeType:(nonnull NSString *)titleStr
{
    self.codeType = titleStr;
    self.describeLab.text = titleArr[indexPath.section][indexPath.row];
    self.inputTF.placeholder = placeArr[indexPath.section][indexPath.row];
    self.inputTF.delegate = self;
    self.inputTF.tag = 100+indexPath.section;
    [self.SMSCode setHidden:YES];
    //设置输入限制
    if (indexPath.section == 0 || indexPath.section == 1) {
        self.inputTF.keyboardType = UIKeyboardTypeDefault;
        self.inputTF.secureTextEntry = YES;
        //更新输入框的宽度
        [self.inputTF mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.describeLab.mas_right).mas_equalTo(0);
            make.right.mas_equalTo(-kMargin_right);
            make.centerY.mas_equalTo(self.mas_centerY);
            make.height.mas_equalTo(FIT(50));
        }];
        [self.inputTF layoutIfNeeded];
    }else {
        [self.SMSCode setHidden:NO];
        self.inputTF.keyboardType = UIKeyboardTypeNumberPad;
        
    }
    [self.inputTF addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
}

- (void)valueChanged:(UITextField *)textField {
    
    if (textField.tag == 100 || textField.tag == 101) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:16];
    }else if (textField.tag == 102) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:6];
    }
}


- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.valueBlock) {
        self.valueBlock(textField.tag, textField.text);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
