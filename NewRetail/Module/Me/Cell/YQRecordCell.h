//
//  YQRecordCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//


#define YQ_RecordCell @"YQRecordCell"
#import "YQCustomCell.h"
#import "YQRecordModel.h"

NS_ASSUME_NONNULL_BEGIN


@interface YQRecordCell : YQCustomCell

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 time
 */
@property (nonatomic, strong) UILabel *timeLab;

/**
 是否完成
 */
@property (nonatomic, strong) UILabel *commitStatusLab;

/**
  金额
 */
@property (nonatomic, strong) UILabel *commitMoneyLab;

- (void)loadDataWithModel:(YQRecordModel *)model;

- (void)loadPassCardWithModel:(YQRecordModel *)model;

@end

NS_ASSUME_NONNULL_END
