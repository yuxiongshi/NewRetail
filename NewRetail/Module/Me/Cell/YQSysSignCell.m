//
//  YQSysSignCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQSysSignCell.h"

@interface YQSysSignCell () 

@property (nonatomic, strong) UIView *bottomLineView;
@property (nonatomic, strong) UILabel *lookDetailLab;
@property (nonatomic, strong) UIButton *arrowBtn;

@end

@implementation YQSysSignCell
{
    NSString *_titleStr;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = WhiteColor;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupSysSignView];
    }
    return self;
}
#pragma mark - layout
- (void)setupSysSignView {
    [self addSubview:self.signBtn];
    [self addSubview:self.detailDes];
    [self addSubview:self.timeLab];
    [self addSubview:self.bottomLineView];
    [self addSubview:self.lookDetailLab];
    [self addSubview:self.arrowBtn];
    
    
    [self.detailDes mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(FIT(35));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-4*kMargin_left, FIT(35)));
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.detailDes.mas_bottom).mas_equalTo(FIT(15));
    }];
    
    [self.bottomLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.timeLab.mas_bottom).mas_equalTo(FIT(10));
        make.height.mas_equalTo(1);
    }];
    
    [self.lookDetailLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.bottomLineView.mas_bottom).mas_equalTo(FIT(10));
    }];
    
    [self.arrowBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.bottomLineView.mas_bottom).mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(FIT(9), FIT(15)));
    }];
}

#pragma mark - init

- (UIButton *)signBtn {
    if (!_signBtn) {
        _signBtn = [YQViewFactory buttonWithTitle:@"关于这次干嘛的公告" titleColor:WhiteColor fontSize:FIT(12) userBold:NO target:self sel:@selector(noSelectAction)];
        _signBtn.enabled = NO;
        [_signBtn setBackgroundImage:YQ_IMAGE(@"sys_sign") forState:UIControlStateNormal];
    }
    return _signBtn;
}

- (UILabel *)detailDes {
    if (!_detailDes) {
        _detailDes = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:FIT(14) userBold:NO];
        _detailDes.text = @"尊敬的用户，1.【天赐艾】于【3月5日0:00-6:00】进行系统升级，期间将对如下业成...";
        _detailDes.numberOfLines = 2;
    }
    return _detailDes;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentRight fontSize:FIT(12) userBold:NO];
        _timeLab.text = @"2019/2/6 10:22:33";
    }
    return _timeLab;
}

- (UIView *)bottomLineView {
    if (!_bottomLineView) {
        _bottomLineView = [[UIView alloc] init];
        _bottomLineView.backgroundColor = UIColorFromRGB(0xf1f1f1);
    }
    return _bottomLineView;
}

- (UILabel *)lookDetailLab {
    if (!_lookDetailLab) {
        _lookDetailLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentLeft fontSize:FIT(14) userBold:NO];
        _lookDetailLab.text = @"查看详情";
    }
    return _lookDetailLab;
}

- (UIButton *)arrowBtn {
    if (!_arrowBtn) {
        _arrowBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"push_next") target:self sel:@selector(noSelectAction)];
        _arrowBtn.enabled = NO;
    }
    return _arrowBtn;
}

- (void)noSelectAction {
    //不让点
}

- (void)loadCellWithModel:(YQSysSignModel *)model {
    _titleStr = model.title;
    [self.signBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.mas_offset(0);
        make.size.mas_equalTo(CGSizeMake([UILabel getWidthWithText:model.title height:FIT(20) font:FIT(12)]+FIT(20), FIT(20)));
    }];
    [self.signBtn setTitle:model.title forState:UIControlStateNormal];
    self.detailDes.text = model.content;
    //时间戳转时间
    NSString *timeStr = [NSString getTimeFromTimestampWithTime:model.createTime formatter:@"yyyy-mm-dd HH:mm:ss"];
    self.timeLab.text = timeStr;
}

@end
