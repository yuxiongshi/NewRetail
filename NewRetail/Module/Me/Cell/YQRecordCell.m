//
//  YQRecordCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQRecordCell.h"

@interface YQRecordCell ()

//状态标题
@property (nonatomic, strong) UILabel *theTitleOne;

//金额标题
@property (nonatomic, strong) UILabel *theTitleTwo;

@property (nonatomic, strong) UIView *lineView;

@end

@implementation YQRecordCell

- (void)setupTheView {
    [self addSubview:self.theTitleLab];
    [self addSubview:self.timeLab];
    [self addSubview:self.lineView];
    [self addSubview:self.theTitleOne];
    [self addSubview:self.theTitleTwo];
    [self addSubview:self.commitStatusLab];
    [self addSubview:self.commitMoneyLab];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(44)));
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(44)));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.timeLab.mas_bottom).mas_equalTo(0);
        make.height.mas_equalTo(1);
    }];
    
    [self.theTitleOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(15));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(15)));
    }];
    
    [self.theTitleTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.theTitleLab.mas_bottom).mas_equalTo(FIT(15));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(15)));
    }];
    
    [self.commitStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.theTitleOne.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(44)));
    }];
    
    [self.commitMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.theTitleTwo.mas_bottom).mas_equalTo(FIT(0));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(44)));
    }];
    
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = @"测试测试";
    }
    return _theTitleLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentRight fontSize:FIT(12) userBold:NO];
        _timeLab.text = @"2019-08-05 22:33:44";
    }
    return _timeLab;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = F1LineColor;
    }
    return _lineView;
}

- (UILabel *)theTitleOne {
    if (!_theTitleOne) {
        _theTitleOne = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleOne.text = @"状态";
    }
    return _theTitleOne;
}

- (UILabel *)theTitleTwo {
    if (!_theTitleTwo) {
        _theTitleTwo = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _theTitleTwo.text = @"金额";
    }
    return _theTitleTwo;
}

- (UILabel *)commitStatusLab {
    if (!_commitStatusLab) {
        _commitStatusLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:NO];
        _commitStatusLab.text = @"测试";
    }
    return _commitStatusLab;
}

- (UILabel *)commitMoneyLab {
    if (!_commitMoneyLab) {
        _commitMoneyLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentRight fontSize:FIT(18) userBold:NO];
        _commitMoneyLab.text = @"+1200";
    }
    return _commitMoneyLab;
}

- (void)loadDataWithModel:(YQRecordModel *)model {
    NSDictionary *theTitleDict = @{@"1":@"购买商品支出",
                                    @"2":@"提现支出",
                                    @"3":@"直推奖",
                                    @"4":@"间接奖",
                                    @"5":@"辅导奖",
                                    @"6":@"业绩奖",
                                    @"7":@"管理奖",
                                    @"8":@"商品卖出",
                                    @"9":@"GBK划转",
                                   @"10":@"佣金转出",
                                   @"11":@"GBK转出",
                                   @"12":@"充值",
                                   @"14":@"佣金手续费转入",
                                   @"15":@"奖励余额转入可用余额",
                                   @"16":@"商品卖出",
                                   @"17":@"委托销售支出",
                                   @"18":@"健康保障基金收入"
                                   };
    self.theTitleLab.text = [theTitleDict objectForKey:model.transType];
    self.timeLab.text = model.createTime;
    self.commitStatusLab.text = @"已完成";
    if ([model.amount containsString:@"-"]) {
        self.commitMoneyLab.text = model.amount;
    }else {
        self.commitMoneyLab.text = [NSString stringWithFormat:@"+%@",model.amount];
    }
    
}

- (void)loadPassCardWithModel:(YQRecordModel *)model {
    NSDictionary *theTitleDict = @{@"1":@"购买商品支出",
                                   @"2":@"提现支出",
                                   @"3":@"直推奖",
                                   @"4":@"间接奖",
                                   @"5":@"辅导奖",
                                   @"6":@"业绩奖",
                                   @"7":@"管理奖",
                                   @"8":@"商品卖出",
                                   @"9":@"GBK划转",
                                   @"10":@"佣金转出",
                                   @"11":@"GBK转出",
                                   @"12":@"充值",
                                   @"14":@"佣金手续费转入",
                                   @"15":@"奖励余额转入可用余额",
                                   @"16":@"商品卖出",
                                   @"17":@"委托销售支出",
                                   @"18":@"健康保障基金收入"
                                   };
    self.theTitleLab.text = [theTitleDict objectForKey:model.transType];
    self.timeLab.text = model.createTime;
    self.commitStatusLab.text = @"已完成";
    if ([model.amount containsString:@"-"]) {
        self.commitMoneyLab.text = model.amount;
    }else {
        self.commitMoneyLab.text = [NSString stringWithFormat:@"+%@",model.amount];
    }
    
}


@end
