//
//  SZUserInfoCommonCell.m
//  GBKTrade
//
//  Created by sumrain on 2018/7/27.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "SZUserInfoCommonCell.h"
@interface SZUserInfoCommonCell()

@property (nonatomic, strong) UIView *lineView;

@end;

@implementation SZUserInfoCommonCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
//        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setSubView];
        self.backgroundColor=[UIColor whiteColor];
    }
    return self;
}


-(void)setSubView{
    
    UILabel* titleLabel=[UILabel new];
    titleLabel.text= NSLocalizedString(@"昵称", nil) ;
    titleLabel.font=[UIFont systemFontOfSize:FIT(16)];
    titleLabel.textColor=MainLabelBlackColor;
    [self addSubview:titleLabel];
    self.titleLabel=titleLabel;
    [titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(16));
        make.centerY.equalTo(self.mas_centerY);
        make.width.mas_equalTo(FIT(200));
        make.height.mas_equalTo(titleLabel.font.lineHeight);
        
    }];
    
    
    UIImageView *rightBtn = [[UIImageView alloc] init];
    rightBtn.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:rightBtn];
    self.rightBtn = rightBtn;
    [rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(FIT(-16));
        make.top.mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(FIT(60), FIT(60)));
    }];
    rightBtn.layer.cornerRadius = FIT(60)/2.0;
    rightBtn.layer.masksToBounds = YES;
    [rightBtn setHidden:YES];
    
    UIButton* detailBtn=[UIButton new];
    [detailBtn setTitleColor:MainLabelGrayColor forState:UIControlStateNormal];
    detailBtn.titleLabel.font = [UIFont systemFontOfSize:FIT(14)];
    detailBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    detailBtn.userInteractionEnabled=NO;
    [self addSubview:detailBtn];
    self.detailBtn=detailBtn;
    [detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(FIT(200));
        make.height.mas_equalTo(FIT(25));
        make.right.mas_equalTo(FIT(-16));
        make.centerY.equalTo(self.mas_centerY);
    }];
    
    UIView* lineView=[UIView new];
    [lineView setBackgroundColor:LineColor];
    [self addSubview:lineView];
    self.lineView = lineView;
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);;
        make.left.mas_equalTo(FIT(15));
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(FIT(1));
    }];
    
}

- (void)loadCellValueWithArray:(NSArray *)titleArr withIndexPath:(NSIndexPath *)indexPath {
    self.titleLabel.text = titleArr[indexPath.section][indexPath.row];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //头像
            [self.rightBtn setHidden:NO];
            [self.lineView setHidden:YES];
            [self.rightBtn sd_setImageWithURL:[NSURL URLWithString:[YQPersonInformationModel sharedYQPersonInformationModel].avatar] placeholderImage:YQ_IMAGE(@"header_icon")];
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            [self.detailBtn setTitle:[YQPersonInformationModel sharedYQPersonInformationModel].nickName forState:UIControlStateNormal];
        }else if (indexPath.row == 1) {
            //手机号
            [self.detailBtn setTitle:[YQPersonInformationModel sharedYQPersonInformationModel].phoneNum forState:UIControlStateNormal];
            [self.lineView setHidden:YES];
        }
    }else {
        if (indexPath.row == 0) {
            //是否身份认证
            //实名认证状态（0：未认证，1：认证中，2：认证失败，3，认证通过）
            [self.detailBtn setTitleColor:UIColorFromRGB(0x3d5896) forState:UIControlStateNormal];
            if ([[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] == 0) {
                [self.detailBtn setTitle:@"去认证" forState:UIControlStateNormal];
            }else if ([[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] == 1) {
                [self.detailBtn setTitle:@"认证中" forState:UIControlStateNormal];
            }else if ([[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] == 2) {
                [self.detailBtn setTitle:@"认证失败" forState:UIControlStateNormal];
            }else {
                
                [self.detailBtn setTitle:@"认证通过" forState:UIControlStateNormal];
            }

        }else {
            [self.detailBtn setImage:YQ_IMAGE(@"push_next") forState:UIControlStateNormal];
            if (indexPath.row == 4) {
                [self.lineView setHidden:YES];
            }
        }
    }
}

//时间戳转时间
- (NSString *)timeStampTurnTime:(NSString *)timeStamp {
    NSTimeInterval interval = [timeStamp doubleValue] / 1000.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
