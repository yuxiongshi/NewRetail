//
//  YQMyTeamCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_MyTeamCell @"YQMyTeamCell"
#define teamCell_height FIT(42)

#import "YQCustomCell.h"
#import "YQMyTeamModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTeamCell : YQCustomCell

/**
 描述
 */
@property (nonatomic, strong) UILabel *leftLab;

/**
 金额
 */
@property (nonatomic, strong) UILabel *moneyLab;

- (void)loadCellWithData:(YQMyTeamModel *)model index:(NSIndexPath *)indexPath;


@end

NS_ASSUME_NONNULL_END
