//
//  YQMyTeamCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTeamCell.h"

@implementation YQMyTeamCell

- (void)setupTheView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self addSubview:self.leftLab];
    [self addSubview:self.moneyLab];
    
    [self.leftLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(35));
        make.top.mas_equalTo(FIT(24));
        make.size.mas_equalTo(CGSizeMake((ScreenWidth-FIT(70))/2.0, FIT(17)));
    }];
    
    [self.moneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(35));
        make.top.mas_equalTo(FIT(24));
        make.size.mas_equalTo(CGSizeMake((ScreenWidth-FIT(70))/2.0, FIT(17)));
    }];
    
}

- (UILabel *)leftLab {
    if (!_leftLab) {
        _leftLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:NO];
        _leftLab.text = @"直接奖累计";
    }
    return _leftLab;
}

- (UILabel *)moneyLab {
    if (!_moneyLab) {
        _moneyLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentRight fontSize:FIT(18) userBold:NO];
        _moneyLab.text = @"￥210";
    }
    return _moneyLab;
}

- (void)loadCellWithData:(YQMyTeamModel *)model index:(NSIndexPath *)indexPath {
    
    
}

- (void)leftContentTextWith:(NSString *)text label:(UILabel *)label{
    [label setAttributedTextColorWithBeforeString:text beforeColor:MainRedTextColor afterString:@"累计" afterColor:UIColorFromRGB(0xcccccc)];
}

@end
