//
//  YQBankCardSettingCell.m
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "YQBankCardSettingCell.h"

@interface YQBankCardSettingCell ()

//@property (nonatomic, strong) NSTimer *timers;
@property (nonatomic, assign) NSInteger second;

@end

@implementation YQBankCardSettingCell


- (void)loadCellDataWithDataArr:(NSArray *)dataArr withPlaceHoderArr:(NSArray *)placeArr withShowModel:(nonnull YQAvertiseShowModel *)model IndexPath:(nonnull NSIndexPath *)indexPath {
    
    //model是否为nil
    if (model != nil) {
        
        self.bankLab.text = dataArr[indexPath.row];
        NSDictionary *dict = @{@"1":model.bankUserName,
                               @"2":model.bankName,
                               @"3":model.bankAddress,
                               @"4":model.bankNum,
                               @"5":@""
                               };
        self.describtionLab.text = [dict objectForKey:[NSString stringWithFormat:@"%ld",indexPath.row+1]];
            if (indexPath.row == 3 ) {
                self.describtionLab.keyboardType = UIKeyboardTypeNumberPad;
            }else if (indexPath.row == 4) {
                [self setupSendBtn];
            }
        
    }else {
        
        self.bankLab.text = dataArr[indexPath.row];
        self.describtionLab.placeholder = placeArr[indexPath.row];
        self.describtionLab.tag = indexPath.row+200;
        [self.describtionLab addTarget:self action:@selector(changeTextFieldContext:) forControlEvents:UIControlEventEditingChanged];
        if (indexPath.row == 3) {
            self.describtionLab.keyboardType = UIKeyboardTypeNumberPad;
        }else if (indexPath.row == 4) {
            [self setupSendBtn];
        }
    }
    
    
}

- (void)setupSendBtn {
    [self addSubview:self.sendBtn];
    self.describtionLab.keyboardType = UIKeyboardTypeNumberPad;
    self.describtionLab.placeholder = @"请输入短信验证码";
    [self.sendBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(30)));
    }];
    
    [self.describtionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.sendBtn.mas_left).mas_equalTo(-FIT(10));
        make.left.equalTo(self.bankLab.mas_right).mas_equalTo(0);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.height.mas_equalTo(FIT(20));
    }];
}

- (void)changeTextFieldContext:(UITextField *)textField {
    if (textField.tag == 200) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:6];
    }else if (textField.tag == 201) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:10];
    }else if (textField.tag == 202) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:30];
    }else {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:30];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    
}

- (QCCountdownButton *)sendBtn {
    if (!_sendBtn) {
        _sendBtn = [QCCountdownButton countdownButton];
        _sendBtn.title = @"获取验证码";
        // 普通状态下的背景颜色
        _sendBtn.nomalBackgroundColor = [UIColor redColor];
        // 字体
        _sendBtn.titleLabelFont = [UIFont systemFontOfSize:11];
        _sendBtn.totalSecond = 59;
        _sendBtn.layer.cornerRadius = 3;
        [_sendBtn addTarget:self action:@selector(sendAction) forControlEvents:UIControlEventTouchUpInside];
        //进度b
        WeakSelf(self);
        [_sendBtn processBlock:^(NSUInteger second) {
            weakSelf.sendBtn.title = [NSString stringWithFormat:@"(%lis)后重新获取", second];
            
        } onFinishedBlock:^{  // 倒计时完毕
            weakSelf.sendBtn.title = @"重新获取验证码";
        }];
    }
    return _sendBtn;
}

- (void)sendAction {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"users/tokenMsg") appendParameters:@{@"msgType":@"6"} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            [LCProgressHUD showSuccess:@"发送成功"];
        }else {
            [LCProgressHUD showSuccess:base.msg];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
