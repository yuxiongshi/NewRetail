//
//  GSPersonInfoCell.m
//  GBKTrade
//
//  Created by sumrain on 2019/1/10.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "GSPersonInfoCell.h"
@interface GSPersonInfoCell()



@end

@implementation GSPersonInfoCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setSubView];
    }
    return self;
}


-(void)setSubView{
    
    UIImageView* itemIV=[UIImageView new];
    itemIV.contentMode=UIViewContentModeScaleAspectFit;
    [self addSubview:itemIV];
    self.itemIV=itemIV;
    [itemIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.mas_equalTo(FIT(15));
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(FIT(20));
        make.width.mas_equalTo(FIT(20));
    }];
    
    
    
    UILabel* titleLab=[UILabel new];
    titleLab.textColor=MainBlackColor;
    titleLab.font=[UIFont fontWithName:@"PingFangSC-Regular" size:FIT(15)];
    [self addSubview:titleLab];
    self.titleLab=titleLab;
    [titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.left.equalTo(itemIV.mas_right).offset(FIT(15));
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(FIT(15));
        make.width.mas_equalTo(FIT(100));
    }];
    
    
    
    UIImageView* rightIV=[UIImageView new];
    rightIV.contentMode=UIViewContentModeScaleAspectFit;
    [rightIV setImage:[UIImage imageNamed:@"push_next"]];
    [self addSubview:rightIV];
    self.rightIV=rightIV;
    [rightIV mas_makeConstraints:^(MASConstraintMaker *make) {
        
        make.right.mas_equalTo(FIT(-15));
        make.centerY.equalTo(self.mas_centerY);
        make.height.mas_equalTo(FIT(15));
        make.width.mas_equalTo(FIT(15));
    }];
    
    UIView* lineView=[UIView new];
    [lineView setBackgroundColor:LineColor];
    [self addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);;
        make.left.mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.height.mas_equalTo(FIT(0.5));
    }];

}


- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

@end
