//
//  AboutCell.m
//  GBKTrade
//
//  Created by yuqin on 2019/4/29.
//  Copyright © 2019年 LionIT. All rights reserved.
//

#import "AboutCell.h"

@implementation AboutCell
- (IBAction)copycontactAction:(id)sender {
    
    UIPasteboard *paste = [UIPasteboard generalPasteboard];
    [paste setString:self.contactLab.text];
    if (paste == nil) {
        [LCProgressHUD showFailure:@"复制失败"];
    }else {
        [LCProgressHUD showSuccess:@"已复制到粘贴板"];
    }
    
}

//- (void)cellWithModel:(AdvertisingModel *)model withLeftArr:(NSMutableArray *)leftArr withIndexPath:(NSIndexPath *)index{
//    [self.btn setHidden:YES];
//    self.desLab.text = leftArr[index.row];
//    self.desLab.font = YQ_Font(13);
//    if ([model.orderType isEqualToString:@"1"]) {
//        model.orderType = @"我要在线购买";
//    }else {
//        model.orderType = @"我要在线出售";
//    }
//    if (isEmptyString(model.premiumRate)) {
//        model.premiumRate = @"0%";
//    }
//    NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:
//                          model.advNum,@"0",
//                          model.orderType,@"1",
//                          model.adStatus,@"2",
//                          model.createTime,@"3",
//                          model.dueTime,@"4",
//                          model.tokenName,@"5",
//                          YQ_StringAndInt(@"%@", model.premiumRate),@"6",
//                          model.tokenPrice,@"7",
//                          model.minNum,@"8",
//                          model.maxNum,@"9",
//                          model.tokenNum,@"10",
//                          model.remainedNum,@"11", nil];
//    self.contentLab.text = [dict objectForKey:[NSString stringWithFormat:@"%ld",index.row]];
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
