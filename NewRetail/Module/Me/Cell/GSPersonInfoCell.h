//
//  GSPersonInfoCell.h
//  GBKTrade
//
//  Created by sumrain on 2019/1/10.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
#define GSPersonInfoCellHeight FIT(60)

#define GSPersonInfoCellReuseIdentifier  @"GSPersonInfoCellReuseIdentifier"


@interface GSPersonInfoCell : UITableViewCell


@property (strong, nonatomic)  UIImageView *itemIV;

@property (strong, nonatomic)  UILabel *titleLab;

@property (strong, nonatomic)  UIImageView *rightIV;
@end

NS_ASSUME_NONNULL_END
