//
//  IdentityCertificationCell.m
//  GBKTrade
//
//  Created by admin on 2019/4/23.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "IdentityCertificationCell.h"


@interface IdentityCertificationCell ()

@end

@implementation IdentityCertificationCell

- (void)loadCellWithTitleArr:(NSArray *)titleArr withPlaceArr:(NSArray *)placeArr indexPath:(NSIndexPath *)indexPath {
    self.nameLab.text = [NSString stringWithFormat:@"%@:",titleArr[indexPath.row]];
    self.contextTF.placeholder = placeArr[indexPath.row];
    self.contextTF.tag = 1000+indexPath.row;
    if (indexPath.row == 0) {
        
    }else {
        [self.contextTF addTarget:self action:@selector(changeTextFieldContext:) forControlEvents:UIControlEventEditingChanged];
        self.showBtn.hidden = YES;
        if (indexPath.row == 2) {
            self.contextTF.keyboardType = UIKeyboardTypeNumberPad;
        }
    }
    
}

- (void)changeTextFieldContext:(UITextField *)textField {
    if (textField.tag == 1001) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:6];
    }else if (textField.tag == 1002) {
        [LimitTextFieldNum limitTextFieldContentNumWith:textField withMaxNum:18];
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
