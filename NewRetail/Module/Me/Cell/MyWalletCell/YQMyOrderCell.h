//
//  YQMyOrderCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_MyOrderCell @"YQMyOrderCell"
#define orderCell_height FIT(124)

#import "YQCustomCell.h"
#import "YQMyOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderCell : YQCustomCell

@property (weak, nonatomic) IBOutlet UILabel *czAndTX;

@property (weak, nonatomic) IBOutlet UILabel *createTimeLab;

@property (weak, nonatomic) IBOutlet UILabel *theTitleLabOne;
@property (weak, nonatomic) IBOutlet UILabel *theTitleLabTwo;
@property (weak, nonatomic) IBOutlet UILabel *theTitleLabThree;
@property (weak, nonatomic) IBOutlet UILabel *statusLab;
@property (weak, nonatomic) IBOutlet UILabel *amountOne;
@property (weak, nonatomic) IBOutlet UILabel *amountTwo;

- (void)loadCellDataWithModel:(YQMyOrderModel *)orderModel czOrTX:(NSString *)czOrTX status:(NSString *)status statusColor:(NSString *)color threeLabText:(NSString *)threeLabText ;

@end

NS_ASSUME_NONNULL_END
