//
//  YQMyWholesaleCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//


#define YQ_MyWholesaleCell @"YQMyWholesaleCell"
#define WholesaleCell_Height FIT(140)

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMyWholesaleCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
