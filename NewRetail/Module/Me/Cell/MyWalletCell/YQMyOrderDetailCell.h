//
//  YQMyOrderDetailCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_MyOrderDetailCell @"YQMyOrderDetailCell"
#define orderCell_height FIT(30)

#import "YQCustomCell.h"
#import "YQMyOrderDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderDetailCell : YQCustomCell

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 des
 */
@property (nonatomic, strong) UILabel *desLab;

/**
 copyBtn
 */
@property (nonatomic, strong) UIButton *copyBtn;

@property (nonatomic, copy) void (^CopyBlock) (void);

- (void)loadCellDataWithModel:(YQMyOrderDetailModel *)model indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
