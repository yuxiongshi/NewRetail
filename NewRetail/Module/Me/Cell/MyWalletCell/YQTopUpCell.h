//
//  YQTopUpCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_TopUpCell  @"YQTopUpCell"
#define YQTopUpCell_Height FIT(50)
#import "YQCustomCell.h"

NS_ASSUME_NONNULL_BEGIN


@interface YQTopUpCell : YQCustomCell
@property (weak, nonatomic) IBOutlet UIImageView *paywayImg;
@property (weak, nonatomic) IBOutlet UILabel *paywayLab;
@property (weak, nonatomic) IBOutlet UIButton *isSelectBtn;
@property (weak, nonatomic) IBOutlet UIView *bottomLineView;

@end

NS_ASSUME_NONNULL_END
