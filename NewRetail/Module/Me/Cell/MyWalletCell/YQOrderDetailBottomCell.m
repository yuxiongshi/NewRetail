//
//  YQOrderDetailBottomCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQOrderDetailBottomCell.h"

@implementation YQOrderDetailBottomCell

- (void)setupTheView {
    [self addSubview:self.bottomView];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
}

- (YQTopUpBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[YQTopUpBottomView alloc] init];
        _bottomView.backgroundColor = WhiteColor;
        _bottomView.headView.theTitleLab.text = @"我的付款信息";
    }
    return _bottomView;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
