//
//  YQMyOrderDetailCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyOrderDetailCell.h"

@implementation YQMyOrderDetailCell

- (void)setupTheView {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self addSubview:self.theTitleLab];
    [self addSubview:self.copyBtn];
    [self addSubview:self.desLab];
    
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(10));
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(30)));
    }];
    
    [self.copyBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-FIT(10));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
    [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.copyBtn.mas_left).mas_equalTo(-FIT(10));
        make.centerY.mas_equalTo(self.mas_centerY);
    }];
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = @"订单号";
    }
    return _theTitleLab;
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _desLab.text = @"2112478126412";
    }
    return _desLab;
}

- (UIButton *)copyBtn {
    if (!_copyBtn) {
        _copyBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"me_copy") target:self sel:@selector(copyAction)];
    }
    return _copyBtn;
}

//- (void)loadCellDataWithModel:(YQMyOrderDetailModel *)model indexPath:(nonnull NSIndexPath *)indexPath{
////    1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
//    NSArray *titleArr;
//    if ([model.orderStatus integerValue] == 3) {
//        titleArr = @[@"订单号",@"下单时间",@"完成时间"];
//    }else if ([model.orderStatus integerValue] == 4) {
//        titleArr = @[@"订单号",@"下单时间",@"取消时间"];
//    }
//}

- (void)copyAction {
    //copy
    if (self.CopyBlock) {
        self.CopyBlock();
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
