//
//  YQOrderDetailBottomCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_OrderDetailBottomCell @"YQOrderDetailBottomCell"

#import "YQCustomCell.h"
#import "YQTopUpBottomView.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQOrderDetailBottomCell : YQCustomCell

@property (nonatomic, strong) YQTopUpBottomView *bottomView;

@end

NS_ASSUME_NONNULL_END
