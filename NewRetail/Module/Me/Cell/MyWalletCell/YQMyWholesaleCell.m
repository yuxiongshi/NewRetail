//
//  YQMyWholesaleCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyWholesaleCell.h"

@implementation YQMyWholesaleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
