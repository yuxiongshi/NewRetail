//
//  YQMyOrderCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyOrderCell.h"

@implementation YQMyOrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
//    self.amountOne.hidden = YES;
//    self.theTitleLabTwo.hidden = YES;
    self.selectionStyle = UITableViewCellSeparatorStyleNone;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)loadCellDataWithModel:(YQMyOrderModel *)orderModel czOrTX:(NSString *)czOrTX status:(NSString *)status statusColor:(NSString *)color threeLabText:(NSString *)threeLabText {
    self.czAndTX.text = czOrTX;
    self.createTimeLab.text = orderModel.createTime;
    self.theTitleLabTwo.text = @"状态";
    self.theTitleLabThree.text = threeLabText;
    self.statusLab.text = status;
    self.statusLab.textColor = [UIColor colorWithHexString:color];
    if ([orderModel.orderStatus integerValue] == 3) {
        
        self.theTitleLabTwo.text = @"预充值金额";
        self.amountOne.text = orderModel.cashNum;
        self.amountTwo.text = orderModel.realCnyNum;
    }else {
        self.amountTwo.text = orderModel.cashNum;
    }
}

@end
