//
//  AboutCell.h
//  GBKTrade
//
//  Created by yuqin on 2019/4/29.
//  Copyright © 2019年 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "AdvertisingModel.h"

@interface AboutCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (weak, nonatomic) IBOutlet UIButton *theTitleBtn;
@property (weak, nonatomic) IBOutlet UILabel *contactLab;

//- (void)cellWithModel:(AdvertisingModel *)model withLeftArr:(NSMutableArray *)leftArr withIndexPath:(NSIndexPath *)index;



@end
