//
//  ResetPWDCell.h
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CountDownBlock) (void);
typedef void (^ObserveTextFieldBlock) (NSInteger tag,NSString * _Nullable textValue);

@interface ResetPWDCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *describeLab;

@property (weak, nonatomic) IBOutlet UITextField *inputTF;
@property (weak, nonatomic) IBOutlet UIButton *SMSCode;

@property (nonatomic, copy) CountDownBlock block;
@property (nonatomic, copy) ObserveTextFieldBlock valueBlock;


- (void)loadCellWithDesLab:(NSArray *)titleArr placeArr:(NSArray *)placeArr withIndex:(NSIndexPath *)indexPath codeType:(NSString *)titleStr;

@end

NS_ASSUME_NONNULL_END
