//
//  IdentityCertificationCell.h
//  GBKTrade
//
//  Created by admin on 2019/4/23.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>



NS_ASSUME_NONNULL_BEGIN

@interface IdentityCertificationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLab;
@property (weak, nonatomic) IBOutlet UITextField *contextTF;
@property (weak, nonatomic) IBOutlet UIButton *showBtn;


- (void)loadCellWithTitleArr:(NSArray *)titleArr withPlaceArr:(NSArray *)placeArr indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
