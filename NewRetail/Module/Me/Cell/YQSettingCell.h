//
//  YQSettingCell.h
//  GBKTrade
//
//  Created by admin on 2019/4/18.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YQAvertiseShowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQSettingCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *desLab;
@property (weak, nonatomic) IBOutlet UILabel *chooseLanguageLab;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UIImageView *leftIcon;

- (void)cellWithPaymentArr:(NSMutableArray *)paymentArr
              withTotalArr:(NSMutableArray *)totalArr
            withIndex:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
