//
//  YQSysSignCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_SysSignCell @"YQSysSignCell"

#import <UIKit/UIKit.h>
#import "YQSysSignModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQSysSignCell : UITableViewCell

/**
 公告说明
 */
@property (nonatomic, strong) UIButton *signBtn;

/**
 详情说明
 */
@property (nonatomic, strong) UILabel *detailDes;

/**
 发布时间
 */
@property (nonatomic, strong) UILabel *timeLab;

- (void)loadCellWithModel:(YQSysSignModel *)model;

@end

NS_ASSUME_NONNULL_END
