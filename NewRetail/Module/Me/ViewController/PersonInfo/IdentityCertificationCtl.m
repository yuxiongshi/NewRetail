//
//  IdentityCertificationCtl.m
//  GBKTrade
//
//  Created by admin on 2019/4/23.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "IdentityCertificationCtl.h"
#import "IdentityCertificationCell.h"
#import "WMZAlert.h"
#import "PhotoChooseView.h"


@interface IdentityCertificationCtl ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *placeArr;
@property (nonatomic, strong) UITableView *identityTB;
@property (nonatomic, copy) NSString *identityStr;//第一个cell
@property (nonatomic, copy) NSString *realNameStr;//第二个cell
@property (nonatomic, copy) NSString *certificateCarStr;//第三个cell

@property (nonatomic, strong) PhotoChooseView *photoView;
@end

@implementation IdentityCertificationCtl
{
    NSString *iconUrl1;
    NSString *iconUrl2;
    NSString *iconUrl3;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.iconAndTitleLeftBtn setTitle:@"身份认证" forState:UIControlStateNormal];
    //创建视图
    [self createUI];
}


- (void)createUI {
    
    //初始化空值
    self.identityStr = @"";
    self.realNameStr = @"";
    self.certificateCarStr = @"";
    iconUrl1 = @"";
    iconUrl2 = @"";
    iconUrl3 = @"";
    
    self.titleArr=@[NSLocalizedString(@"证件类型", nil),NSLocalizedString(@"真实姓名", nil),
                    NSLocalizedString(@"证件号码", nil)];
    self.placeArr=@[NSLocalizedString(@"请选择", nil),NSLocalizedString(@"请输入", nil),
                    NSLocalizedString(@"请输入", nil)];
    _identityTB = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, FIT(195)) style:UITableViewStylePlain];
    _identityTB.scrollEnabled = NO;
    _identityTB.delegate = self;
    _identityTB.dataSource = self;
    _identityTB.sectionHeaderHeight = FIT(15);
    _identityTB.sectionFooterHeight = FIT(0);
    [_identityTB registerNib:[UINib nibWithNibName:@"IdentityCertificationCell" bundle:nil] forCellReuseIdentifier:@"IdentityCertificationCell"];
    [self.view addSubview:_identityTB];
    
    self.photoView = [[PhotoChooseView alloc] init];
    [self.view addSubview:self.photoView];
    [self.photoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(self->_identityTB.mas_bottom).mas_offset(@0);
        make.left.right.bottom.mas_offset(@0);
    }];
    [self.photoView.commitBtn addTarget:self action:@selector(commitAction) forControlEvents:UIControlEventTouchUpInside];
    
    self.photoView.urlBlock = ^(NSString * _Nonnull urlStr, NSInteger tag) {
        if (tag == 200) {
            self->iconUrl1 = urlStr;
        }else if (tag == 201) {
            self->iconUrl2 = urlStr;
        }else if (tag == 202) {
            self->iconUrl3 = urlStr;
        }
    };
}

- (void)commitAction {
    DLog(@"iconUrl1 = %@ iconUrl2 = %@ iconUrl3 = %@",iconUrl1,iconUrl2,iconUrl3);
    //判断空
    NSDictionary *identifyDic = @{@"身份证":@"1",
                                  @"军官证":@"2",
                                  @"护照":@"3",
                                  @"台湾居民通行证":@"4",
                                  @"港澳居民通行证":@"5",
                                  };
    NSString *identifyKey = [identifyDic objectForKey:self.identityStr];
    if (isEmptyString(self.identityStr)) {
        [LCProgressHUD showMessage:@"请选择证件类型"];
        return;
    }
    if (isEmptyString(self.realNameStr)) {
        [LCProgressHUD showMessage:@"请输入真实姓名"];
        return;
    }
    if (isEmptyString(self.certificateCarStr)) {
        [LCProgressHUD showMessage:@"请输入证件号码"];
        return;
    }

    if (isEmptyString(iconUrl1)) {
        [LCProgressHUD showMessage:@"请上传证件正面照"];
        return;
    }
    if (isEmptyString(iconUrl2)) {
        [LCProgressHUD showMessage:@"请上传证件反面照"];
        return;
    }
    if (isEmptyString(iconUrl3)) {
        [LCProgressHUD showMessage:@"请上传手持证件正面照"];
        return;
    }

    NSDictionary *parameters = @{@"pictureOne":iconUrl1,
                                @"pictureTwo":iconUrl2,
                                @"pictureThree":iconUrl3,
                                @"authType":identifyKey,
                                @"authNum":self.certificateCarStr,
                                @"authName":self.realNameStr
                                };
    NSString* urlString=[NSString stringWithFormat:@"%@sys/authentication",BaseHttpUrl];
    [[SZHTTPSRsqManager sharedSZHTTPSRsqManager] postRequestWithUrlString:urlString appendParameters:nil bodyParameters:parameters successBlock:^(id responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            [LCProgressHUD showMessage:@"上传成功"];
            
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    } failureBlock:^(NSError *error) {
        
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.titleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(60);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return FIT(15);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IdentityCertificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"IdentityCertificationCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.contextTF.delegate = self;
    [cell loadCellWithTitleArr:self.titleArr withPlaceArr:self.placeArr indexPath:indexPath];
    if (indexPath.row == 0) {
        cell.contextTF.text = self.identityStr;
    }else if (indexPath.row == 1) {
        cell.contextTF.text = self.realNameStr;
    }else {
        cell.contextTF.text = self.certificateCarStr;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark - 第一个cell点击不弹出键盘
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 1000) {
        [textField resignFirstResponder];
        //显示弹框 并且showBtn的箭头向上
        NSArray *titleArr = @[@"身份证",@"军官证",@"护照",@"台湾居民通行证",@"港澳居民通行证"];
        WeakSelf(self);
        [[WMZAlert shareInstance] showAlertWithType:AlertTypeSelect headTitle:@"请选择类型" textTitle:titleArr leftHandle:^(id anyID) {
            
        } rightHandle:^(id anyID) {
            weakSelf.identityStr = anyID;
            
            [weakSelf.identityTB reloadData];
            NSLog(@"self.identityStr = %@",self.identityStr);
        }];
    }
    
    NSLog(@"++++++self.identityStr = %@",self.identityStr);
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 1001) {
        //真实姓名
        self.realNameStr = textField.text;
    }else if (textField.tag == 1002) {
        //证件号码
        self.certificateCarStr = textField.text;
    }
}

@end
