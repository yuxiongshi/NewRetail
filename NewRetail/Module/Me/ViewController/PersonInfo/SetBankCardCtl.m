//
//  SetBankCardCtl.m
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "SetBankCardCtl.h"
#import "YQBankCardSettingCell.h"

@interface SetBankCardCtl ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic, strong) UITableView *settingTB;
@property (nonatomic, copy) NSArray *dataArr;
@property (nonatomic, copy) NSArray *placeHolderArr;
@property (nonatomic, strong) UITextField *nameTF;
@property (nonatomic, strong) UITextField *bankNameTF;
@property (nonatomic, strong) UITextField *branchTF;
@property (nonatomic, strong) UITextField *bankCardTF;
@property (nonatomic, strong) UITextField *codeTF;

@end

@implementation SetBankCardCtl
{
    NSString *nameText;//姓名
    NSString *bankName;//银行名称
    NSString *branch;//分行
    NSString *bankCard;//银行卡号
    NSString *codeStr;//验证码
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.iconAndTitleLeftBtn setTitle:@"设置银行卡" forState:UIControlStateNormal];
    self.dataArr = @[NSLocalizedString(@"持卡人：", nil),NSLocalizedString(@"银行名称：", nil),NSLocalizedString(@"所属分行:", nil),NSLocalizedString(@"银行卡号:", nil),NSLocalizedString(@"短信验证码:", nil)];
    self.placeHolderArr = @[NSLocalizedString(@"请输入持卡人姓名", nil),NSLocalizedString(@"请输入银行名称", nil),NSLocalizedString(@"请输入所属分行", nil),NSLocalizedString(@"请输入银行卡号", nil),NSLocalizedString(@"请输入短信验证码", nil)];
    [self tableView];
    [self createLoginOutView];
}

- (void)tableView {
    _settingTB = [[UITableView alloc] initWithFrame:CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, ScreenHeight-(FIT(85))) style:UITableViewStyleGrouped];
    _settingTB.scrollEnabled = NO;
    _settingTB.delegate = self;
    _settingTB.dataSource = self;
    _settingTB.separatorStyle = UITableViewCellEditingStyleNone;
    _settingTB.sectionHeaderHeight = FIT(15);
    _settingTB.sectionFooterHeight = FIT(0);
    [_settingTB registerNib:[UINib nibWithNibName:@"YQBankCardSettingCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_settingTB];
//    [_settingTB mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(@(FIT(NavigationStatusBarHeight)));
//        make.leading.trailing.bottom.equalTo(self.view);
//    }];
}

- (void)createLoginOutView {
    UIButton *loginOutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    loginOutBtn.frame = CGRectMake(15, ScreenHeight-FIT(150), ScreenWidth-30, 60);
    [loginOutBtn setTitle:@"提交" forState:UIControlStateNormal];
    [loginOutBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginOutBtn setBackgroundColor:MainRedTextColor];
    loginOutBtn.layer.cornerRadius = 5.f;
    loginOutBtn.layer.masksToBounds = YES;
    [self.view addSubview:loginOutBtn];
    [loginOutBtn addTarget:self action:@selector(loginOutAction) forControlEvents:UIControlEventTouchUpInside];
    [loginOutBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(16));
        make.bottom.mas_equalTo(-(Botoom_IPhoneX + FIT(20)));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-FIT(32), FIT(45)));
    }];
}

#pragma mark - 提交
- (void)loginOutAction {
    //姓名
    if (isEmptyString(nameText)) {
        //如果姓名为空 弹出提示框
        [LCProgressHUD showMessage:@"请输入姓名!"];
        return;
    }
    else {
        if ([LimitTextFieldNum validateUserName:nameText] == YES) {
            [LCProgressHUD showMessage:@"请输入正确姓名!"];
            return;
        }
    }
    
    //银行名称
    if (isEmptyString(bankName)) {
        [LCProgressHUD showMessage:@"请输入银行名称!"];
        return;
    }
    //所属分行
    if (isEmptyString(branch)) {
        [LCProgressHUD showMessage:@"请输入分行地址!"];
        return;
    }
    //银行卡号
    if (isEmptyString(bankCard)) {
        [LCProgressHUD showMessage:@"请输入银行卡号!"];
        return;
    }
    else if ([LimitTextFieldNum isBankCard:bankCard] == NO) {
        [LCProgressHUD showMessage:@"请输入正确的银行卡号!"];
        return;
    }
    
    if (isEmptyString(self.codeTF.text)) {
        [LCProgressHUD showMessage:@"请输入验证码!"];
        return;
    }
    
    //发送请求
    NSString *urlStr = [NSString stringWithFormat:@"%@sys/payment-way",BaseHttpUrl];
//    NSDictionary *parameters;
//    if (self.model != nil) {
//        parameters = @{@"paymentWay":@"3",
//                       @"bankUserName":self.model.bankUserName,
//                       @"bankName":self.model.bankName,
//                       @"bankAddress":self.model.bankAddress,
//                       @"bankNum":self.model.bankNum,
//                       @"messageCode":codeStr
//                       };
//    }else {
//        parameters = @{@"paymentWay":@"3",
//                       @"bankUserName":nameText,
//                       @"bankName":bankName,
//                       @"bankAddress":branch,
//                       @"bankNum":bankCard,
//                       @"messageCode":codeStr
//                       };
//    }
     NSDictionary *parameters = @{@"paymentWay":@"3",
                   @"bankUserName":self.nameTF.text,
                   @"bankName":self.bankNameTF.text,
                   @"bankAddress":self.branchTF.text,
                   @"bankNum":self.bankCardTF.text,
                   @"messageCode":self.codeTF.text
                   };
    [LCProgressHUD showLoading:@"加载中..."];
    [SZHTTPSReqManager postRequestWithUrlString:urlStr appendParameters:nil bodyParameters:parameters successBlock:^(id responseObject) {
        
        if ([responseObject[@"code"] integerValue] == 0) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [LCProgressHUD hide];
                [LCProgressHUD showSuccess:@"保存成功"];
                [self.navigationController popViewControllerAnimated:YES];
            });
        }else {
            [LCProgressHUD showFailure:responseObject[@"msg"]];
        }
        
    } failureBlock:^(NSError *error) {
        
    }];
}

#pragma mark - table view delegate/dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  FIT(60);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return FIT(15);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQBankCardSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    cell.describtionLab.text = self.model[indexPath.row];
    
    if (self.model != nil) {
        NSDictionary *dict = @{@"1":self.model.bankUserName,
                               @"2":self.model.bankName,
                               @"3":self.model.bankAddress,
                               @"4":self.model.bankNum,
                               @"5":@""
                               };
        cell.describtionLab.text = [dict objectForKey:StringFromLongInt(indexPath.row+1)];
        if (indexPath.row == 0) {
            nameText = [dict objectForKey:@"1"];
            self.nameTF = cell.describtionLab;
        }else if (indexPath.row == 1) {
            bankName = [dict objectForKey:@"2"];
            self.bankNameTF = cell.describtionLab;
        }else if (indexPath.row == 2) {
            branch = [dict objectForKey:@"3"];
            self.branchTF = cell.describtionLab;
        }else if (indexPath.row == 3) {
            bankCard = [dict objectForKey:@"4"];
            self.bankCardTF = cell.describtionLab;
        }else {
            self.codeTF = cell.describtionLab;
        }
    }
    [cell loadCellDataWithDataArr:self.dataArr withPlaceHoderArr:self.placeHolderArr withShowModel:self.model IndexPath:indexPath];
//    cell.describtionLab.delegate = self;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
}

#pragma mark - textfield delegate
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 200) {
        nameText = textField.text;
    }else if (textField.tag == 201) {
        bankName = textField.text;
    }else if (textField.tag == 202) {
        branch = textField.text;
    }else if (textField.tag == 203){
        bankCard = textField.text;
    }else {
        codeStr = textField.text;
    }
    
}

- (void)dealloc {
//    NSLog(@"溜了");
    DLog(@"溜了");
}

@end
