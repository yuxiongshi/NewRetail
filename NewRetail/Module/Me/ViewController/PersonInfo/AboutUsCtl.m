//
//  AboutUsCtl.m
//  GBKTrade
//
//  Created by yuqin on 2019/4/29.
//  Copyright © 2019年 LionIT. All rights reserved.
//

#import "AboutUsCtl.h"
#import "AboutCell.h"
#import "YQAboutUsHeadView.h"

@interface AboutUsCtl ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *aboutTB;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UILabel *appVersionLab;//描述文字
@property (nonatomic, strong) YQAboutUsHeadView *aboutHeadView;
@property (nonatomic, strong) NSArray *contactArr;

@end

@implementation AboutUsCtl
{
    CGFloat _label_H;
    
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray arrayWithObjects:@"客服微信",@"客服QQ", nil];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.txtTitle.text = @"关于我们";
    [self.iconAndTitleLeftBtn setTitle:@"关于我们" forState:UIControlStateNormal];
    self.view.backgroundColor = TableViewBackGroundColor;
    [self loadAboutUsData];
    [self setupUI];
}
//数据请求
- (void)loadAboutUsData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/customService") appendParameters:nil successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            self.contactArr = @[base.data[@"kfWX"],base.data[@"kfQQ"]];
            [self.aboutTB reloadData];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupUI {

    [self.view addSubview:self.aboutHeadView];
    [self.aboutHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationStatusBarHeight);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(235)));
    }];
    
    self.aboutTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.aboutTB.backgroundColor = TableViewBackGroundColor;
    self.aboutTB.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.aboutTB.scrollEnabled = NO;
    self.aboutTB.bounces = NO;
    self.aboutTB.delegate = self;
    self.aboutTB.dataSource = self;
//    self.aboutTB.tableFooterView = [[UIView alloc] init];
    [self.aboutTB registerNib:[UINib nibWithNibName:@"AboutCell" bundle:nil] forCellReuseIdentifier:@"AboutCell"];
    [self.view addSubview:self.aboutTB];
    [self.aboutTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.aboutHeadView.mas_bottom).mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(200)));
    }];
    
    [self.view addSubview:self.appVersionLab];
    [self.appVersionLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-Botoom_IPhoneX-20);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, 15));
    }];
    
}

- (YQAboutUsHeadView *)aboutHeadView {
    if (!_aboutHeadView) {
        _aboutHeadView = [[YQAboutUsHeadView alloc] init];
    }
    return _aboutHeadView;
}

- (UILabel *)appVersionLab {
    if (!_appVersionLab) {
        _appVersionLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0xcccccc) textAlignment:NSTextAlignmentCenter fontSize:FIT(14) userBold:NO];
        _appVersionLab.text = [NSString stringWithFormat:@"版本号 %@",app_Version];
    }
    return _appVersionLab;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(50);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AboutCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AboutCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *imgArr = @[@"wechat_about",@"QQ"];
    [cell.theTitleBtn setTitle:self.dataArr[indexPath.row] forState:UIControlStateNormal];
    [cell.theTitleBtn setImage:YQ_IMAGE(imgArr[indexPath.row]) forState:UIControlStateNormal];
    cell.contactLab.text = self.contactArr[indexPath.row];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
