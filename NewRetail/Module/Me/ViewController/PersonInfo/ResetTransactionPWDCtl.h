//
//  ResetTransactionPWDCtl.h
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "CustomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResetTransactionPWDCtl : CustomViewController

@property (nonatomic, copy) NSString *titleString;

@end

NS_ASSUME_NONNULL_END
