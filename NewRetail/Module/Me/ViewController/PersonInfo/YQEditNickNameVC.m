//
//  YQEditNickNameVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQEditNickNameVC.h"

#import "SZUserInfoViewModel.h"
#import "SZHttpsService.h"
@interface YQEditNickNameVC ()
@property (nonatomic,strong) SZUserInfoViewModel* viewModel;
@property (nonatomic,strong) UITextField* nickNameTextField;
@end

@implementation YQEditNickNameVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor=UIColorFromRGB(0xF0F2F5);
    
    
    [self.btnLeft setTitle:@"昵称修改" forState:UIControlStateNormal];
    UITextField* nickNameTextField=[UITextField new];
    nickNameTextField.textColor=MainLabelBlackColor;
    nickNameTextField.font=[UIFont systemFontOfSize:14.0f];
    nickNameTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, FIT(16), 0)];
    nickNameTextField.leftViewMode=UITextFieldViewModeAlways;
    nickNameTextField.placeholder=@"请输入您的昵称";
    nickNameTextField.clearButtonMode = UITextFieldViewModeAlways;
    nickNameTextField.clearsOnBeginEditing = NO;
    nickNameTextField.text = [YQPersonInformationModel sharedYQPersonInformationModel].nickName;
    nickNameTextField.backgroundColor=[UIColor whiteColor];
    self.nickNameTextField=nickNameTextField;
    [self.view addSubview:nickNameTextField];
    [nickNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationStatusBarHeight+FIT(15));
        make.left.mas_equalTo(FIT(0));
        make.width.mas_equalTo(ScreenWidth);
        make.height.mas_equalTo(FIT(50));
    }];
    RACChannelTo(self,nickNameTextField.text) = RACChannelTo(self.viewModel,nickName);
    @weakify(self);
    [self.nickNameTextField.rac_textSignal subscribeNext:^(id x) {
        @strongify(self);
        self.viewModel.nickName = x;
    }];
    
    UILabel* noticeLab=[UILabel new];
    noticeLab.text=@"好名字可以让你的朋友更容易记住你";
    noticeLab.textColor=MainBlackColor;
    noticeLab.font=[UIFont systemFontOfSize:FIT(13)];
    [self.view addSubview:noticeLab];
    [noticeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nickNameTextField.mas_bottom).offset(FIT(11));
        make.left.mas_equalTo(FIT(16));
        make.right.mas_equalTo(FIT(-16));
        make.height.mas_equalTo(FIT(13));
    }];
    
    
    [self.btnRight setHidden:NO];
    [self.btnRight setTitle:@"保存" forState:UIControlStateNormal];
    [[self.btnRight rac_signalForControlEvents:UIControlEventTouchUpInside]subscribeNext:^(id x) {
        
        @strongify(self);
        [LCProgressHUD showLoading:@"正在加载..."];
        //        [self showLoadingMBProgressHUD];
        if (isEmptyObject(nickNameTextField.text)) {
            [LCProgressHUD showMessage:@"请输入昵称"];
            
        }else if ([nickNameTextField.text isEqualToString:[YQPersonInformationModel sharedYQPersonInformationModel].nickName]){
            [LCProgressHUD showMessage:@"您的昵称并没有改变"];
        }else{
            
            [[[SZHttpsService sharedSZHttpsService] signalRequestModifyNickNameWithParameters:@{@"nickName":nickNameTextField.text}]subscribeNext:^(id x) {
                BaseModel* model=[BaseModel modelWithJson:x];
                if (model.code == 0) {
                    [LCProgressHUD showSuccess:@"修改成功"];
                    [[NSNotificationCenter defaultCenter] postNotificationName:ModifyName object:nil userInfo:@{@"name":self.nickNameTextField.text}];
                    [self.navigationController popViewControllerAnimated:YES];
                }else{
                    [LCProgressHUD showFailure:model.msg];
                }
                
            }error:^(NSError *error) {
                [LCProgressHUD showFailure: error.localizedDescription];
            }];
        }
        
        
        
        
    }];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
