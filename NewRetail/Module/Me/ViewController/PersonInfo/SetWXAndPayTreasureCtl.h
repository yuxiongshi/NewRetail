//
//  SetWXAndPayTreasureCtl.h
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "CustomViewController.h"
#import "YQAvertiseShowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetWXAndPayTreasureCtl : CustomViewController

@property (nonatomic, copy) NSString *titleName;//标题
@property (nonatomic, copy) NSString *isCommitSuccess;//是否已经绑定好
@property (nonatomic, strong) YQAvertiseShowModel *model;

@end

NS_ASSUME_NONNULL_END
