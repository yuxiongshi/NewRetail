//
//  ResetTransactionPWDCtl.m
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "ResetTransactionPWDCtl.h"
#import "BaseTableView.h"

@interface ResetTransactionPWDCtl ()

@property (nonatomic, strong) BaseTableView *UITB;

@end

@implementation ResetTransactionPWDCtl

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([self.titleString integerValue] == 1) {
        
        [self.iconAndTitleLeftBtn setTitle:@"登录密码" forState:UIControlStateNormal];
    }else {
        [self.iconAndTitleLeftBtn setTitle:@"修改密码" forState:UIControlStateNormal];
    }
    
    
    [self setupUI];
}

- (void)setupUI {
    self.UITB = [[BaseTableView alloc] initWithFrame:CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, ScreenHeight)];
    [self.UITB distinguishWithString:self.titleString];
    WeakSelf(self);
    self.UITB.pushBlock = ^{
        [LCProgressHUD showMessage:@"修改成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    };
    [self.view addSubview:self.UITB];
}

@end
