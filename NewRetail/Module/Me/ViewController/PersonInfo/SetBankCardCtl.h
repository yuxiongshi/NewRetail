//
//  SetBankCardCtl.h
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "CustomViewController.h"
#import "YQAvertiseShowModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface SetBankCardCtl : CustomViewController

@property (nonatomic, strong) YQAvertiseShowModel *model;

@end

NS_ASSUME_NONNULL_END
