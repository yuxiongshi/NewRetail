//
//  MePaySettingCtl.m
//  GBKTrade
//
//  Created by admin on 2019/4/19.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "MePaySettingCtl.h"
#import "YQSettingCell.h"
#import "SetWXAndPayTreasureCtl.h"
#import "SetBankCardCtl.h"
#import "YQAvertiseShowModel.h"

@interface MePaySettingCtl ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *settingTB;
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, strong) NSMutableArray *paymentArr;//付款方式的array
@property (nonatomic, strong) NSMutableArray *totalArr;//存放model的array
@property (nonatomic, strong) NSArray *imgArr;//左边的icon

@end

@implementation MePaySettingCtl

- (NSMutableArray *)totalArr {
    if (!_totalArr) {
        _totalArr = [NSMutableArray array];
    }
    return _totalArr;
}

- (NSMutableArray *)paymentArr {
    if (!_paymentArr) {
        _paymentArr = [NSMutableArray arrayWithObjects:@"去绑定",@"去绑定",@"去绑定", nil];
    }
    return _paymentArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self loadPaymentWayData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    self.txtTitle.text = @"支付设置";
    [self.iconAndTitleLeftBtn setTitle:@"支付设置" forState:UIControlStateNormal];
    self.dataArr = @[NSLocalizedString(@"银行卡", nil),NSLocalizedString(@"支付宝", nil),NSLocalizedString(@"微信账号", nil)];
    self.imgArr = @[@"bankCard",@"alipay",@"wechat"];
    
    [self tableView];
}

- (void)loadPaymentWayData {
    NSString * urlStr = [NSString stringWithFormat:@"%@sys/payment-way",BaseHttpUrl];
    [SZHTTPSReqManager get:urlStr appendParameters:nil successBlock:^(id responseObject) {
        
        if (isEmptyArray(responseObject[@"data"])) {
            //未绑定任何支付方式
            self.paymentArr = [NSMutableArray arrayWithObjects:@"去绑定",@"去绑定",@"去绑定", nil];
        }else {
            for (NSDictionary *dict in responseObject[@"data"]) {
                YQAvertiseShowModel *model = [YQAvertiseShowModel mj_objectWithKeyValues:dict];
                [self.totalArr addObject:model];
                if ([model.paymentWay integerValue] == 1) {
                    //微信
                    [self.paymentArr replaceObjectAtIndex:2 withObject:@"已绑定"];
                }else if ([model.paymentWay integerValue] == 2) {
                    //支付宝
                    [self.paymentArr replaceObjectAtIndex:1 withObject:@"已绑定"];
                }else {
                    //银行卡
                    [self.paymentArr replaceObjectAtIndex:0 withObject:@"已绑定"];
                }
            }
            [self.settingTB reloadData];
        }
        
    } failureBlock:^(NSError *error) {
        
    }];
    
}

- (void)tableView {
    _settingTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    _settingTB.scrollEnabled = NO;
    _settingTB.delegate = self;
    _settingTB.dataSource = self;
//    _settingTB.separatorStyle = UITableViewCellSeparatorStyleNone;
    _settingTB.separatorStyle = UITableViewCellEditingStyleNone;
    _settingTB.sectionHeaderHeight = FIT(15);
    _settingTB.sectionFooterHeight = FIT(0);
    [_settingTB registerNib:[UINib nibWithNibName:@"YQSettingCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [self.view addSubview:_settingTB];
    [_settingTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(NavigationStatusBarHeight);
        make.leading.trailing.bottom.equalTo(self.view);
    }];
}

#pragma mark - table view delegate/dataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.dataArr count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  FIT(60);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return FIT(15);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.desLab.text = self.dataArr[indexPath.row];
    cell.leftIcon.image = YQ_IMAGE(self.imgArr[indexPath.row]);
    cell.chooseLanguageLab.hidden = NO;
    cell.chooseLanguageLab.text = self.paymentArr[indexPath.row];
    if ([cell.chooseLanguageLab.text isEqualToString:@"已绑定"]) {
        cell.chooseLanguageLab.textColor = MainRedTextColor;
    }else {
        cell.chooseLanguageLab.textColor = MainLabelGrayColor;
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0) {
        //银行卡
        SetBankCardCtl *ctl = [[SetBankCardCtl alloc] init];
        //传值
        if (!isEmptyArray(self.totalArr)) {
            //传Array
            for (YQAvertiseShowModel *model in self.totalArr) {
                if ([model.paymentWay integerValue] == 3) {
                    ctl.model = model;
                }
            }
        }
        YQ_PUSH(ctl);
    }else if (indexPath.row == 1) {
        //支付宝
        SetWXAndPayTreasureCtl *ctl = [[SetWXAndPayTreasureCtl alloc] init];
        for (YQAvertiseShowModel *model in self.totalArr) {
            if ([model.paymentWay integerValue] == 2) {
                ctl.model = model;
            }
        }
        ctl.titleName = self.dataArr[indexPath.row];
        YQ_PUSH(ctl);
    }else {
        SetWXAndPayTreasureCtl *ctl = [[SetWXAndPayTreasureCtl alloc] init];
        for (YQAvertiseShowModel *model in self.totalArr) {
            if ([model.paymentWay integerValue] == 1) {
                ctl.model = model;
            }
        }
        ctl.titleName = self.dataArr[indexPath.row];
        YQ_PUSH(ctl);
    }
    
}


@end
