//
//  YQEditNickNameVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQEditNickNameVC : YQBaseVC

@property (nonatomic, copy) NSString *nameStr;

@end

NS_ASSUME_NONNULL_END
