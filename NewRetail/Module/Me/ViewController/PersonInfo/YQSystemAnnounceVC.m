//
//  YQSystemAnnounceVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQSystemAnnounceVC.h"
#import "YQSysSignVC.h"

@interface YQSystemAnnounceVC ()

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) MLMSegmentHead *segHead;
@property (nonatomic, strong) MLMSegmentScroll *segScroll;

@end

@implementation YQSystemAnnounceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.iconAndTitleLeftBtn setTitle:@"系统公告" forState:UIControlStateNormal];
    //创建seg
    self.titleArr = @[@"系统通知",@"系统公告"];
    [self setupSegView];
}

- (void)setupSegView {
    _segHead = [[MLMSegmentHead alloc] initWithFrame:CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, 44) titles:self.titleArr headStyle:SegmentHeadStyleLine layoutStyle:MLMSegmentLayoutDefault];
    _segHead.lineColor = kTheTitleBackgroundColor;
    _segHead.lineColor = MainRedTextColor;
    _segHead.lineHeight = 1.0;
    _segHead.selectColor = MainRedTextColor;
    _segHead.fontScale = 1.1;
    _segHead.showIndex = 1;
    
    _segScroll = [[MLMSegmentScroll alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_segHead.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(_segHead.frame)) vcOrViews:[self vcArr:self.titleArr.count]];
    _segScroll.loadAll = NO;
    WeakSelf(self);
    [MLMSegmentManager associateHead:_segHead withScroll:_segScroll completion:^{
        [self.view addSubview:weakSelf.segHead];
        [self.view addSubview:weakSelf.segScroll];
    }];
}

#pragma mark - 数据源
- (NSArray *)vcArr:(NSInteger)count {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSInteger i = 0; i < count; i ++) {
        YQSysSignVC *vc = [YQSysSignVC new];
        vc.index = StringFromLongInt(i);
        [arr addObject:vc];
    }
    return arr;
}

@end
