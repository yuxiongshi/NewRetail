//
//  SZUserInfoViewController.m
//  BTCoin
//
//  Created by sumrain on 2018/7/26.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "GSUserInfoViewController.h"
#import "SZUserInfoViewModel.h"
#import "SZUserInfoCommonCell.h"
//#import "HBEditNickNameVC.h"
#import "YQEditNickNameVC.h"
//#import "GSModifyLoginPsdVC.h"
#import "IdentityCertificationCtl.h"
#import "ResetTransactionPWDCtl.h"
#import "YQAddressListVC.h"
#import "MePaySettingCtl.h"

@interface GSUserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property(nonatomic, strong) UITableView *tableView;

@property(nonatomic,strong) SZUserInfoViewModel* viewModel;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) UIImageView *imgBtn;
@property (nonatomic, strong) UIImage *icon;
@property (nonatomic, strong) UIButton *nameBtn;


@end

@implementation GSUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self tableView];
//    [self setTitleText:NSLocalizedString(@"账户资料", nil)];
    [self.iconAndTitleLeftBtn setTitle:@"账户资料" forState:UIControlStateNormal];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(backNameAction:) name:ModifyName object:nil];
}

- (void)backNameAction:(NSNotification *)noti {
    [self.nameBtn setTitle:noti.userInfo[@"name"] forState:UIControlStateNormal];
}

-(SZUserInfoViewModel *)viewModel{
    
    if (!_viewModel) {
        _viewModel=[SZUserInfoViewModel new];
    }
    return _viewModel;
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tableView.sectionHeaderHeight=FIT(0);
        _tableView.sectionFooterHeight = FIT(0);
        _tableView.backgroundColor = MainBackgroundColor;
        [_tableView registerClass:[SZUserInfoCommonCell class] forCellReuseIdentifier:SZUserInfoCommonCellReuseIdentifier];
        [self.view addSubview:_tableView];
        [_tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(NavigationStatusBarHeight);
            make.left.right.bottom.mas_equalTo(0);
        }];
    }
    return _tableView;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return self.viewModel.titleArr.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.viewModel.titleArr[section] count];
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return FIT(80);
    }
    return  FIT(60);
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return FIT(15);
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    
    static NSString *reuseId = @"sectionHeader";
    UITableViewHeaderFooterView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:reuseId];
    if (!headerView) {
        headerView=[[UITableViewHeaderFooterView alloc]initWithReuseIdentifier:reuseId];
        UIView* subFootView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, ScreenWidth, FIT(15))];
        [headerView addSubview:subFootView];
    }
    return headerView;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SZUserInfoCommonCell *cell = [tableView dequeueReusableCellWithIdentifier:SZUserInfoCommonCellReuseIdentifier forIndexPath:indexPath];
    [cell loadCellValueWithArray:self.viewModel.titleArr withIndexPath:indexPath];
    if (indexPath.section == 0 && indexPath.row == 0) {        
        self.imgBtn = cell.rightBtn;
    }else if (indexPath.section == 1 && indexPath.row == 0 ) {
        self.nameBtn = cell.detailBtn;
    }
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            //修改头像
            [self choosePickIcon];
        }
    }else if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            YQ_PUSH([[YQEditNickNameVC alloc] init]);
        }
    }else {
        if (indexPath.row == 0) {
            if ([[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] == 0 || [[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] == 2) {
                YQ_PUSH([[IdentityCertificationCtl alloc] init]);
            }
 
        }else if (indexPath.row == 1) {
            ResetTransactionPWDCtl *ctl = [[ResetTransactionPWDCtl alloc] init];
            ctl.titleString = @"1";
            YQ_PUSH(ctl);
        }else if (indexPath.row == 2) {
            if ([[YQPersonInformationModel sharedYQPersonInformationModel].authStatus integerValue] != 3) {
                [LCProgressHUD showMessage:@"请先进行实名认证"];
                return;
            }
            ResetTransactionPWDCtl *ctl = [[ResetTransactionPWDCtl alloc] init];
            ctl.titleString = @"2";
            YQ_PUSH(ctl);
        }else if (indexPath.row == 3) {
            YQAddressListVC *listVC = [YQAddressListVC new];
            listVC.isMeComing = @"2";
            YQ_PUSH(listVC);
        }else {
            YQ_PUSH([MePaySettingCtl new]);
        }
    }

}

- (void)choosePickIcon {
    //点击弹框选择照片 或者相机
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
            
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerController *,id> *)info {
    //获取图片
    self.icon = info[UIImagePickerControllerOriginalImage];
    
    [self selectedImg:self.icon withPicker:picker];
    [self.tableView reloadData];
}

//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // 返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 照片上传
- (void)selectedImg:(UIImage *)backImg withPicker:(UIImagePickerController *)picker{
    
    NSString *urlStr = [NSString stringWithFormat:@"%@users/userAvatar",BaseHttpUrl];
//    __block NSString *iconStr;
    [picker dismissViewControllerAnimated:YES completion:^{
        NSString* urlString=[NSString stringWithFormat:@"%@users/fileUpload",BaseHttpUrl];
        //把照片上传获取返回的URL
        [[SZHTTPSRsqManager sharedSZHTTPSRsqManager] postUploadWithUrl:urlString image:backImg successBlock:^(id responseObject) {
//            iconStr = responseObject[@"data"][@"resourceURL"];
            [self.imgBtn sd_setImageWithURL:[NSURL URLWithString:responseObject[@"data"][@"resourceURL"]]];
            [SZHTTPSReqManager postRequestWithUrlString:urlStr appendParameters:nil bodyParameters:@{@"avatar":responseObject[@"data"][@"resourceURL"]} successBlock:^(id responseObject) {
                DLog(@"%@",responseObject);
            } failureBlock:^(NSError *error) {
                
            }];
        } successBlock:^(NSError *error) {
            
        }];
    }];
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
