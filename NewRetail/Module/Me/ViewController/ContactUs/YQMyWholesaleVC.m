//
//  YQMyWholesaleVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/9.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyWholesaleVC.h"
#import "YQMyWholesaleCell.h"

@interface YQMyWholesaleVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *wholesaleTB;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation YQMyWholesaleVC
{
    NSInteger _pageStart;
    NSInteger _pageSize;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _pageStart = 1;
    _pageSize = 10;
    [self.iconAndTitleLeftBtn setTitle:@"批发券" forState:UIControlStateNormal];
    [self loadWholesaleData];
    [self loadTableView];
}

- (void)loadWholesaleData {
    NSDictionary *parameters = @{@"pageStart":StringFromLongInt(_pageStart),
                                 @"pageSize":StringFromLongInt(_pageSize),
                                 @"couponType":@"1"
                                 };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/couponList") appendParameters:parameters successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            for (NSDictionary *dict in base.data[@"dataList"]) {
                [self.dataArr addObject:dict];
                
            }
            [self.wholesaleTB reloadData];
            NSLog(@"self.dataArr.count = %ld",self.dataArr.count);
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadTableView {
    [self.view addSubview:self.wholesaleTB];
    [self.wholesaleTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom);
    }];
}

- (UITableView *)wholesaleTB {
    if (!_wholesaleTB) {
        _wholesaleTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _wholesaleTB.backgroundColor = MainBackgroundColor;
        _wholesaleTB.delegate = self;
        _wholesaleTB.dataSource = self;
        _wholesaleTB.showsVerticalScrollIndicator = NO;
        YQ_NoDataImg(_wholesaleTB);
        _wholesaleTB.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_wholesaleTB registerNib:[UINib nibWithNibName:YQ_MyWholesaleCell bundle:nil] forCellReuseIdentifier:YQ_MyWholesaleCell];
    }
    return _wholesaleTB;
}

YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return WholesaleCell_Height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(15);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQMyWholesaleCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_MyWholesaleCell forIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

@end
