//
//  YQSysSignDetailVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQSysSignDetailVC.h"
#import "YQSignDetailView.h"

@interface YQSysSignDetailVC ()

@property (nonatomic, strong) YQSignDetailView *detailView;

@end

@implementation YQSysSignDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TableViewBackGroundColor;
    [self.iconAndTitleLeftBtn setTitle:self.model.title forState:UIControlStateNormal];
    [self.view addSubview:self.detailView];
//    NSLog(@"height = %2.f",[UILabel getLabelHeightWithText:self.model.content width:ScreenWidth-2*kMargin_left font:kDescribeFont]);
    [self.detailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, [UILabel getLabelHeightWithText:self.model.content width:ScreenWidth-2*kMargin_left font:kDescribeFont]+FIT(105)));
    }];
}

- (YQSignDetailView *)detailView {
    if (!_detailView) {
        _detailView = [[YQSignDetailView alloc] init];
        [_detailView loadDetailViewWithModel:self.model];
    }
    return _detailView;
}

@end
