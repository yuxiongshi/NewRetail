//
//  YQSysSignVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQSysSignVC.h"
#import "YQSysSignCell.h"
#import "YQSysSignModel.h"
#import "YQSysSignDetailVC.h"

@interface YQSysSignVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *signTableView;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation YQSysSignVC
{
    NSInteger _current;
    NSInteger _size;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _current = 1;
    _size = 10;
    self.view.backgroundColor = TableViewBackGroundColor;
    [self.headView setHidden:YES];
    [self setupSignView];
    
    self.signTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [LCProgressHUD showLoading:@"努力加载中..."];
        //数据刷新
        [self loadSignData];
    }];
    [self.signTableView.mj_header beginRefreshing];
    
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)loadSignData {
    [self.dataArr removeAllObjects];
    if ([self.index integerValue] == 0) {
        self.index = @"2";
    }else {
        self.index = @"1";
    }
    NSDictionary *dict = @{@"current":StringFromLongInt(_current),
                           @"size":StringFromLongInt(_size),
                           @"noticeType":self.index
                           };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/notice") appendParameters:dict successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            for (NSDictionary *dic in base.data[@"dataList"]) {
                YQSysSignModel *model = [YQSysSignModel mj_objectWithKeyValues:dic];
                [self.dataArr addObject:model];
            }
            [LCProgressHUD hide];
            [self.signTableView reloadData];
            [self.signTableView.mj_header endRefreshing];
        }
    } failureBlock:^(NSError *error) {
        [self.signTableView.mj_header endRefreshing];
    }];
}

- (void)setupSignView {
    [self.view addSubview:self.signTableView];
    [self.signTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(16);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, TableView_Height_IPhoneX-FIT(100)));
    }];
}

- (UITableView *)signTableView {
    if (!_signTableView) {
        _signTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _signTableView.backgroundColor = TableViewBackGroundColor;
        _signTableView.delegate = self;
        _signTableView.dataSource = self;
        YQ_NoDataImg(_signTableView);
        [_signTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_signTableView registerClass:[YQSysSignCell class] forCellReuseIdentifier:YQ_SysSignCell];
    }
    return _signTableView;
}

YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [self.index integerValue] == 1?self.dataArr.count:0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.index integerValue] == 1?1:0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(145);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(16);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQSysSignCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_SysSignCell forIndexPath:indexPath];
    YQSysSignModel *model = self.dataArr[indexPath.section];
    [cell loadCellWithModel:model];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    YQSysSignModel *model = self.dataArr[indexPath.section];
    YQSysSignDetailVC *vc = [[YQSysSignDetailVC alloc] init];
    vc.model = model;
    YQ_PUSH(vc);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = TableViewBackGroundColor;
    return backView;
}


@end
