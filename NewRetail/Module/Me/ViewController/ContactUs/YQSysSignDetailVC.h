//
//  YQSysSignDetailVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/4.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"
#import "YQSysSignModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQSysSignDetailVC : CustomViewController

@property (nonatomic, strong) YQSysSignModel *model;

@end

NS_ASSUME_NONNULL_END
