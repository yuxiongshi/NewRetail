//
//  YQPassCardTransferVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQPassCardTransferVC.h"
#import "YQMyTransferHeadView.h"
#import "YQMyTransferMiddleView.h"
#import "YQTransferBindingView.h"

@interface YQPassCardTransferVC ()

@property (nonatomic, strong) YQMyTransferMiddleView *middleView;
@property (nonatomic, strong) YQMyTransferHeadView *transferView;
@property (nonatomic, strong) YQTransferBindingView *passCardHead;
@property (nonatomic, strong) UIButton *transferBtn;
@property (nonatomic, strong) BaseModel *transferModel;

@end

@implementation YQPassCardTransferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.iconAndTitleLeftBtn setTitle:@"划转" forState:UIControlStateNormal];
    //获取划转数据
    [self loadTransferData];
    [self setupView];
}

- (void)loadTransferData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/profileCooperate") appendParameters:nil successBlock:^(id responseObject) {
        self.transferModel = [BaseModel modelWithJson:responseObject];
        if (self.transferModel.code == 0) {
            //可用
            self.middleView.balanceLab.text = [NSString stringWithFormat:@"可用 %@ GBK",self.transferModel.data[@"balance"]];
        }else {
            [LCProgressHUD showFailure:self.transferModel.msg];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}
- (void)setupView {
    [self.view addSubview:self.passCardHead];
    [self.view addSubview:self.transferView];
    [self.view addSubview:self.middleView];
    [self.view addSubview:self.transferBtn];
    
    [self.passCardHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(NavigationStatusBarHeight);
        make.height.mas_equalTo(FIT(FIT(77)));
    }];
    
    [self.transferView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.passCardHead.mas_bottom).mas_equalTo(FIT(10));
        make.height.mas_equalTo(FIT(106));
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.transferView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(150)));
    }];
    
    [self.transferBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(37));
        make.height.mas_equalTo(kCommitButtonHeight);
    }];
    
}

- (YQTransferBindingView *)passCardHead {
    if (!_passCardHead) {
        _passCardHead = [[YQTransferBindingView alloc] init];        
    }
    return _passCardHead;
}

- (YQMyTransferHeadView *)transferView {
    if (!_transferView) {
        _transferView = [[YQMyTransferHeadView alloc] init];
        _transferView.rewardBalanceLab.text = @"果币APP";
        _transferView.AvailableBalance.text = @"天赐艾APP";
    }
    return _transferView;
}

- (YQMyTransferMiddleView *)middleView {
    if (!_middleView) {
        _middleView = [[YQMyTransferMiddleView alloc] init];
        _middleView.poundageLab.hidden = YES;
        _middleView.cnyLab.text = @"GBK";
        WeakSelf(self);
        _middleView.CommitAllBlock = ^{
            weakSelf.middleView.transferTF.text = weakSelf.transferModel.data[@"balance"];
        };
    }
    return _middleView;
}

- (UIButton *)transferBtn {
    if (!_transferBtn) {
        _transferBtn = [YQViewFactory buttonWithTitle:@"划转" titleColor:WhiteColor fontSize:FIT(15) userBold:NO target:self sel:@selector(transferAction)];
        [_transferBtn setBackgroundImage:YQ_IMAGE(@"commit_back") forState:UIControlStateNormal];
    }
    return _transferBtn;
}

- (void)transferAction {
    if (isEmptyString(self.middleView.transferTF.text)) {
        [LCProgressHUD showMessage:@"请输入划转数量"];
        return;
    }
    if ([self.middleView.transferTF.text integerValue] <= 0 || [self.middleView.transferTF.text integerValue] > [self.transferModel.data[@"balance"] integerValue]) {
        [LCProgressHUD showMessage:@"请输入正确的数量"];
        return;
    }
    [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/transTokenCooperate") appendParameters:nil bodyParameters:@{@"transNum":self.middleView.transferTF.text} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            [LCProgressHUD showSuccess:@"划转成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LCProgressHUD showFailure:base.msg];
        }
    } failureBlock:^(NSError *error) {
        
    }];
    
}


@end
