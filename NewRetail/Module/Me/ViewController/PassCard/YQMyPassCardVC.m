//
//  YQMyPassCardVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyPassCardVC.h"
#import "YQPassCardHeadView.h"
#import "YQTwoButtonView.h"
#import "YQMyWalletModel.h"
#import "YQRecordCell.h"
#import "YQBindingVC.h"
#import "YQPassCardTransferVC.h"

@interface YQMyPassCardVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) YQPassCardHeadView *passCardHead;
@property (nonatomic, strong) YQTwoButtonView *twoBtnView;

@property (nonatomic, strong) NSMutableArray *headArr;//头部的数据源
@property (nonatomic, strong) NSMutableArray *recordArr;//收支记录的数据源
@property (nonatomic, strong) UITableView *recordTB;
@property (nonatomic, strong) UILabel *recordLab;

@end

@implementation YQMyPassCardVC
{
    NSInteger _pageStart;
    NSInteger _pageSize;
    NSInteger _isBinding;
}

- (NSMutableArray *)headArr {
    if (!_headArr) {
        _headArr = [NSMutableArray array];
    }
    return _headArr;
}

- (NSMutableArray *)recordArr {
    if (!_recordArr) {
        _recordArr = [NSMutableArray array];
    }
    return _recordArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //头部数据
    [self loadWalletData];
    //是否已绑定
    [self isBindingData];
    
    self.recordTB.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //数据刷新
        [self loadRecordData];
    }];
    [self.recordTB.mj_header beginRefreshing];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.headView setHidden:YES];
    _pageStart = 1;
    _pageSize = 10;
    //头部试图
    [self setupHead];
    
    
    
}

- (void)isBindingData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/isbinding") appendParameters:nil successBlock:^(id responseObject) {
        NSDictionary *dataDict = responseObject[@"data"];
        if ([dataDict[@"isBinding"] boolValue] == YES) {
            [self.twoBtnView.upMoneyBtn setTitle:@"已绑定" forState:UIControlStateNormal];
            self->_isBinding = 1;
        }else {
            [self.twoBtnView.upMoneyBtn setTitle:@"未绑定" forState:UIControlStateNormal];
            self->_isBinding = 0;
            
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadWalletData {
    
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/myWallet") appendParameters:nil successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            YQMyWalletModel *model = [YQMyWalletModel mj_objectWithKeyValues:base.data];
            self.passCardHead.balanceLab.text = model.gbkBalance;
            self.passCardHead.rateLab.text = [NSString stringWithFormat:@"交易所汇率 1GBK≈%@",model.tradeGBKToCNYRate];
            self.passCardHead.GBKAndCNYRateLab.text = [NSString stringWithFormat:@"新零售抵扣比例 1GBK≈%@",model.gbkCNYRate];
            [self.headArr addObject:model];
            [self.recordTB reloadData];
            [self setupHead];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadRecordData {
    [self.recordArr removeAllObjects];
    NSDictionary *parameters = @{@"pageStart":StringFromLongInt(_pageStart),
                                 @"pageSize":StringFromLongInt(_pageSize),
                                 @"type":@"2"
                                 };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/transition/record") appendParameters:parameters successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            for (NSDictionary *dict in base.data[@"dataList"]) {
                YQRecordModel *model = [YQRecordModel mj_objectWithKeyValues:dict];
                [self.recordArr addObject:model];
            }
            [self.recordTB reloadData];
            [self.recordTB.mj_header endRefreshing];
        }
    } failureBlock:^(NSError *error) {
        [self.recordTB.mj_header endRefreshing];
    }];
}


- (void)setupHead {
    [self.view addSubview:self.passCardHead];
    [self.passCardHead mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.mas_equalTo(0);
        make.height.mas_equalTo(FIT(300));
    }];
    
    [self.view addSubview:self.twoBtnView];
    [self.twoBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passCardHead.mas_bottom).mas_equalTo(-FIT(25));
        make.centerX.equalTo(self.passCardHead.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(165), FIT(50)));
    }];
    
    //组头
    [self.view addSubview:self.recordLab];
    [self.recordLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.twoBtnView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(45)));
    }];
    
    [self.view addSubview:self.recordTB];
    [self.recordTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.recordLab.mas_bottom).mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
    
}

- (YQPassCardHeadView *)passCardHead {
    if (!_passCardHead) {
        _passCardHead = [[YQPassCardHeadView alloc] init];
        WeakSelf(self);
        _passCardHead.BackBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _passCardHead;
}

- (YQTwoButtonView *)twoBtnView {
    if (!_twoBtnView) {
        _twoBtnView = [[YQTwoButtonView alloc] init];
        [_twoBtnView.upMoneyBtn setTitle:@"绑定" forState:UIControlStateNormal];
        [_twoBtnView.withdrawalBtn setTitle:@"划转" forState:UIControlStateNormal];
        _twoBtnView.layer.cornerRadius = FIT(50)/2.0;
        _twoBtnView.layer.masksToBounds = YES;
        
        _twoBtnView.SelectBlock = ^(NSInteger tag) {
            if (tag == 200) {
                YQBindingVC *vc = [[YQBindingVC alloc] init];
                vc.isBinding = self->_isBinding;
                YQ_PUSH(vc);
            }else {
                if (_isBinding == 0) {
                    //未绑定划转不能进入
                    [LCProgressHUD showFailure:@"请先绑定果币APP后授权密钥"];
                    return ;
                }
                
                YQ_PUSH([YQPassCardTransferVC new]);
                
            }
            
        };
    }
    return _twoBtnView;
}

- (UILabel *)recordLab {
    if (!_recordLab) {
        _recordLab = [YQViewFactory labelWithTextColor:MainGrayColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:YES];
        _recordLab.text = @"抵扣记录";
    }
    return _recordLab;
}

- (UITableView *)recordTB {
    if (!_recordTB) {
        _recordTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _recordTB.backgroundColor = TableViewBackGroundColor;
        _recordTB.delegate = self;
        _recordTB.dataSource = self;
        YQ_NoDataImg(_recordTB);
        _recordTB.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_recordTB registerClass:[YQRecordCell class] forCellReuseIdentifier:YQ_RecordCell];
    }
    return _recordTB;
}

YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.recordArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(120);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(8);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_RecordCell forIndexPath:indexPath];
    YQRecordModel *model = self.recordArr[indexPath.section];
    [cell loadPassCardWithModel:model];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = TableViewBackGroundColor;
    return backView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


@end
