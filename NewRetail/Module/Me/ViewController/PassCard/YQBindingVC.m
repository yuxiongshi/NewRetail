//
//  YQBindingVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBindingVC.h"
#import "YQBindingView.h"

@interface YQBindingVC ()

@property (nonatomic, strong) YQBindingView *bindingView;
@property (nonatomic, strong) UILabel *desLab;
@property (nonatomic, strong) UIView *whiteBackView;
@property (nonatomic, strong) UITextField *keyTF;
@property (nonatomic, strong) UIButton *bindingBtn;

@property (nonatomic, strong) UIImageView *icon;

@end

@implementation YQBindingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //先判断是否绑定 已绑定显示绑定key
    [self setupBackView];
    
    //加载是否绑定数据
    [self loadGBInfoData];
}

- (void)loadGBInfoData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/profileCooperate") appendParameters:nil successBlock:^(id responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            [self.icon sd_setImageWithURL:[NSURL URLWithString:responseObject[@"data"][@"avatar"]] placeholderImage:YQ_IMAGE(@"gb_icon")];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupBackView {
    [self.view addSubview:self.bindingView];
    [self.bindingView addSubview:self.desLab];
    [self.bindingView addSubview:self.whiteBackView];
    
    [self.bindingView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [self.desLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(20));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(50)));
    }];

    [self.whiteBackView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(100));
        make.left.mas_equalTo(kMargin_left);
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.equalTo(self.desLab.mas_top).mas_equalTo(-FIT(20));
    }];

    //国币icon
    UIImageView *icon = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"gb_icon")];
    [self.whiteBackView addSubview:icon];
    self.icon = icon;
    [icon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(42));
        make.centerX.mas_equalTo(self.whiteBackView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(64), FIT(64)));
    }];

    //标题
    UILabel *theTitleLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:FIT(21) userBold:YES];
    theTitleLab.text = @"果币APP授权登录密钥";
    [self.whiteBackView addSubview:theTitleLab];
    [theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(icon.mas_bottom).mas_equalTo(FIT(20));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(20)));
    }];
    
    
    UIView *tfView = [[UIView alloc] init];
    tfView.backgroundColor = WhiteColor;
    tfView.layer.cornerRadius = 3;
    tfView.layer.borderColor = MainRedTextColor.CGColor;
    tfView.layer.borderWidth = 0.5;
    tfView.layer.masksToBounds = YES;
    [self.whiteBackView addSubview:tfView];
    [tfView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(50));
        make.right.mas_equalTo(-FIT(50));
        make.top.equalTo(theTitleLab.mas_bottom).mas_equalTo(FIT(22));
        make.height.mas_equalTo(FIT(40));
    }];
    
    //        输入框
    [tfView addSubview:self.keyTF];
    [self.keyTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(10));
        make.right.mas_equalTo(-FIT(10));
        make.top.mas_equalTo(0);
        make.height.mas_equalTo(FIT(40));
    }];
    
    if (self.isBinding == 1) {
        //已绑定
        [tfView setHidden:YES];
    }else {
        [tfView setHidden:NO];
    }
    
    
    [self.whiteBackView addSubview:self.bindingBtn];
    [self.bindingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(-FIT(35));
        make.left.mas_equalTo(FIT(80));
        make.right.mas_equalTo(-FIT(80));
        make.height.mas_equalTo(kCommitButtonHeight);
    }];
    
}

- (YQBindingView *)bindingView {
    if (!_bindingView) {
        _bindingView = [[YQBindingView alloc] init];
        WeakSelf(self);
        _bindingView.BackBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _bindingView;
}

- (UILabel *)desLab {
    if (!_desLab) {
        _desLab = [YQViewFactory labelWithTextColor:WhiteColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:YES];
        _desLab.numberOfLines = 0;
        _desLab.text = @"绑定果币APP后可以从钱包的币币账户划转GBK至天赐艾APP中“我的通证。";
    }
    return _desLab;
}

- (UIView *)whiteBackView {
    if (!_whiteBackView) {
        _whiteBackView = [[UIView alloc] init];
        _whiteBackView.backgroundColor = WhiteColor;
        _whiteBackView.layer.cornerRadius = 5;
        _whiteBackView.layer.masksToBounds = YES;
    }
    return _whiteBackView;
}

- (UITextField *)keyTF {
    if (!_keyTF) {
        _keyTF = [YQViewFactory textFieldWithPlaceholderText:@"输入密钥" textColor:MainBlackColor fontSize:FIT(18) userBold:NO];
        _keyTF.borderStyle = UITextBorderStyleNone;
    }
    return _keyTF;
}

- (UIButton *)bindingBtn {
    if (!_bindingBtn) {
        _bindingBtn = [YQViewFactory buttonWithTitle:@"" titleColor:WhiteColor fontSize:kNormalButtonFont userBold:YES target:self sel:@selector(bindingAction)];
        [_bindingBtn setBackgroundImage:YQ_IMAGE(@"binding") forState:UIControlStateNormal];
        if (_isBinding == 1) {//已经绑定 再次点击解除绑定
            [_bindingBtn setTitle:@"解绑" forState:UIControlStateNormal];
        }else {
            [_bindingBtn setTitle:@"绑定" forState:UIControlStateNormal];
        }
    }
    return _bindingBtn;
}

- (void)bindingAction {
    //绑定或者解除绑定
    NSDictionary *dict = @{@"publicKey":self.keyTF.text,
                           @"type":self.isBinding == 1?@"2":@"1"
                           };
    NSString *message;
    if (self.isBinding == 1) {
        message = @"解绑成功";
    }else {
        message = @"绑定成功";
    }
    [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/binding") appendParameters:nil bodyParameters:dict successBlock:^(id responseObject) {
        
        if ([responseObject[@"code"] integerValue] == 0) {
            [LCProgressHUD showSuccess:message];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            
            [LCProgressHUD showSuccess:responseObject[@"msg"]];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

@end
