//
//  YQBindingVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQBindingVC : CustomViewController

@property (nonatomic, assign) NSInteger isBinding;//是否绑定

@end

NS_ASSUME_NONNULL_END
