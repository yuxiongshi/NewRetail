//
//  YQMyOrderDetailVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyOrderDetailVC.h"
#import "YQMyOrderheadView.h"
#import "YQNoPaymentHeadView.h"
#import "YQMyOrderDetailModel.h"
#import "YQTopUpBottomView.h"

@interface YQMyOrderDetailVC ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>

@property (nonatomic, strong) YQMyOrderheadView *orderHeadView;
@property (nonatomic, strong) YQNoPaymentHeadView *noPaymentHeadView;
@property (nonatomic, strong) YQMyOrderDetailModel *model;
@property (nonatomic, assign) CGFloat headHeight;
@property (nonatomic, strong) YQTopUpBottomView *meInfo;
@property (nonatomic, strong) YQTopUpBottomView *companyInfo;
@property (nonatomic, strong) UIScrollView *backScroll;

@property (nonatomic, strong) UIView *containerView;

@property (nonatomic, strong) UIView *twoBtnView;

@property (nonatomic, strong) UIView *myAnimationView;//动画弹出试图
@property (nonatomic, strong) UIImageView *backImg;

@property (nonatomic, strong) UIImagePickerController *imagePicker;//调用相机相册

@end

@implementation YQMyOrderDetailVC
{
    NSString *_imgUrl;//接收图片
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.headView setHidden:YES];
    //获取头部数据
    
    _backScroll = [[UIScrollView alloc] init];
    _backScroll.userInteractionEnabled = YES;
    _backScroll.backgroundColor = [UIColor clearColor];
//    _backScroll.contentSize = CGSizeMake(ScreenWidth, ScreenHeight*2);
    _backScroll.pagingEnabled = YES;
    [self.view addSubview:_backScroll];//is time to you quick
    [_backScroll mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    self.containerView = [[UIView alloc] init];
    [_backScroll addSubview:self.containerView];
    [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.backScroll);
        make.width.equalTo(self.backScroll);
    }];

    
    [self loadDetailData];

    //应该写成tableView的 ==
}

- (void)loadDetailData {
    WeakSelf(self);
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/detail") appendParameters:@{@"id":self.orderModel.id} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            self.model = [YQMyOrderDetailModel mj_objectWithKeyValues:base.data];
            if (isEmptyString(self.model.companyAliUrl)) {
                self->_imgUrl = self.model.companyWechatUrl;
            }else if (isEmptyString(self.model.companyWechatUrl)) {
                self->_imgUrl = self.model.companyAliUrl;
            }
            [self.orderHeadView loadOrderHeadViewWithModel:self.model];
            NSInteger orderStatus = [self.model.orderStatus integerValue];
            //添加底部
            if (orderStatus == 1) {
                //使用另外的头部
                self.noPaymentHeadView.hidden = NO;
                [self.containerView addSubview:self.noPaymentHeadView];
                [self.noPaymentHeadView mas_updateConstraints:^(MASConstraintMaker *make) {
                    make.left.right.top.mas_equalTo(0);
                    make.height.mas_equalTo(FIT(195));
                }];
                self.noPaymentHeadView.preloadedLab.text = self.model.cashNum;
                
                
                [self.containerView addSubview:self.meInfo];
                [self.containerView addSubview:self.companyInfo];
                self.meInfo.headView.hidden = YES;
//                self.companyInfo.headView.hidden = YES;
                self.meInfo.notPayingHead.hidden = NO;
//                self.companyInfo.notPayingHead.hidden = NO;
                
                [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.companyInfo.mas_bottom).offset(10);
                }];
                //除了银行 其余的高度都是145
                CGFloat bottomView_height = 0;
                CGFloat meBottomView_height = 0;
                if ([self.model.paymentWay integerValue] == 3) {
                    bottomView_height = FIT(190+56);
                    meBottomView_height = FIT(215);
                    self.companyInfo.copyNameBtn.hidden = NO;
                    self.companyInfo.copyBankBtn.hidden = NO;
                    self.companyInfo.copyBankNumBtn.hidden = NO;
                }else {
                    bottomView_height = FIT(130+56);
                    meBottomView_height = FIT(145);
                    self.companyInfo.copyBankBtn.hidden = NO;
                    self.companyInfo.codeBtn.hidden = NO;
                    self.companyInfo.lookCodeLab.hidden = NO;
                }
                
                //再未付款时 我的信息与公司信息交换位置
                self.meInfo.desLab.hidden = NO;
                [self.companyInfo mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(0);
                    make.top.equalTo(self.noPaymentHeadView.mas_bottom).mas_equalTo(0);
                    make.height.mas_equalTo(meBottomView_height);
                }];
                
                [self.meInfo mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(0);
                    make.top.equalTo(self.companyInfo.mas_bottom).mas_equalTo(FIT(10));
                    make.height.mas_equalTo(bottomView_height);
                }];
                [self.meInfo loadMeInfoOrderDetailWithModel:self.model payWay:self.model.paymentWay];
                [self.companyInfo loadCompanyOrderDetailWithModel:self.model payWay:self.model.paymentWay];
                
                //添加底部
                [self.view addSubview:self.twoBtnView];
                [self.twoBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.mas_equalTo(0);
                    make.bottom.mas_equalTo(-Botoom_IPhoneX);
                    make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(50)));
                }];
                
                
            }else {
                //使用orderHeadView
                /**
                 1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
                 */
                self.orderHeadView.hidden = NO;
                if (orderStatus == 2) {
                    weakSelf.headHeight = FIT(280);
                }else if (orderStatus == 3) {
                    weakSelf.headHeight = FIT(350);
                }else if (orderStatus == 4 || orderStatus == 5) {
                    weakSelf.headHeight = FIT(320);
                }
                [self.containerView addSubview:self.orderHeadView];
                [self.orderHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.top.mas_equalTo(0);
                    make.height.mas_equalTo(weakSelf.headHeight);
                }];

                [self.containerView addSubview:self.meInfo];
                [self.containerView addSubview:self.companyInfo];
                
                [self.containerView mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.bottom.equalTo(self.companyInfo.mas_bottom).offset(10);
                }];
                //除了银行 其余的高度都是145
                CGFloat bottomView_height = 0;
                if ([self.model.paymentWay integerValue] == 3) {
                    bottomView_height = FIT(215);
                }else {
                    bottomView_height = FIT(145);
                    self.companyInfo.codeBtn.hidden = NO;
                    self.companyInfo.lookCodeLab.hidden = NO;
                }
                
                [self.meInfo mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(0);
                    make.top.equalTo(self.orderHeadView.mas_bottom).mas_equalTo(0);
                    make.height.mas_equalTo(bottomView_height);
                }];
                
                [self.companyInfo mas_makeConstraints:^(MASConstraintMaker *make) {
                    make.left.right.mas_equalTo(0);
                    make.top.equalTo(self.meInfo.mas_bottom).mas_equalTo(FIT(10));
                    make.height.mas_equalTo(bottomView_height);
                }];
                [self.meInfo loadMeInfoOrderDetailWithModel:self.model payWay:self.model.paymentWay];
                [self.companyInfo loadCompanyOrderDetailWithModel:self.model payWay:self.model.paymentWay];
            }
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (YQMyOrderheadView *)orderHeadView {
    if (!_orderHeadView) {
        _orderHeadView = [[YQMyOrderheadView alloc] init];
        _orderHeadView.hidden = YES;
        WeakSelf(self);
        _orderHeadView.BackBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _orderHeadView;
}

- (YQNoPaymentHeadView *)noPaymentHeadView {
    if (!_noPaymentHeadView) {
        _noPaymentHeadView = [[YQNoPaymentHeadView alloc] init];
        _noPaymentHeadView.hidden = YES;
        WeakSelf(self);
        _noPaymentHeadView.BackBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
    }
    return _noPaymentHeadView;
}

- (YQTopUpBottomView *)meInfo {
    if (!_meInfo) {
        _meInfo = [[YQTopUpBottomView alloc] init];
        _meInfo.backgroundColor = WhiteColor;
        _meInfo.headView.theTitleLab.text = @"我的付款信息";
        
    }
    return _meInfo;
}

- (YQTopUpBottomView *)companyInfo {
    if (!_companyInfo) {
        _companyInfo = [[YQTopUpBottomView alloc] init];
        _companyInfo.backgroundColor = WhiteColor;
        _companyInfo.headView.theTitleLab.text = @"公司收款信息";
        @weakify(self);
        _companyInfo.LookBlock = ^{
            @strongify(self);
            //相当于查看大图
            //1.获取图片的宽高
            UIView *backView = [[UIView alloc] initWithFrame:CGRectZero];
            backView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.3];
            [self.view addSubview:backView];
            self.myAnimationView = backView;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchBackView:)];
            [backView addGestureRecognizer:tap];
            
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:self->_imgUrl]];
            UIImage *showImage = [UIImage imageWithData:data];
            CGFloat scale;
            if (showImage.size.height > showImage.size.width) {
                scale = showImage.size.height/showImage.size.width;
            }else {
                scale = showImage.size.width/showImage.size.height;
            }
            UIImageView *bgImg = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"")];
            [bgImg sd_setImageWithURL:[NSURL URLWithString:self->_imgUrl]];
            [backView addSubview:bgImg];
            self.backImg = bgImg;
            [bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.mas_equalTo(self.view.mas_centerX);
                make.centerY.mas_equalTo(self.view.mas_centerY);
                make.size.mas_equalTo(CGSizeMake(ScreenWidth, ScreenWidth*scale));
            }];
            
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionLayoutSubviews animations:^{
                backView.frame = CGRectMake(0, 0, ScreenWidth, ScreenHeight);
            } completion:^(BOOL finished) {
                NSLog(@"创建完成");
            }];
        };
    }
    return _companyInfo;
}

- (void)touchBackView:(UITapGestureRecognizer *)tap {
    [UIView performSystemAnimation:UISystemAnimationDelete onViews:@[self.myAnimationView] options:UIViewAnimationOptionLayoutSubviews animations:^{
        NSLog(@"--------");
    } completion:^(BOOL finished) {
        NSLog(@"销毁结束");
    }];
}

- (UIView *)twoBtnView {
    if (!_twoBtnView) {
        _twoBtnView = [[UIView alloc] init];
        _twoBtnView.backgroundColor = WhiteColor;
//        _twoBtnView.hidden = YES;
        NSArray *titleArr = @[@"取消订单",@"标记为已付款"];
        NSArray *titleColorArr = @[MainBlackColor,WhiteColor];
        NSArray *backgroundArr = @[WhiteColor,MainRedTextColor];
        CGFloat button_W = ScreenWidth/2.0;
        for (NSInteger i = 0; i < 2; i++) {
            UIButton *btn = [YQViewFactory buttonWithTitle:titleArr[i] titleColor:titleColorArr[i] fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(selectAction:)];
            btn.tag = 10+i;
            [btn setBackgroundColor:backgroundArr[i]];
            [_twoBtnView addSubview:btn];
            [btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(button_W*i);
                make.bottom.mas_equalTo(0);
                make.size.mas_equalTo(CGSizeMake(button_W, FIT(50)));
            }];
        }
    }
    return _twoBtnView;
}

- (void)selectAction:(UIButton *)sender {
    NSString *status;
    NSString *message;
    if (sender.tag == 10) {
        status = @"4";
        message = @"取消成功";
        //取消与标记付款
        
        [self signStatusWithMessage:message status:status certificate:@""];
    }else {
        [JXTAlertView showAlertViewWithTitle:@"温馨提示" message:@"请上传付款凭证" cancelButtonTitle:@"取消" otherButtonTitle:@"确认" cancelButtonBlock:^(NSInteger buttonIndex) {
            
        } otherButtonBlock:^(NSInteger buttonIndex) {
            [self callPhotosShow];
        }];
        
    }
}

//数据上传
- (void)signStatusWithMessage:(NSString *)message status:(NSString *)status certificate:(NSString *)certificate {
    NSDictionary *parameters = @{@"id":self.orderModel.id,
                                 @"status":status,
                                 @"certificate":certificate
                                 };
    [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/cannelOrPay") appendParameters:nil bodyParameters:parameters successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            [LCProgressHUD showMessage:message];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LCProgressHUD showFailure:responseObject[@"data"]];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)callPhotosShow {
    //点击弹框选择照片 或者相机
    _imagePicker = [[UIImagePickerController alloc] init];
    _imagePicker.delegate = self;
    _imagePicker.allowsEditing = YES;
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cameraAction = [UIAlertAction actionWithTitle:@"从相机拍照" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
            self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.imagePicker animated:YES completion:nil];
            
        }
    }];
    
    UIAlertAction *photoAction = [UIAlertAction actionWithTitle:@"从相册选择" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.imagePicker animated:YES completion:nil];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        NSLog(@"点击了取消");
    }];
    
    [actionSheet addAction:cameraAction];
    [actionSheet addAction:photoAction];
    [actionSheet addAction:cancelAction];
    [self presentViewController:actionSheet animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<UIImagePickerController *,id> *)info {
    //获取图片
    UIImage *backImg = info[UIImagePickerControllerOriginalImage];
    [self  senderImg:backImg withPicker:picker];
    
}

//按取消按钮时候的功能
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    // 返回
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - 照片上传
- (void)senderImg:(UIImage *)backImg withPicker:(UIImagePickerController *)picker{
    [LCProgressHUD showLoading:@""];
    [picker dismissViewControllerAnimated:YES completion:^{
        NSString* urlString=[NSString stringWithFormat:@"%@users/fileUpload",BaseHttpUrl];
        //把照片上传获取返回的URL
        [[SZHTTPSRsqManager sharedSZHTTPSRsqManager] postUploadWithUrl:urlString image:backImg successBlock:^(id responseObject) {
            BaseModel *base = [BaseModel modelWithJson:responseObject];
            if (base.code == 0) {
                [self signStatusWithMessage:@"操作成功" status:@"2" certificate:responseObject[@"data"][@"resourceURL"]];
            }else {
                [LCProgressHUD hide];
            }
            
        } successBlock:^(NSError *error) {
            
        }];
    }];
    
}


@end
