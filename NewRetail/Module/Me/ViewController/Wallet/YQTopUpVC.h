//
//  YQTopUpVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface YQTopUpVC : CustomViewController


@property (nonatomic, copy) NSString *cashType;//区分是提现还是充值

@end

NS_ASSUME_NONNULL_END
