//
//  YQFreezeBalanceVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"
#import "YQMyWalletModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQFreezeBalanceVC : CustomViewController

@property (nonatomic, strong) YQMyWalletModel *headModel;

@end

NS_ASSUME_NONNULL_END
