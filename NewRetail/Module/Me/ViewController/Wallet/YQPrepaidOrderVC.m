//
//  YQPrepaidOrderVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQPrepaidOrderVC.h"
#import "YQMyOrderCell.h"
#import "YQMyOrderModel.h"
#import "YQMyOrderDetailVC.h"

@interface YQPrepaidOrderVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *orderTB;
@property (nonatomic, strong) NSMutableArray *dataArr;

@end

@implementation YQPrepaidOrderVC
{
    NSInteger _pageStart;
    NSInteger _pageSize;
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //下拉刷新
    self.orderTB.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self setupData];
    }];
    //马上进入刷新状态
    [self.orderTB.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.iconAndTitleLeftBtn setTitle:[self.cashType integerValue] == 1?@"充值订单":@"提现订单" forState:UIControlStateNormal];
    _pageStart = 1;
    _pageSize = 10;
    
    [self setupOrderView];
}

- (void)setupData {
    [self.dataArr removeAllObjects];
    NSDictionary *dict = @{@"cashType":self.cashType,
                           @"pageStart":StringFromLongInt(_pageStart),
                           @"pageSize":StringFromLongInt(_pageSize)
                           };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/list") appendParameters:dict successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            //a成功
            for (NSDictionary *dict in base.data[@"dataList"]) {
                YQMyOrderModel *model = [YQMyOrderModel mj_objectWithKeyValues:dict];
                [self.dataArr addObject:model];
            }
            [self.orderTB reloadData];
            [self.orderTB.mj_header endRefreshing];
        }
    } failureBlock:^(NSError *error) {
        [self.orderTB.mj_header endRefreshing];
    }];
}

- (void)setupOrderView {
    [self.view addSubview:self.orderTB];
    [self.orderTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(0);
        make.bottom.mas_equalTo(0);
    }];
}

- (UITableView *)orderTB {
    if (!_orderTB) {
        _orderTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _orderTB.backgroundColor = TableViewBackGroundColor;
        _orderTB.delegate = self;
        _orderTB.dataSource = self;
        YQ_NoDataImg(_orderTB);
        _orderTB.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_orderTB registerNib:[UINib nibWithNibName:YQ_MyOrderCell bundle:nil] forCellReuseIdentifier:YQ_MyOrderCell];
    }
    return _orderTB;
}

YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.dataArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return orderCell_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 0?0:FIT(10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQMyOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_MyOrderCell forIndexPath:indexPath];
    //区分入口
    NSString *czOrTX;
    if ([self.cashType integerValue] == 1) {
        czOrTX = @"充值";
    }else {
        czOrTX = @"提现";
    }
    YQMyOrderModel *model = self.dataArr[indexPath.section];
    if ([model.orderStatus integerValue] != 3) {
        cell.amountOne.hidden = YES;
        cell.theTitleLabTwo.hidden = YES;
    }else {
        cell.amountOne.hidden = NO;
        cell.theTitleLabTwo.hidden = NO;
    }
    NSDictionary *threeLabTextDict;
    //1：未付款，2：确认中，3：成功，4：用户取消，5：支付超时取消
    if ([self.cashType integerValue] == 1) {
        threeLabTextDict = @{@"1":@"待付款金额",
                             @"2":@"预充值金额",
                             @"3":@"实际充值金额",
                             @"4":@"预充值金额",
                             @"5":@"预充值金额"
                             };
    }else {
        threeLabTextDict = @{@"1":@"待付款金额",
                             @"2":@"预提现金额",
                             @"3":@"实际提现金额",
                             @"4":@"预提现金额",
                             @"5":@"预提现金额"
                             };
    }
    
    NSDictionary *statusLabDict = @{@"1":@"待付款",
                                    @"2":@"确认中",
                                    @"3":@"已成功",
                                    @"4":@"已取消",
                                    @"5":@"已取消"};
    NSDictionary *colorDict = @{@"1":@"FF873E",
                                @"2":@"E13134",
                                @"3":@"16AA02",
                                @"4":@"808080",
                                @"5":@"808080"};
    [cell loadCellDataWithModel:model czOrTX:czOrTX status:[statusLabDict objectForKey:model.orderStatus] statusColor:[colorDict objectForKey:model.orderStatus]  threeLabText:[threeLabTextDict objectForKey:model.orderStatus]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    YQMyOrderModel *model = self.dataArr[indexPath.section];
    YQMyOrderDetailVC *detail = [[YQMyOrderDetailVC alloc] init];
    detail.orderModel = model;
    YQ_PUSH(detail);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

@end
