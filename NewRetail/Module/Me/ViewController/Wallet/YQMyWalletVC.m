//
//  YQMyWalletVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyWalletVC.h"
#import "YQMyWalletView.h"
#import "YQTwoButtonView.h"
#import "YQMyWalletModel.h"
#import "YQRecordCell.h"
#import "YQRecordModel.h"
#import "YQMyTransferVC.h"
#import "YQFreezeBalanceVC.h"
#import "YQTopUpVC.h"
#import "MePaySettingCtl.h"

@interface YQMyWalletVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) YQMyWalletView *walletView;
@property (nonatomic, strong) YQTwoButtonView *twoBtnView;

@property (nonatomic, strong) NSMutableArray *headArr;//头部的数据源
@property (nonatomic, strong) UITableView *recordTB;
@property (nonatomic, strong) NSMutableArray *recordArr;//收支记录的数据源
@property (nonatomic, strong) UILabel *recordLab;


@property (nonatomic, assign) NSInteger pageStart;
@property (nonatomic, assign) NSInteger pageSize;

@end

@implementation YQMyWalletVC
{
    NSInteger _pageStart;
    NSInteger _pageSize;
}
- (NSMutableArray *)headArr {
    if (!_headArr) {
        _headArr = [NSMutableArray array];
    }
    return _headArr;
}

- (NSMutableArray *)recordArr {
    if (!_recordArr) {
        _recordArr = [NSMutableArray array];
    }
    return _recordArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = TableViewBackGroundColor;
    _pageStart = 1;
    _pageSize = 10;
    //获取钱包数据
    [self loadWalletData];
    [self.headView setHidden:YES];
//    self.recordTB.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
//        //数据刷新
//        [self loadRecordData];
//    }];
//    [self.recordTB.mj_header beginRefreshing];
    
    WeakSelf(self);
    [LTREFRESH refreshView:self.recordTB refreshType:RefreshTypeHeaderAndNormalFooter headerRefreshBlock:^{
        weakSelf.pageStart = 1;
        weakSelf.pageSize = 10;
        [weakSelf loadRecordData];
    } footerRefreshBlock:^{
        weakSelf.pageStart += 1;
        weakSelf.pageSize += 10;
        [weakSelf loadRecordData];
    }];
    //进入就刷新
    HEADER_BEGIN_REFRESH;
}

- (void)loadWalletData {
    
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/myWallet") appendParameters:nil successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            YQMyWalletModel *model = [YQMyWalletModel mj_objectWithKeyValues:base.data];
            [self.headArr addObject:model];
            [self.recordTB reloadData];
            [self setupWalletView];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadRecordData {
    [self.recordArr removeAllObjects];
    NSDictionary *parameters = @{@"pageStart":StringFromLongInt(_pageStart),
                           @"pageSize":StringFromLongInt(_pageSize),
                           @"type":@"1"
                           };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/transition/record") appendParameters:parameters successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            for (NSDictionary *dict in base.data[@"dataList"]) {
                YQRecordModel *model = [YQRecordModel mj_objectWithKeyValues:dict];
                [self.recordArr addObject:model];
            }
//            [self.recordTB.mj_header endRefreshing];
            HEADER_END_REFRESHING
            [self.recordTB reloadData];
            
        }
    } failureBlock:^(NSError *error) {
        HEADER_END_REFRESHING
//        [self.recordTB.mj_header endRefreshing];
    }];
}

- (void)setupWalletView {
    [self.view addSubview:self.walletView];
    [self.walletView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(300)));
    }];
    
    [self.view addSubview:self.twoBtnView];
    [self.twoBtnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.walletView.mas_bottom).mas_equalTo(-FIT(25));
        make.centerX.equalTo(self.walletView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(FIT(165), FIT(50)));
    }];
    
    //组头
    [self.view addSubview:self.recordLab];
    [self.recordLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.twoBtnView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(45)));
    }];
    
    [self.view addSubview:self.recordTB];
    [self.recordTB mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.recordLab.mas_bottom).mas_equalTo(0);
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-Botoom_IPhoneX);
    }];
    
}

#pragma mark - init
- (YQMyWalletView *)walletView {
    if (!_walletView) {
        _walletView = [[YQMyWalletView alloc] init];
        WeakSelf(self);
        _walletView.BackBlock = ^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        };
        
        _walletView.TransferBlock = ^(NSInteger tag) {
            if (tag == 100) {
                YQMyTransferVC *transfer = [[YQMyTransferVC alloc] init];
                YQMyWalletModel *model = [weakSelf.headArr lastObject];
                transfer.recordModel = model;
                YQ_PUSH(transfer);
            }else {
                YQFreezeBalanceVC *vc = [[YQFreezeBalanceVC alloc] init];
                YQMyWalletModel *headModel = [weakSelf.headArr lastObject];
                vc.headModel = headModel;
                YQ_PUSH(vc);
            }
        };
        
        
        YQMyWalletModel *model = [self.headArr lastObject];
        _walletView.balanceLab.text = [NSString stringWithFormat:@"￥%@",model.balance];
        _walletView.rewardBalanceLab.text = [NSString stringWithFormat:@"￥%@",model.rewardBalance];
        _walletView.freezeBalanceLab.text = [NSString stringWithFormat:@"￥%@",model.freezeBalance];
    }
    return _walletView;
}

- (YQTwoButtonView *)twoBtnView {
    if (!_twoBtnView) {
        _twoBtnView = [[YQTwoButtonView alloc] init];
        _twoBtnView.layer.cornerRadius = FIT(50)/2.0;
        _twoBtnView.layer.masksToBounds = YES;
        WeakSelf(self);
        _twoBtnView.SelectBlock = ^(NSInteger tag) {
            
            if ([[YQPersonInformationModel sharedYQPersonInformationModel].isPaymentWay boolValue] == true) {
                NSString *cashType;
                if (tag == 200) {
                    cashType = @"1";
                    
                }else {
                    cashType = @"2";
                }
                YQTopUpVC *vc = [[YQTopUpVC alloc] init];
                vc.cashType = cashType;
                [weakSelf.navigationController pushViewController:vc animated:YES];
            }else {
                [JXTAlertView showAlertViewWithTitle:@"温馨提示" message:@"请您先绑定支付方式" cancelButtonTitle:@"取消" otherButtonTitle:@"去绑定" cancelButtonBlock:^(NSInteger buttonIndex) {
                    NSLog(@"cancel");
                } otherButtonBlock:^(NSInteger buttonIndex) {
                    YQ_PUSH([MePaySettingCtl new]);
                }];
            }
        };
    }
    return _twoBtnView;
}

- (UILabel *)recordLab {
    if (!_recordLab) {
        _recordLab = [YQViewFactory labelWithTextColor:MainGrayColor textAlignment:NSTextAlignmentLeft fontSize:FIT(18) userBold:YES];
        _recordLab.text = @"收支记录";
    }
    return _recordLab;
}

- (UITableView *)recordTB {
    if (!_recordTB) {
        _recordTB = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _recordTB.backgroundColor = TableViewBackGroundColor;
        _recordTB.delegate = self;
        _recordTB.dataSource = self;
        YQ_NoDataImg(_recordTB);
        _recordTB.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_recordTB registerClass:[YQRecordCell class] forCellReuseIdentifier:YQ_RecordCell];
    }
    return _recordTB;
}

YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.recordArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(120);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(8);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_RecordCell forIndexPath:indexPath];
    YQRecordModel *model = self.recordArr[indexPath.section];
    [cell loadDataWithModel:model];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = TableViewBackGroundColor;
    return backView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
