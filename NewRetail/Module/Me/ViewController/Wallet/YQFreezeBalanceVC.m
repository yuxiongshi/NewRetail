//
//  YQFreezeBalanceVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/7.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQFreezeBalanceVC.h"

@interface YQFreezeBalanceVC ()

/**
 冻结余额
 */
@property (nonatomic, strong) UILabel *freezeLab;

/**
 健康保障基金
 */
@property (nonatomic, strong) UILabel *healthLab;

/**
 余额提现中
 */
@property (nonatomic, strong) UILabel *withdrawalLab;

@property (nonatomic, strong) UILabel *theTitleLabOne;

@property (nonatomic, strong) UILabel *theTitleLabTwo;

@property (nonatomic, strong) UILabel *theTitleLabThree;

@end

@implementation YQFreezeBalanceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.iconAndTitleLeftBtn setTitle:@"冻结余额" forState:UIControlStateNormal];
    self.view.backgroundColor = TableViewBackGroundColor;
    [self setupView];
    
}

- (void)setupView {
    //冻结
    UIView *backView1 = [[UIView alloc] init];
    backView1.backgroundColor = WhiteColor;
    [self.view addSubview:backView1];
    
    UIView *backView2 = [[UIView alloc] init];
    backView2.backgroundColor = WhiteColor;
    [self.view addSubview:backView2];
    
    UIView *backView3= [[UIView alloc] init];
    backView3.backgroundColor = WhiteColor;
    [self.view addSubview:backView3];
    
    [backView1 addSubview:self.theTitleLabOne];
    [backView1 addSubview:self.freezeLab];
    [backView2 addSubview:self.theTitleLabTwo];
    [backView2 addSubview:self.healthLab];
    [backView3 addSubview:self.theTitleLabThree];
    [backView3 addSubview:self.withdrawalLab];
    
    //view1
    [backView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(0);
        make.height.mas_equalTo(FIT(124));
    }];
    
    [self.theTitleLabOne mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(FIT(30));
        make.height.mas_equalTo(FIT(15));
    }];
    
    [self.freezeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(backView1.mas_centerX);
        make.top.equalTo(self.theTitleLabOne.mas_bottom).mas_equalTo(FIT(20));
    }];
    
    //将康保障基金view2
    [backView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(backView1.mas_bottom).mas_equalTo(2);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0-1, FIT(95)));
    }];
    
    [self.theTitleLabTwo mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(FIT(20));
        make.height.mas_equalTo(FIT(15));
    }];
    
    [self.healthLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.theTitleLabTwo.mas_bottom).mas_equalTo(FIT(15));
        make.centerX.mas_equalTo(backView2.mas_centerX);
    }];
    
    //view3
    [backView3 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.top.equalTo(backView1.mas_bottom).mas_equalTo(2);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0-1, FIT(95)));
    }];
    
    [self.theTitleLabThree mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(20));
        make.centerX.mas_equalTo(backView3.mas_centerX);
    }];
    
    [self.withdrawalLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.theTitleLabThree.mas_bottom).mas_equalTo(FIT(15));
        make.centerX.mas_equalTo(backView3.mas_centerX);
    }];
    
}

- (UILabel *)theTitleLabOne {
    if (!_theTitleLabOne) {
        _theTitleLabOne = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _theTitleLabOne.text = @"冻结余额 (全部)";
    }
    return _theTitleLabOne;
}

- (UILabel *)freezeLab {
    if (!_freezeLab) {
        _freezeLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:FIT(36) userBold:YES];
        _freezeLab.text = [NSString stringWithFormat:@"￥%@",self.headModel.freezeBalance];
    }
    return _freezeLab;
}

- (UILabel *)theTitleLabTwo {
    if (!_theTitleLabTwo) {
        _theTitleLabTwo = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _theTitleLabTwo.text = @"健康保障基金";
    }
    return _theTitleLabTwo;
}

- (UILabel *)healthLab {
    if (!_healthLab) {
        _healthLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:FIT(30) userBold:YES];
        _healthLab.text = [NSString stringWithFormat:@"￥%@",self.headModel.healthBalance];
    }
    return _healthLab;
}

- (UILabel *)theTitleLabThree {
    if (!_theTitleLabThree) {
        _theTitleLabThree = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _theTitleLabThree.text = @"余额提现中";
    }
    return _theTitleLabThree;
}

- (UILabel *)withdrawalLab {
    if (!_withdrawalLab) {
        _withdrawalLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentCenter fontSize:FIT(30) userBold:YES];
        _withdrawalLab.text = [NSString stringWithFormat:@"￥%@",self.headModel.healthBalance];
    }
    return _withdrawalLab;
}

@end
