//
//  YQMyTransferVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"
#import "YQMyWalletModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyTransferVC : CustomViewController

@property (nonatomic, strong) YQMyWalletModel *recordModel;

@end

NS_ASSUME_NONNULL_END
