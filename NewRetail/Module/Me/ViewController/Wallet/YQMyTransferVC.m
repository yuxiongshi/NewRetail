//
//  YQMyTransferVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/5.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTransferVC.h"
#import "YQMyTransferHeadView.h"
#import "YQMyTransferMiddleView.h"

@interface YQMyTransferVC ()

@property (nonatomic, strong) YQMyTransferMiddleView *middleView;
@property (nonatomic, strong) YQMyTransferHeadView *transferView;
@property (nonatomic, strong) UIButton *transferBtn;

@end

@implementation YQMyTransferVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"recordModel = %@",self.recordModel);
    [self.iconAndTitleLeftBtn setTitle:@"划转" forState:UIControlStateNormal];
    //获取划转数据
    [self loadTransferData];
    [self setupView];
}

- (void)loadTransferData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/transferConfig") appendParameters:nil successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            //可用
            
            self.middleView.balanceLab.text = [NSString stringWithFormat:@"可用 %@ CNY",StringFromLongInt(base.data[@"allbonu"])];
            
            self.middleView.poundageLab.text = [NSString stringWithFormat:@"手续费 %@ %%",base.data[@"bkgeRate"]];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupView {
    [self.view addSubview:self.transferView];
    [self.view addSubview:self.middleView];
    [self.view addSubview:self.transferBtn];
    
    [self.transferView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(NavigationStatusBarHeight);
        make.height.mas_equalTo(FIT(106));
    }];
    
    [self.middleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.transferView.mas_bottom).mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(150)));
    }];
    
    [self.transferBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(37));
        make.height.mas_equalTo(kCommitButtonHeight);
    }];
    
}

- (YQMyTransferHeadView *)transferView {
    if (!_transferView) {
        _transferView = [[YQMyTransferHeadView alloc] init];
    }
    return _transferView;
}

- (YQMyTransferMiddleView *)middleView {
    if (!_middleView) {
        _middleView = [[YQMyTransferMiddleView alloc] init];
        
    }
    return _middleView;
}

- (UIButton *)transferBtn {
    if (!_transferBtn) {
        _transferBtn = [YQViewFactory buttonWithTitle:@"划转" titleColor:WhiteColor fontSize:FIT(15) userBold:NO target:self sel:@selector(transferAction)];
        [_transferBtn setBackgroundImage:YQ_IMAGE(@"commit_back") forState:UIControlStateNormal];
    }
    return _transferBtn;
}

- (void)transferAction {
    if (isEmptyString(self.middleView.transferTF.text)) {
        [LCProgressHUD showMessage:@"请输入划转数量"];
        return;
    }
    
    [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"sys/wallet/transfer") appendParameters:nil bodyParameters:@{@"transNum":self.middleView.transferTF.text} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            [LCProgressHUD showSuccess:@"划转成功"];
            [self.navigationController popViewControllerAnimated:YES];
        }else {
            [LCProgressHUD showFailure:base.msg];
        }
    } failureBlock:^(NSError *error) {
        
    }];
    
}

@end
