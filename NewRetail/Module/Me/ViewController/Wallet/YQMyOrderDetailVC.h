//
//  YQMyOrderDetailVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/10.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"
#import "YQMyOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMyOrderDetailVC : CustomViewController

@property (nonatomic, strong) YQMyOrderModel *orderModel;

@end

NS_ASSUME_NONNULL_END
