//
//  YQPrepaidOrderVC.h
//  NewRetail
//
//  Created by yuqin on 2019/7/8.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "CustomViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQPrepaidOrderVC : CustomViewController

@property (nonatomic, copy) NSString *cashType;//区分入口 1为充值 2为提现

@end

NS_ASSUME_NONNULL_END
