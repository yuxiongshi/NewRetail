//
//  YQMyTeamVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/11.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMyTeamVC.h"
#import "YQMyTeamHeadView.h"
#import "YQMyTeamCell.h"
#import "YQMyTeamBottomView.h"

@interface YQMyTeamVC ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) YQMyTeamHeadView *teamHeadView;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) UITableView *teamTableView;
@property (nonatomic, strong) YQMyTeamBottomView *bottomView;


@end

@implementation YQMyTeamVC

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UIColorFromRGB(0xf5f5f5);
    [self.iconAndTitleLeftBtn setHidden:YES];
    [self.btnLeft setHidden:NO];
    self.txtTitle.text = @"我的团队";
    
    //获取团队数据
    [self loadTeamData];
}

- (void)loadTeamData {
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"users/invitation") appendParameters:nil successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            YQMyTeamModel *model = [YQMyTeamModel mj_objectWithKeyValues:base.data];
            self.teamHeadView.zjRecommended.text = [NSString stringWithFormat:@"%@人",model.fristFriend];
            self.teamHeadView.indirectRecommended.text = [NSString stringWithFormat:@"%@人",model.secondFriend];
            NSDictionary *dict = @{@"1":@"店员",
                                   @"2":@"主管",
                                   @"3":@"店长",
                                   @"4":@"区域代理",
                                   @"5":@"市代理"
                                   };
            self.teamHeadView.myLevel.text = [dict objectForKey:model.role];
            self.bottomView.downLoadLab.text = model.downloadUrl;
            self.bottomView.referralCodeLab.text = [YQPersonInformationModel sharedYQPersonInformationModel].referralCode;
            self.bottomView.TeamCopyBlock = ^(NSInteger tag) {
                NSString *str;
                if (tag == 10) {
                    str = model.downloadUrl;
                }else if(tag == 11){
                    str = [YQPersonInformationModel sharedYQPersonInformationModel].referralCode;
                }
                NSLog(@"str = %@",str);
                UIPasteboard *paste = [UIPasteboard generalPasteboard];
                [paste setString:str];
                if (paste == nil) {
                    [LCProgressHUD showFailure:@"复制失败"];
                }else {
                    [LCProgressHUD showSuccess:@"已复制到粘贴板"];
                }
                
            };
            [self.dataArr addObject:model];
            [self.teamTableView reloadData];
            [self loadTeamHeadView];
        }
        
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)loadTeamHeadView {
    [self.view addSubview:self.teamHeadView];
    [self.teamHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(12));
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(FIT(12));
        make.right.mas_equalTo(-FIT(12));
        make.height.mas_equalTo(FIT(145));
    }];
    
    [self.view addSubview:self.teamTableView];
    [self.teamTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.equalTo(self.teamHeadView.mas_bottom).mas_equalTo(FIT(10));
//        make.height.mas_equalTo(FIT(300));
        make.bottom.mas_equalTo(0);
    }];
    
}

- (YQMyTeamHeadView *)teamHeadView {
    if (!_teamHeadView) {
        _teamHeadView = [[YQMyTeamHeadView alloc] init];
    }
    return _teamHeadView;
}

- (UITableView *)teamTableView {
    if (!_teamTableView) {
        _teamTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _teamTableView.backgroundColor = WhiteColor;
        _teamTableView.delegate = self;
        _teamTableView.dataSource = self;
        _teamTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_teamTableView registerClass:[YQMyTeamCell class] forCellReuseIdentifier:YQ_MyTeamCell];
    }
    return _teamTableView;
}

- (YQMyTeamBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[YQMyTeamBottomView alloc] init];
    }
    return _bottomView;
}

#pragma mark - 代理

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return FIT(42);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return FIT(320);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQMyTeamCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_MyTeamCell forIndexPath:indexPath];
    YQMyTeamModel *model = [self.dataArr lastObject];
    NSArray *moneyArr = @[model.directBonus,model.inDirectBonus,model.coachBonus,model.achievementBonus,model.administrationBonus,model.allBonus];
    cell.moneyLab.text = [NSString stringWithFormat:@"￥%@",moneyArr[indexPath.row]];
    if (indexPath.row == 0) {
        cell.leftLab.text = @"直接奖累计";
        [self leftContentTextWith:@"直接奖" label:cell.leftLab];
    }else if (indexPath.row == 1) {
        cell.leftLab.text = @"间接奖累计";
        [self leftContentTextWith:@"间接奖" label:cell.leftLab];
    }else if (indexPath.row == 2) {
        cell.leftLab.text = @"辅导奖累计";
        [self leftContentTextWith:@"辅导奖" label:cell.leftLab];
    }else if (indexPath.row == 3) {
        cell.leftLab.text = @"业绩奖累计";
        [self leftContentTextWith:@"业绩奖" label:cell.leftLab];
    }else if (indexPath.row == 4) {
        cell.leftLab.text = @"管理奖累计";
        [self leftContentTextWith:@"管理奖" label:cell.leftLab];
    }else {
        cell.leftLab.text = @"全部";
        cell.leftLab.textColor = UIColorFromRGB(0xcccccc);
        [cell.leftLab mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(FIT(90));
        }];
    }
    return cell;
}

- (void)leftContentTextWith:(NSString *)text label:(UILabel *)label{
    [label setAttributedTextColorWithBeforeString:text beforeColor:MainRedTextColor afterString:@"累计" afterColor:UIColorFromRGB(0xcccccc)];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return self.bottomView;
}

@end
