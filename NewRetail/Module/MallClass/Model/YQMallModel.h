//
//  YQMallModel.h
//  NewRetail
//
//  Created by yuqin on 2019/6/22.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMallModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 商品
 */
@property (nonatomic, copy) NSString *goodName;

/**
 标签
 */
@property (nonatomic, copy) NSString *goodTitile;

/**
 现价
 */
@property (nonatomic, copy) NSString *price;
/**
 库存
 */
@property (nonatomic, copy) NSString *stock;
/**
 级别
 */
@property (nonatomic, copy) NSString *level;
/**
 净含量
 */
@property (nonatomic, copy) NSString *weight;
/**
 商品缩略图
 */
@property (nonatomic, copy) NSString *goodImgUrl;
/**
 商品详情图
 */
@property (nonatomic, copy) NSString *goodImgDetailUrl;
/**
 原价
 */
@property (nonatomic, copy) NSString *originalPrice;
/**
 1.市价商品 2：批发商品
 */
@property (nonatomic, copy) NSString *type;
/**
 createTime
 */
@property (nonatomic, copy) NSString *createTime;
/**
 updateTime
 */
@property (nonatomic, copy) NSString *updateTime;
/**
 createBy
 */
@property (nonatomic, copy) NSString *createBy;

/**
 updateBy
 */
@property (nonatomic, copy) NSString *updateBy;

@end

NS_ASSUME_NONNULL_END
