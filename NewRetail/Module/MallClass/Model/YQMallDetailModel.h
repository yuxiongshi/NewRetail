//
//  YQMallDetailModel.h
//  NewRetail
//
//  Created by yuqin on 2019/6/27.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMallDetailModel : UIView

/**
  goodTitle
 */
@property (nonatomic, copy) NSString *goodTitle;

/**
 price
 */
@property (nonatomic, copy) NSString *price;

/**
 faceValue 批发券减去的价格
 */
@property (nonatomic, copy) NSString *faceValue;

/**
 库存
 */
@property (nonatomic, copy) NSString *stock;

/**
 goodImgUrl
 */
@property (nonatomic, copy) NSString *goodImgUrl;

/**
 goodId
 */
@property (nonatomic, copy) NSString *goodId;
@end

NS_ASSUME_NONNULL_END
