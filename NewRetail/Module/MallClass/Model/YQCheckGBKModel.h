//
//  YQCheckGBKModel.h
//  NewRetail
//
//  Created by yuqin on 2019/6/28.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQCheckGBKModel : NSObject

/**
 gbk与人民币的汇率
 */
@property (nonatomic, copy) NSString *gbkCnyRate;

/**
 钱与GBK的占比
 */
@property (nonatomic, copy) NSString *moneyGBKRate;

/**
 gbk的数量
 */
@property (nonatomic, copy) NSString *gbkNum;

@end

NS_ASSUME_NONNULL_END
