//
//  YQPayModel.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQPayModel : NSObject

/**
 id
 */
@property (nonatomic, copy) NSString *id;

/**
 地区
 */
@property (nonatomic, copy) NSString *countryCode;

/**
 名称
 */
@property (nonatomic, copy) NSString *name;

/**
 电话号码
 */
@property (nonatomic, copy) NSString *phone;

/**
 省份
 */
@property (nonatomic, copy) NSString *province;

/**
 城市
 */
@property (nonatomic, copy) NSString *city;

/**
 区域
 */
@property (nonatomic, copy) NSString *area;

/**
 地址详情
 */
@property (nonatomic, copy) NSString *addressDetail;

/**
 isDefault 0 /1 1就是默认地址
 */
@property (nonatomic, copy) NSString *isDefault;

/**
 userId
 */
@property (nonatomic, copy) NSString *userId;

/**
 createTime
 */
@property (nonatomic, copy) NSString *createTime;

/**
 createBy
 */
@property (nonatomic, copy) NSString *createBy;

/**
 updateTime
 */
@property (nonatomic, copy) NSString *updateTime;

/**
 updateBy
 */
@property (nonatomic, copy) NSString *updateBy;




@end

NS_ASSUME_NONNULL_END
