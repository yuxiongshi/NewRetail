//
//  YQMallHeadView.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQMallHeadView : UIView

@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UIView *middleLineView;
@property (nonatomic, strong) UIView *bottomView;//下划线

@property (nonatomic, copy) void (^SelectBlock) (NSInteger index);

@end

NS_ASSUME_NONNULL_END
