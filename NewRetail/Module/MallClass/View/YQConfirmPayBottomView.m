//
//  YQConfirmPayBottomView.m
//  NewRetail
//
//  Created by yuqin on 2019/6/27.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQConfirmPayBottomView.h"

@interface YQConfirmPayBottomView ()
//左边的标题
@property (nonatomic, strong) UILabel *buyNumbersLab;
@property (nonatomic, strong) UILabel *courierFeesLab;
@property (nonatomic, strong) UILabel *baoyouLab;
@property (nonatomic, strong) UILabel *tradingPwdLab;

@property (nonatomic, strong) UIView *headView;

@end

@implementation YQConfirmPayBottomView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = WhiteColor;
        [self setupUI];
    }
    return self;
}

#pragma mark - layout
- (void)setupUI {
    //添加顺序 从左往右 从上置下
    [self addSubview:self.headView];
    [self addSubview:self.mallIocn];
    [self addSubview:self.theTitleLab];
    [self addSubview:self.priceLab];
    [self addSubview:self.buyNumbersLab];
    [self addSubview:self.addAndReduceBtn];
    [self addSubview:self.courierFeesLab];
    [self addSubview:self.wholesLab];
    [self addSubview:self.WholesaleNumbersLab];
    [self addSubview:self.tradingPwdLab];
    [self addSubview:self.tradingTF];
    [self addSubview:self.baoyouLab];
    [self addSubview:self.selectBtn];
    
    //layout
    
    [self.headView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(10)));
    }];
    
    [self.mallIocn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(kMargin_top);
        make.left.mas_equalTo(kMargin_left);
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(100)));
    }];
    
    //应根据返回的text求高度
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mallIocn.mas_right).mas_equalTo(kSpace);
        make.top.equalTo(self.headView.mas_bottom).mas_equalTo(kMargin_top);
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(FIT(45));
    }];
    
    [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mallIocn.mas_right).mas_equalTo(kSpace);
        make.bottom.lessThanOrEqualTo(self.mallIocn.mas_bottom);
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(FIT(20));
    }];
    
    [self.buyNumbersLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mallIocn.mas_bottom).mas_equalTo(kMargin_top);
        make.left.mas_equalTo(kMargin_left);
    }];
    
    [self.addAndReduceBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.mallIocn.mas_bottom).mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(FIT(100), FIT(25)));
    }];
    
    [self.courierFeesLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.buyNumbersLab.mas_bottom).mas_equalTo(kMargin_top);
        make.left.mas_equalTo(kMargin_left);
    }];
    
    [self.baoyouLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.lessThanOrEqualTo(self.courierFeesLab.mas_top);
        make.right.mas_equalTo(-kMargin_right);
    }];

    [self.wholesLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.courierFeesLab.mas_bottom).mas_equalTo(kMargin_top);
        make.left.mas_equalTo(kMargin_left);
    }];

    [self.WholesaleNumbersLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.lessThanOrEqualTo(self.wholesLab.mas_top);
        make.right.mas_equalTo(-kMargin_right);
    }];

    [self.tradingPwdLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.wholesLab.mas_bottom).mas_equalTo(kMargin_top);
        make.left.mas_equalTo(kMargin_left);
    }];

    [self.tradingTF mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.lessThanOrEqualTo(self.tradingPwdLab.mas_top);
        make.right.mas_equalTo(-kMargin_right);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth/2.0, FIT(20)));
    }];
    
    [self.selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.lessThanOrEqualTo(self.wholesLab.mas_top);
        make.size.mas_equalTo(CGSizeMake(FIT(23), FIT(23)));
    }];
}

//ui

- (UIView *)headView {
    if (!_headView) {
        _headView = [[UIView alloc] init];
        _headView.backgroundColor = MainBackgroundColor;
    }
    return _headView;
}

- (UIImageView *)mallIocn {
    if (!_mallIocn) {
        _mallIocn = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"back_goods")];
    }
    return _mallIocn;
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kCellDescribeLabelFont userBold:NO];
        _theTitleLab.numberOfLines = 2;
        _theTitleLab.text = @"大红袍臻品礼盒装 用批发券可以享受优惠";
    }
    return _theTitleLab;
}

- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(15) userBold:NO];
        _priceLab.text = @"￥1500";
    }
    return _priceLab;
}

- (UILabel *)buyNumbersLab {
    if (!_buyNumbersLab) {
        _buyNumbersLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _buyNumbersLab.text = @"购买数量";
    }
    return _buyNumbersLab;
}

- (UIButton *)addAndReduceBtn {
    if (!_addAndReduceBtn) {
        _addAndReduceBtn = [YQViewFactory buttonWithImage:nil target:self sel:@selector(AddAndReduceAction:)];
        [_addAndReduceBtn setTitle:@"1" forState:UIControlStateNormal];
        [_addAndReduceBtn setTitleColor:MainBlackColor forState:UIControlStateNormal];
        [_addAndReduceBtn setBackgroundImage:YQ_IMAGE(@"add_reduce") forState:UIControlStateNormal];
        
        _addAndReduceBtn.enabled = NO;
    }
    return _addAndReduceBtn;
}

//快递费
- (UILabel *)courierFeesLab {
    if (!_courierFeesLab) {
        _courierFeesLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _courierFeesLab.text = @"快递费";
    }
    return _courierFeesLab;
}

- (UILabel *)baoyouLab {
    if (!_baoyouLab) {
        _baoyouLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _baoyouLab.text = @"包邮";
    }
    return _baoyouLab;
}

- (UILabel *)wholesLab {
    if (!_wholesLab) {
        _wholesLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _wholesLab.text = @"使用批发券";
    }
    return _wholesLab;
}

- (UILabel *)WholesaleNumbersLab {
    if (!_WholesaleNumbersLab) {
        _WholesaleNumbersLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _WholesaleNumbersLab.text = @"1张";
    }
    return _WholesaleNumbersLab;
}

- (UILabel *)tradingPwdLab {
    if (!_tradingPwdLab) {
        _tradingPwdLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _tradingPwdLab.text = @"交易密码";
    }
    return _tradingPwdLab;
}

- (UITextField *)tradingTF {
    if (!_tradingTF) {
        _tradingTF = [YQViewFactory textFieldWithPlaceholderText:@"请输入交易密码" textColor:MainBlackColor fontSize:kDescribeFont userBold:NO];
        _tradingTF.borderStyle = UITextBorderStyleNone;
        _tradingTF.textAlignment = NSTextAlignmentRight;
        _tradingTF.secureTextEntry = YES;
    }
    return _tradingTF;
}

- (UIButton *)selectBtn {
    if (!_selectBtn) {
        //先判断GBK 是否满足抵扣 才判断是否选中
        _selectBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"") target:self sel:@selector(isUseGBK:)];
    }
    return _selectBtn;
}


#pragma mark - action
- (void)AddAndReduceAction:(UIButton *)sender {
    
}

- (void)isUseGBK:(UIButton *)button {
    //复选
    button.selected = !button.selected;
    if (self.SelectUseGBKBlok) {
        self.SelectUseGBKBlok();
    }
}

@end
