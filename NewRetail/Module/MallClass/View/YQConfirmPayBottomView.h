//
//  YQConfirmPayBottomView.h
//  NewRetail
//
//  Created by yuqin on 2019/6/27.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQConfirmPayBottomView : UIView

/**
 商品图
 */
@property (nonatomic, strong) UIImageView *mallIocn;

/**
 标题
 */
@property (nonatomic, strong) UILabel *theTitleLab;

/**
 价格
 */
@property (nonatomic, strong) UILabel *priceLab;

/**
  批发券的张数
 */
@property (nonatomic, strong) UILabel *WholesaleNumbersLab;

/**
 交易密码输入框
 */
@property (nonatomic, strong) UITextField *tradingTF;

/**
 右边增加减少按钮
 */
@property (nonatomic, strong) UIButton *addAndReduceBtn;
//右边显示y批发券的数量
@property (nonatomic, strong) UILabel *wholesLab;


//右边选中使用GBK
@property (nonatomic, strong) UIButton *selectBtn;
@property (nonatomic, copy) void (^SelectUseGBKBlok) (void);

@end

NS_ASSUME_NONNULL_END
