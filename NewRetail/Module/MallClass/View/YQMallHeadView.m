//
//  YQMallHeadView.m
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMallHeadView.h"

@implementation YQMallHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupHeadView];
    }
    return self;
}

- (void)setupHeadView {
    [self addSubview:self.leftBtn];
    [self addSubview:self.rightBtn];
    CGFloat button_W = ScreenWidth/2.0 - 10;
    [self.leftBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-FIT(15));
        make.size.mas_equalTo(CGSizeMake(button_W, FIT(20)));
    }];
    
    [self.rightBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(0);
        make.bottom.mas_equalTo(-FIT(15));
        make.size.mas_equalTo(CGSizeMake(button_W, FIT(20)));
    }];
    
    UIView *middleLineView = [[UIView alloc] init];
    middleLineView.backgroundColor = WhiteColor;
    [self addSubview:middleLineView];
    self.middleLineView = middleLineView;
    [middleLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftBtn.mas_right).mas_equalTo(9);
        make.bottom.mas_equalTo(-FIT(18));
        make.size.mas_equalTo(CGSizeMake(1, FIT(14)));
    }];
    
    
}

- (UIButton *)leftBtn {
    if (!_leftBtn) {
        _leftBtn = [YQViewFactory buttonWithTitle:@"市价区" titleColor:UIColorFromRGB(0xFFC9C9) fontSize:FIT(18) userBold:YES target:self sel:@selector(chooseAction:)];
        [_leftBtn setTitleColor:WhiteColor forState:UIControlStateSelected];
        _leftBtn.selected = YES;
        _leftBtn.tag = 20;
        _leftBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    }
    return _leftBtn;
}

- (UIButton *)rightBtn {
    if (!_rightBtn) {
        _rightBtn = [YQViewFactory buttonWithTitle:@"批发区" titleColor:UIColorFromRGB(0xFFC9C9) fontSize:FIT(18) userBold:YES target:self sel:@selector(chooseAction:)];
        _rightBtn.tag = 21;
        [_rightBtn setTitleColor:WhiteColor forState:UIControlStateSelected];
        _rightBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    }
    return _rightBtn;
}


- (void)chooseAction:(UIButton *)sender {
    if (sender!=self.leftBtn) {
        self.leftBtn.selected = NO;
        sender.selected = YES;
        self.leftBtn = sender;
    }else {
        self.leftBtn.selected = YES;
    }
    if (self.SelectBlock) {
        self.SelectBlock(sender.tag-19);
    }
}

@end
