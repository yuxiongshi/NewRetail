//
//  YQAddressListVC.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQAddressListVC : YQBaseVC

@property (nonatomic, copy) NSString *isMeComing;//是否是会员中心进入,如果是会员中心进入 点击cell是不返回的

@end

NS_ASSUME_NONNULL_END
