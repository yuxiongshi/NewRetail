//
//  YQMallDetailVC.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"
#import "YQMallModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface YQMallDetailVC : YQBaseVC

@property (nonatomic, copy) NSString *titleStr;
@property (nonatomic, strong) YQMallModel *model;

@end

NS_ASSUME_NONNULL_END
