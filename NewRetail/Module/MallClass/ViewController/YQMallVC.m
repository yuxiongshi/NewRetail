//
//  YQMallVC.m
//  NewRetail
//
//  Created by yuqin on 2019/6/21.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMallVC.h"
#import "YQMallCell.h"
#import "YQMallModel.h"
#import "YQMallDetailVC.h"
#import "YQMallHeadView.h"
#import <SHSegmentedControl/SHSegmentedControlHeader.h>

@interface YQMallVC ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) UITableView *mallTB;//市场价
@property (nonatomic, strong) NSMutableArray *marketArr;
@property (nonatomic, copy) NSString *typeStr;

@end

@implementation YQMallVC

- (NSMutableArray *)marketArr {
    if (!_marketArr) {
        _marketArr = [NSMutableArray array];
    }
    return _marketArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.mallTB.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        //数据刷新
//        self.typeStr = @"1";
        [self loadTableViewData:self.typeStr];
    }];
    [self.mallTB.mj_header beginRefreshing];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setupMallView];
    [self.btnLeft setHidden:YES];
    [self.iconAndTitleLeftBtn setHidden:YES];
    self.typeStr = @"1";
//    [self loadTableViewData:self.typeStr];
    
    //头部
    [self createHeadView];
}

- (void)createHeadView {
    YQMallHeadView *segHeadView = [[YQMallHeadView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, NavigationStatusBarHeight)];
    segHeadView.backgroundColor = [UIColor clearColor];
    [self.headView addSubview:segHeadView];
    WeakSelf(self);
    segHeadView.SelectBlock = ^(NSInteger index) {
        weakSelf.typeStr = StringFromLongInt(index);
        [weakSelf loadTableViewData:StringFromLongInt(index)];
        NSLog(@"weakSelf.typeStr = %@",weakSelf.typeStr);
    };

}

- (void)loadTableViewData:(NSString *)type {
    [self.marketArr removeAllObjects];
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/goods/list") appendParameters:@{@"type":type} successBlock:^(id responseObject) {
        NSArray *mallArr;
        NSDictionary *mallDict = responseObject[@"data"];
        if ([responseObject[@"code"] integerValue] == 0) {
            if ([type integerValue] == 1) {
                //市场价
                mallArr = mallDict[@"market"];
            }else {
                mallArr = mallDict[@"wholesale"];
            }
            for (NSDictionary *dict in mallArr) {
                YQMallModel *marketModel = [YQMallModel mj_objectWithKeyValues:dict];
                [self.marketArr addObject:marketModel];
            }
            [self.mallTB reloadData];
            [self.mallTB.mj_header endRefreshing];
        }
    } failureBlock:^(NSError *error) {
        [self.mallTB.mj_header endRefreshing];
    }];
}

- (void)setupMallView {

    self.mallTB = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStylePlain];
    self.mallTB.delegate=self;
    self.mallTB.dataSource=self;
    YQ_NoDataImg(self.mallTB);
    self.mallTB.backgroundColor = TableViewBackGroundColor;
    [self.mallTB  setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.mallTB registerClass:[YQMallCell class] forCellReuseIdentifier:MALL_Cell];
    [self.view addSubview:self.mallTB];
    self.mallTB.frame = CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, ScreenHeight-NavigationStatusBarHeight-Botoom_IPhoneX);
}

#pragma mark - delegate /DataSource
YQ_ImageForEmptyDataSet(@"暂无数据", NoDataView_Nor);

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.marketArr.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return YQMallCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQMallCell *cell = [tableView dequeueReusableCellWithIdentifier:MALL_Cell forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    YQMallModel *model = self.marketArr[indexPath.row];
    [cell loadDataWithModel:model];
    WeakSelf(self);
    NSDictionary *dict = @{@"1":@"市价区",
                           @"2":@"批发区"
                           };
    cell.MallDetailBlock = ^{
        YQMallDetailVC *detail = [[YQMallDetailVC alloc] init];
        detail.titleStr = [dict objectForKey:weakSelf.typeStr];
        detail.model = model;
        YQ_PUSH(detail);
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

@end
