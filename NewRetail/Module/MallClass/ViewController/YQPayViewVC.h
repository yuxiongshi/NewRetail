//
//  YQPayViewVC.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"
#import "YQMallModel.h"
#import "YQMallDetailModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQPayViewVC : YQBaseVC

//@property (nonatomic, strong) YQMallModel *mallModel;
@property (nonatomic, strong) YQMallDetailModel *detailModel;
@property (nonatomic, copy) NSString *isWhole;//是否是批发价

@end

NS_ASSUME_NONNULL_END
