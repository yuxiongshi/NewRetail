//
//  YQAddAndEditorVC.h
//  NewRetail
//
//  Created by yuqin on 2019/6/28.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQBaseVC.h"
#import "YQPayModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQAddAndEditorVC : YQBaseVC

@property (nonatomic, strong) YQPayModel *payModel;

@end

NS_ASSUME_NONNULL_END
