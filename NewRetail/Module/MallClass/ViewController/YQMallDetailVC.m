//
//  YQMallDetailVC.m
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMallDetailVC.h"
#import "YQPayViewVC.h"
#import "YQMallDetailModel.h"

@interface YQMallDetailVC ()

@property (nonatomic, strong) UILabel *theTitleLab;//标题
@property (nonatomic, strong) UIScrollView *backScrollView;
@property (nonatomic, strong) NSArray *imageList;
@property (nonatomic, strong) YQMallDetailModel *detailModel;

@end

@implementation YQMallDetailVC
{
    NSInteger _couponsNum;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //批发区2 市价区1
    [self.btnLeft setTitle:[NSString stringWithFormat:@"商品详情（%@）",self.titleStr] forState:UIControlStateNormal];
    [self setupDetailView];
    [self setupDetailData];
    if ([self.titleStr isEqualToString:@"批发区"]) {
        [self checkGBKDataWithType:@"2"];
    }else {
        [self checkGBKDataWithType:@"1"];
    }
}

- (void)checkGBKDataWithType:(NSString *)type {
    //检查是否有批发券
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/goods/checkGBK") appendParameters:@{@"type":type} successBlock:^(id responseObject) {
        if ([responseObject[@"code"] integerValue] == 0) {
            self->_couponsNum = [responseObject[@"data"][@"gbkNum"] integerValue];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupDetailView {
    [self.view addSubview:self.theTitleLab];
    [self.theTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(NavigationStatusBarHeight);
        make.height.mas_equalTo(kLabelHeight);
    }];
    
    //底部按钮
    NSDictionary *titleDict = @{@"1":[NSString stringWithFormat:@"今日库存剩余：%@份",self.model.stock],
                                @"2":@"立即购买"
                                };
    NSDictionary *colorDict = @{@"1":MainRedTextColor,
                                @"2":WhiteColor
                                };
    for (NSInteger i = 0; i<2; i++) {
        UIButton *bottomBtn = [YQViewFactory buttonWithTitle:[titleDict objectForKey:StringFromLongInt(i+1)] titleColor:[colorDict objectForKey:StringFromLongInt(i+1)] fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(buyAction:)];
        bottomBtn.tag = 100+i;
        [self.view addSubview:bottomBtn];
        if (i == 0) {
            bottomBtn.enabled = NO;
            [bottomBtn setBackgroundColor:WhiteColor];
            [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.mas_equalTo(0);
                make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(10));
                make.size.mas_equalTo(CGSizeMake(2*ScreenWidth/3.0, FIT(45)));
            }];
        }else {
            bottomBtn.enabled = YES;
            [bottomBtn setBackgroundColor:MainRedTextColor];
            [bottomBtn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.right.mas_equalTo(0);
                make.bottom.mas_equalTo(-Botoom_IPhoneX-FIT(10));
                make.size.mas_equalTo(CGSizeMake(ScreenWidth/3.0, FIT(45)));
            }];
        }
        
        
    }
}

- (void)buyAction:(UIButton *)sender {
    if ([self.titleStr isEqualToString:@"市价区"]) {
        //立即购买
        if ([self.detailModel.stock integerValue] <= 0) {
            [LCProgressHUD showFailure:@"剩余库存不足"];
            return;
        }
    }else {
        if (_couponsNum <= 0) {
            [LCProgressHUD showFailure:@"优惠券不足，无法购买"];
            return;
        }
    }
    
    YQPayViewVC *payVC = [[YQPayViewVC alloc] init];
    payVC.detailModel = self.detailModel;
    payVC.isWhole = [self.titleStr isEqualToString:@"市价区"]?@"1":@"2";
    YQ_PUSH(payVC);
}

- (void)setupDetailData {
    
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/goods/detail") appendParameters:@{@"goodId":self.model.id} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            self.detailModel = [YQMallDetailModel mj_objectWithKeyValues:base.data];
            self.imageList = base.data[@"imgList"];
            [self setupMiddleScrollViewWithArr:self.imageList];
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupMiddleScrollViewWithArr:(NSArray *)imgList {
//    CGFloat scrollView_H = ScreenHeight-NavigationStatusBarHeight-Botoom_IPhoneX-FIT(55)-kLabelHeight;
    self.backScrollView = [[UIScrollView alloc] initWithFrame:CGRectZero];
    self.backScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.backScrollView];
    
    CGFloat fHeightSum = 0;
    //这里设置scrollView的实际可以滚动的区域高度
    self.backScrollView.frame = CGRectMake(0, NavigationStatusBarHeight+kLabelHeight, ScreenWidth, ScreenHeight - FIT(60) - NavigationStatusBarHeight - kLabelHeight - Botoom_IPhoneX);

    for (NSInteger i = 0; i < imgList.count; i++) {
        //先获取网络图片的size
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:imgList[i]]];
        UIImage *showImage = [UIImage imageWithData:data];
        CGFloat scale;
        if (showImage.size.height > showImage.size.width) {
            scale = showImage.size.height/showImage.size.width;
        }else {
            scale = showImage.size.width/showImage.size.height;
        }
        
        
//        NSLog(@"ScreenWidth*scale = %2.f",ScreenWidth*scale);
        //获取到一张图之后设置imagview的宽高，然后加载到scrollview上面
        UIImageView *img = [[UIImageView alloc] initWithFrame:CGRectMake(0, fHeightSum, ScreenWidth, ScreenWidth*scale)];
        
        //当前的
        fHeightSum += ScreenWidth*scale;
        //contentSize是scrollview的实际长度，为所有的图片的高度之后
        self.backScrollView.contentSize = CGSizeMake(ScreenWidth, fHeightSum);


        
//        NSLog(@"ScreenWidth*scale2 = %2.f",ScreenWidth*scale);
        
        [img sd_setImageWithURL:[NSURL URLWithString:imgList[i]]];
        [self.backScrollView addSubview:img];
    }
    
    
}

- (UILabel *)theTitleLab {
    if (!_theTitleLab) {
        _theTitleLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentCenter fontSize:kDescribeFont userBold:NO];
        _theTitleLab.text = self.model.goodTitile;
    }
    return _theTitleLab;
}

@end
