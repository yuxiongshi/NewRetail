//
//  YQAddressDetailCell.m
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQAddressDetailCell.h"

@implementation YQAddressDetailCell

- (void)loadCellDataWithModel:(YQPayModel *)model {
    self.nameAndTelLab.text = [NSString stringWithFormat:@"%@ %@",model.name,model.phone];
    self.nameAndTelLab.attributedText = [AXAttributedString makeAttributedString:^(AXAttributedStringMaker *make) {
        make.text([NSString stringWithFormat:@"%@ ",model.name]).foregroundColor(MainBlackColor).font(YQ_Font(FIT(18))).underline(NSUnderlineStyleNone);
        make.text(model.phone).foregroundColor(UIColorFromRGB(0xafafaf)).font(YQ_Font(FIT(14)));
    }];
    self.addressLab.text = model.addressDetail;
    
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.defaultBtn setImage:YQ_IMAGE(@"round_select") forState:UIControlStateSelected];
    [self.defaultBtn setTitleColor:MainRedTextColor forState:UIControlStateSelected];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
#pragma mark - 勾选默认地址
- (IBAction)chooseDefaultAction:(UIButton *)sender {
    sender.selected = !sender.selected;
}
- (IBAction)editorAddressAction:(id)sender {
    if (self.EditorAddressBlock) {
        self.EditorAddressBlock();
    }
}
- (IBAction)deleteCurrentAddress:(id)sender {
    if (self.DeleteAddressBlock) {
        self.DeleteAddressBlock(self);
    }
}

@end
