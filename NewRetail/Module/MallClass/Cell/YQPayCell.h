//
//  YQPayCell.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_PayCell @"YQPayCell"
#import <UIKit/UIKit.h>
//#import "YQMallModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQPayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameAndTelLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
//- (void)loadCellDataWithModel:(YQMallModel *)
@end

NS_ASSUME_NONNULL_END
