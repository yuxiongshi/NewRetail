//
//  YQMallCell.m
//  NewRetail
//
//  Created by yuqin on 2019/6/21.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQMallCell.h"

@implementation YQMallCell

- (void)setupTableViewCellView {
    [self addSubview:self.iconImg];
    [self.iconImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(kMargin_top);
        make.size.mas_equalTo(CGSizeMake(FIT(120), FIT(120)));
    }];
    
    [self addSubview:self.describeLab];
    [self.describeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.iconImg.mas_right).mas_equalTo(FIT(12));
        make.top.mas_equalTo(FIT(20));
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(FIT(45));
    }];
    
    [self addSubview:self.inventoryLab];
    [self.inventoryLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.describeLab.mas_left);
        make.top.equalTo(self.describeLab.mas_bottom).mas_equalTo(FIT(7));
    }];
    
    [self addSubview:self.priceLab];
    [self addSubview:self.originalPriceLab];
    [self.originalPriceLab addSubview:self.lineView];
    
    
    [self addSubview:self.detailBtn];
    [self.detailBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(FIT(-12));
        make.bottom.mas_equalTo(FIT(-18));
        make.size.mas_equalTo(CGSizeMake(FIT(80), FIT(30)));
    }];
    
    //底部分割线
    UIView *bottomView = [[UIView alloc] init];
    bottomView.backgroundColor = TableViewBackGroundColor;
    [self addSubview:bottomView];
    [bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-0.5);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, 1));
    }];
    
}

#pragma mark - dy
- (UIImageView *)iconImg {
    if (!_iconImg) {
        _iconImg = [YQViewFactory imageViewWithImage:nil];
    }
    return _iconImg;
}

- (UILabel *)describeLab {
    if (!_describeLab) {
        _describeLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _describeLab.numberOfLines = 2;
        _describeLab.text = @"大红袍臻品礼盒装 用批发卷可以享受优惠";
    }
    return _describeLab;
}

- (UILabel *)inventoryLab {
    if (!_inventoryLab) {
        _inventoryLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _inventoryLab.text = @"今日库存剩余：1000";
    }
    return _inventoryLab;
}

- (UILabel *)priceLab {
    if (!_priceLab) {
        _priceLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:FIT(20) userBold:YES];
        _priceLab.text = @"￥1500";
    }
    return _priceLab;
}

- (UILabel *)originalPriceLab {
    if (!_originalPriceLab) {
        _originalPriceLab = [YQViewFactory labelWithTextColor:[UIColor lightGrayColor] textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _originalPriceLab.text = @"￥1500";
    }
    return _originalPriceLab;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = UIColorFromRGB(0x808080);
    }
    return _lineView;
}

- (UIButton *)detailBtn {
    if (!_detailBtn) {
        _detailBtn = [YQViewFactory buttonWithTitle:@"了解详情" titleColor:WhiteColor fontSize:FIT(12) userBold:NO target:self sel:@selector(forDetails)];
        [_detailBtn setBackgroundColor:MainRedTextColor];
        _detailBtn.layer.cornerRadius = 2;
        _detailBtn.layer.masksToBounds = YES;
    }
    return _detailBtn;
}
#pragma mark - 点击了解详情
- (void)forDetails {
    if (self.MallDetailBlock) {
        self.MallDetailBlock();
    }
}

- (void)loadDataWithModel:(YQMallModel *)mallModel {
    
    //根据原价显示价格的宽度
    CGFloat price_H = [UILabel getWidthWithText:mallModel.price height:FIT(15) font:FIT(20)];
    
    [self.priceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.describeLab.mas_left);
        make.bottom.equalTo(self.iconImg.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(price_H+10, FIT(15)));
    }];
    
    CGFloat originalPrice_h = [UILabel getWidthWithText:mallModel.originalPrice height:FIT(12) font:FIT(20)];
    
    [self.originalPriceLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.priceLab.mas_right).mas_equalTo(5);
        make.bottom.equalTo(self.iconImg.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(originalPrice_h+10, FIT(15)));
    }];
    
    NSDictionary *attrDic = @{
                              NSStrikethroughStyleAttributeName: @(1),
                              NSFontAttributeName : YQ_Font(12),
                              NSBaselineOffsetAttributeName : @(NSUnderlineStyleSingle)
                              };
    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:mallModel.originalPrice attributes:attrDic];
    self.originalPriceLab.attributedText = attrStr;
    
    [self.iconImg sd_setImageWithURL:[NSURL URLWithString:mallModel.goodImgUrl] placeholderImage:PlaceHolder];
    
    self.describeLab.text = mallModel.goodTitile;
    self.inventoryLab.text = [NSString stringWithFormat:@"今日库存剩余：%@",mallModel.stock];
    self.priceLab.text = mallModel.price;
    self.originalPriceLab.text = mallModel.price;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
