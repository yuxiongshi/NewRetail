//
//  YQAddAndEditorCell.m
//  NewRetail
//
//  Created by yuqin on 2019/6/28.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQAddAndEditorCell.h"
@interface YQAddAndEditorCell ()<UITextFieldDelegate>



@end

@implementation YQAddAndEditorCell

- (void)setupTableViewCellView {
    [self addSubview:self.leftTextField];
    [self addSubview:self.selectBtn];
    
    [self.leftTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(20)));
    }];
    
    [self.selectBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(20), FIT(20)));
    }];
    
}

- (UITextField *)leftTextField {
    if (!_leftTextField) {
        _leftTextField = [YQViewFactory textFieldWithPlaceholderText:@"" textColor:MainBlackColor fontSize:kTextFieldFont userBold:NO];
        _leftTextField.borderStyle = UITextBorderStyleNone;
        _leftTextField.delegate = self;
//        [LimitTextFieldNum limitTextFieldContentNumWith:_leftTextField withMaxNum:6];
        
    }
    return _leftTextField;
}

- (UIButton *)selectBtn {
    if (!_selectBtn) {
        _selectBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"round_normal") target:self sel:@selector(setDefaultAddress:)];
        [_selectBtn setImage:YQ_IMAGE(@"round_select") forState:UIControlStateSelected];
        [_selectBtn setHidden:YES];
    }
    return _selectBtn;
}

- (void)setDefaultAddress:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (self.SetDefaultAddressBlock) {
        self.SetDefaultAddressBlock(sender.selected);
    }
}

#pragma mark - 代理
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.TextFieldEndEditorBlock) {
        self.TextFieldEndEditorBlock(textField.text);
    }
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
