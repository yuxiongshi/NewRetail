//
//  YQAddAndEditorCell.h
//  NewRetail
//
//  Created by yuqin on 2019/6/28.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_AddAndEditorCell @"YQAddAndEditorCell"
#import "YQBaseTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQAddAndEditorCell : YQBaseTableViewCell

@property (nonatomic, strong) UITextField *leftTextField;
@property (nonatomic, strong) UIButton *selectBtn;
@property (nonatomic, copy) void (^SetDefaultAddressBlock) (BOOL isSelect);
@property (nonatomic, copy) void (^TextFieldEndEditorBlock) (NSString *text);
@end

NS_ASSUME_NONNULL_END
