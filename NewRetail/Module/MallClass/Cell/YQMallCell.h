//
//  YQMallCell.h
//  NewRetail
//
//  Created by yuqin on 2019/6/21.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define MALL_Cell @"YQMallCell"
#define YQMallCellHeight FIT(150)

#import "YQBaseTableViewCell.h"
#import "YQMallModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQMallCell : YQBaseTableViewCell

/**
 icon
 */
@property (nonatomic, strong) UIImageView *iconImg;

/**
 商品简介
 */
@property (nonatomic, strong) UILabel *describeLab;

/**
 库存
 */
@property (nonatomic, strong) UILabel *inventoryLab;

/**
 现价
 */
@property (nonatomic, strong) UILabel *priceLab;

/**
 原价
 */
@property (nonatomic, strong) UILabel *originalPriceLab;

/**
 灰色的划线
 */
@property (nonatomic, strong) UIView *lineView;

/**
 详情按钮
 */
@property (nonatomic, strong) UIButton *detailBtn;
@property (nonatomic, copy) void (^MallDetailBlock) (void);
- (void)loadDataWithModel:(YQMallModel *)mallModel;

@end

NS_ASSUME_NONNULL_END
