//
//  YQAddressDetailCell.h
//  NewRetail
//
//  Created by yuqin on 2019/6/26.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_AddressDetailCell @"YQAddressDetailCell"
#import <UIKit/UIKit.h>
#import "YQPayModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQAddressDetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameAndTelLab;
@property (weak, nonatomic) IBOutlet UILabel *addressLab;
@property (weak, nonatomic) IBOutlet UIButton *defaultBtn;
@property (weak, nonatomic) IBOutlet UIButton *editorBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;

//@property (nonatomic, copy) void (^ChooseDefaultAddressBlock) (void);
@property (nonatomic, copy) void (^EditorAddressBlock) (void);
@property (nonatomic, copy) void (^DeleteAddressBlock) (UITableViewCell *currentCell);

- (void)loadCellDataWithModel:(YQPayModel *)model;

@end

NS_ASSUME_NONNULL_END
