//
//  YQSeeExpressModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/2.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQSeeExpressModel : NSObject

/**
 快递单号
 */
@property (nonatomic, copy) NSString *logisticCode;

/**
 物流公司
 */
@property (nonatomic, copy) NSString *logisticsCompany;

/**
 客服电话
 */
@property (nonatomic, copy) NSString *serviceTelephone;

/**
 物流状态
 */
@property (nonatomic, copy) NSString *state;

/**
 时间
 */
@property (nonatomic, copy) NSString *AcceptTime;

/**
 描述
 */
@property (nonatomic, copy) NSString *AcceptStation;

/**
 备注
 */
@property (nonatomic, copy) NSString *Remark;

@end

NS_ASSUME_NONNULL_END
