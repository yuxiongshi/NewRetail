//
//  YQOrderModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/1.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQOrderModel : BaseModel

/**
 orderId
 */
@property (nonatomic, copy) NSString *orderId;

/**
 左边的图片
 */
@property (nonatomic, copy) NSString *imgUrl;

/**
 标题
 */
@property (nonatomic, copy) NSString *title;

/**
 couponNum
 */
@property (nonatomic, copy) NSString *couponNum;

/**
 payPrice
 */
@property (nonatomic, copy) NSString *payPrice;
/**
 payGBKNum
 */
@property (nonatomic, copy) NSString *payGBKNum;
/**
 数量
 */
@property (nonatomic, copy) NSString *quantity;
/**
 订单状态
 */
@property (nonatomic, copy) NSString *orderStatus;
/**
 创建时间
 */
@property (nonatomic, copy) NSString *createTime;
/**
 订单编号
 */
@property (nonatomic, copy) NSString *orderNum;

/**
 物流编号
 */
@property (nonatomic, copy) NSString *logisticsNum;

/**
 物流公司
 */
@property (nonatomic, copy) NSString *logisticsCompany;

@end

NS_ASSUME_NONNULL_END
