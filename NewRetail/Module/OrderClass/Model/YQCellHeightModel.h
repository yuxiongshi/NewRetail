//
//  YQCellHeightModel.h
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQCellHeightModel : NSObject

@property (nonatomic, copy) NSString *AcceptStation;
@property (nonatomic, assign) float cellHeight;

@end

NS_ASSUME_NONNULL_END
