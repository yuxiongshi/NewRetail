//
//  YQOrderDealCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/1.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQOrderDealCell.h"

@interface YQOrderDealCell ()

@property (nonatomic, strong) UIView *backView;

@end

@implementation YQOrderDealCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = WhiteColor;
        [self setupTableViewCell];
    }
    return self;
}

- (void)setupTableViewCell {
    [self addSubview:self.orderNumLab];
    [self addSubview:self.backView];
    [self.backView addSubview:self.leftImgView];
    [self.backView addSubview:self.describeTextLab];
    [self.backView addSubview:self.itemsLab];
    [self.backView addSubview:self.statusBtn];
    [self addSubview:self.totalMoneyLab];
    [self addSubview:self.lineView];
    [self addSubview:self.tradingBtn];
    [self addSubview:self.pickUpGoodsBtn];
    [self addSubview:self.cancelEntrustBtn];//委托中
    
    [self.orderNumLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(34)));
    }];
    
    [self.backView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.orderNumLab.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, FIT(138)));
    }];
    
    [self.leftImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(10));
        make.top.mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(FIT(115), FIT(115)));
    }];
    
    [self.describeTextLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.lessThanOrEqualTo(self.leftImgView.mas_top);
        make.left.mas_equalTo(self.leftImgView.mas_right).mas_equalTo(FIT(10));
        make.right.mas_equalTo(-kMargin_right);
        make.height.mas_equalTo(FIT(40));
    }];
    
    [self.itemsLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(16));
    }];
    
    [self.statusBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.leftImgView.mas_right).mas_equalTo(FIT(10));
        make.bottom.mas_equalTo(-FIT(16));
        make.size.mas_equalTo(CGSizeMake(FIT(130), FIT(20)));
    }];
    
    
    [self.totalMoneyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.backView.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_right, FIT(40)));
    }];
    
    [self.lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.equalTo(self.totalMoneyLab.mas_bottom).mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, 1));
    }];
    
    [self.tradingBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.lineView.mas_bottom).mas_equalTo(FIT(15));
        make.size.mas_equalTo(CGSizeMake(FIT(96), FIT(32)));
    }];
    
    [self.pickUpGoodsBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.tradingBtn.mas_left).mas_equalTo(-FIT(5));
        make.top.lessThanOrEqualTo(self.tradingBtn.mas_top);
        make.size.mas_equalTo(CGSizeMake(FIT(96), FIT(32)));
    }];

    [self.cancelEntrustBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(-kMargin_right);
        make.top.equalTo(self.lineView.mas_bottom).mas_equalTo(FIT(15));
        make.size.mas_equalTo(CGSizeMake(FIT(128), FIT(32)));
    }];
}

- (UILabel *)orderNumLab {
    if (!_orderNumLab) {
        _orderNumLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _orderNumLab.text = @"订单编号 2223443233";
    }
    return _orderNumLab;
}

- (UIView *)backView {
    if (!_backView) {
        _backView = [[UIView alloc] init];
        _backView.backgroundColor = TableViewBackGroundColor;
    }
    return _backView;
}

- (UIImageView *)leftImgView {
    if (!_leftImgView) {
        _leftImgView = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"")];
    }
    return _leftImgView;
}

- (UILabel *)describeTextLab {
    if (!_describeTextLab) {
        _describeTextLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _describeTextLab.numberOfLines = 2;
        _describeTextLab.text = @"大红袍臻品礼盒装 用批发券可以享受优惠";
    }
    return _describeTextLab;
}

- (UILabel *)itemsLab {
    if (!_itemsLab) {
        _itemsLab = [YQViewFactory labelWithTextColor:MainBlackColor textAlignment:NSTextAlignmentRight fontSize:kDescribeFont userBold:NO];
        _itemsLab.text = @"x 1";
    }
    return _itemsLab;
}

- (UILabel *)totalMoneyLab {
    if (!_totalMoneyLab) {
        _totalMoneyLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x262626) textAlignment:NSTextAlignmentRight fontSize:FIT(12) userBold:NO];
        _totalMoneyLab.text = @"共1件商品 合计：￥1500.00";
        
    }
    return _totalMoneyLab;
}

- (UIView *)lineView {
    if (!_lineView) {
        _lineView = [[UIView alloc] init];
        _lineView.backgroundColor = LineColor;
    }
    return _lineView;
}

- (UIButton *)tradingBtn {
    if (!_tradingBtn) {
        _tradingBtn = [YQViewFactory buttonWithTitle:@"委托交易" titleColor:MainRedTextColor fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(tradingAction)];
        [_tradingBtn setBackgroundImage:YQ_IMAGE(@"trading") forState:UIControlStateNormal];
        [_tradingBtn setHidden:YES];
    }
    return _tradingBtn;
}

- (UIButton *)pickUpGoodsBtn {
    if (!_pickUpGoodsBtn) {
        _pickUpGoodsBtn = [YQViewFactory buttonWithTitle:@"我要提货" titleColor:UIColorFromRGB(0xb6b6b6) fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(pickGoodsAction)];
        _pickUpGoodsBtn.layer.cornerRadius = FIT(32)/2.0;
        _pickUpGoodsBtn.layer.borderWidth = 1;
        _pickUpGoodsBtn.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
        [_pickUpGoodsBtn setHidden:YES];
    }
    return _pickUpGoodsBtn;
}

- (UIButton *)statusBtn {
    if (!_statusBtn) {
        _statusBtn = [YQViewFactory buttonWithTitle:@"正在委托交易" image:YQ_IMAGE(@"trading_icon") titleColor:MainRedTextColor fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(notSelectAction)];
        _statusBtn.enabled = NO;
    }
    return _statusBtn;
}

- (UIButton *)cancelEntrustBtn {
    if (!_cancelEntrustBtn) {
        _cancelEntrustBtn = [YQViewFactory buttonWithTitle:@"取消委托并提货" titleColor:UIColorFromRGB(0xb6b6b6) fontSize:kNormalButtonFont userBold:NO target:self sel:@selector(cancelEntrustBtnAction)];
        _cancelEntrustBtn.layer.cornerRadius = FIT(32)/2.0;
        _cancelEntrustBtn.layer.borderWidth = 1;
        _cancelEntrustBtn.layer.borderColor = UIColorFromRGB(0xcccccc).CGColor;
        [_cancelEntrustBtn setHidden:YES];
    }
    return _cancelEntrustBtn;
}


//委托交易
- (void)tradingAction {
    if (self.EntrustTradingBlock) {
        self.EntrustTradingBlock();
    }
}

//提货
- (void)pickGoodsAction {
    if (self.PickUpBlock) {
        self.PickUpBlock();
    }
}

//取消委托并提货
- (void)cancelEntrustBtnAction {
    if (self.CancelEntrustBlock) {
        self.CancelEntrustBlock();
    }
}

- (void)notSelectAction {
    //不会点击
}

//赋值
- (void)loadCellWithOrderModel:(YQOrderModel *)orderModel {
    [self.leftImgView sd_setImageWithURL:[NSURL URLWithString:orderModel.imgUrl] placeholderImage:PlaceHolder];
    self.describeTextLab.text = orderModel.title;
    self.itemsLab.text = [NSString stringWithFormat:@"x %@",orderModel.quantity];
    self.orderNumLab.text = [NSString stringWithFormat:@"订单编号：%@",orderModel.orderNum];
    self.totalMoneyLab.text = [NSString stringWithFormat:@"共1件商品 合计：￥%@",orderModel.payPrice];
    [_totalMoneyLab setAttributedTextColorWithBeforeString:@"工1件商品 合计：￥" beforeColor:UIColorFromRGB(0x262626) afterString:orderModel.payPrice afterColor:MainRedTextColor];
}

@end
