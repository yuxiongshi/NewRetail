//
//  YQLogisticsCell.m
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQLogisticsCell.h"

@implementation YQLogisticsCell
{
    CGFloat _labelHeight;
}
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupLogisticsView];
    }
    return self;
}



- (void)setupLogisticsView {
    [self addSubview:self.verticalLineView];
    [self addSubview:self.downLineView];
    [self addSubview:self.roundImg];
    [self addSubview:self.loginsticsStatusLab];
    [self addSubview:self.timeLab];
    
    
    
    
    [self.roundImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(25));
        make.centerY.mas_equalTo(self.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(FIT(16), FIT(16)));
    }];
    
    [self.verticalLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(32));
        make.top.mas_equalTo(0);
        make.bottom.equalTo(self.roundImg.mas_top).mas_equalTo(0);
        make.width.mas_equalTo(1);
    }];
    
    [self.downLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(FIT(32));
        make.top.equalTo(self.roundImg.mas_bottom).mas_equalTo(0);
        make.bottom.mas_equalTo(0);
        make.width.mas_equalTo(1);
    }];
    
}

#pragma mark - alloc
- (UIView *)verticalLineView {
    if (!_verticalLineView) {
        _verticalLineView = [[UIView alloc] init];
        _verticalLineView.backgroundColor = LineColor;
    }
    return _verticalLineView;
}

- (UIView *)downLineView {
    if (!_downLineView) {
        _downLineView = [[UIView alloc] init];
        _downLineView.backgroundColor = LineColor;
    }
    return _downLineView;
}

- (UIImageView *)roundImg {
    if (!_roundImg) {
        _roundImg = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"round_gray")];
    }
    return _roundImg;
}

- (UILabel *)loginsticsStatusLab {
    if (!_loginsticsStatusLab) {
        _loginsticsStatusLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x888888) textAlignment:NSTextAlignmentLeft fontSize:FIT(15) userBold:NO];
        _loginsticsStatusLab.numberOfLines = 0;
    }
    return _loginsticsStatusLab;
}

- (UILabel *)timeLab {
    if (!_timeLab) {
        _timeLab = [YQViewFactory labelWithTextColor:UIColorFromRGB(0x888888) textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
    }
    return _timeLab;
}

#pragma mark - 赋值
- (void)loadCellWithModel:(YQSeeExpressModel *)model {
    self.loginsticsStatusLab.text = model.AcceptStation;
    self.timeLab.text = model.AcceptTime;
    
    //请label高度
//    _labelHeight = [UILabel getLabelHeightWithText:model.AcceptStation width:ScreenWidth-FIT(104) font:FIT(15)];
    //理应跟随数据的高度改变高度
    [self.loginsticsStatusLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.roundImg.mas_right).mas_equalTo(FIT(15));
        make.top.mas_equalTo(FIT(15));
        make.right.mas_equalTo(-kMargin_right);
        make.bottom.mas_equalTo(-FIT(35));
//        make.height.mas_equalTo(_labelHeight+FIT(20));
    }];
    
    [self.timeLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.lessThanOrEqualTo(self.loginsticsStatusLab.mas_left);
        make.top.equalTo(self.loginsticsStatusLab.mas_bottom).mas_equalTo(FIT(10));
    }];
    
    
}

//- (CGFloat)cellHeight {
//    return FIT(70)+_labelHeight;
//}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
