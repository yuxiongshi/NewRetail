//
//  YQOrderDealCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/1.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_OrderDealCell @"YQOrderDealCell"
#import <UIKit/UIKit.h>
#import "YQOrderModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQOrderDealCell : UITableViewCell

/**
 订单编号
 */
@property (nonatomic, strong) UILabel *orderNumLab;

/**
 图片介绍
 */
@property (nonatomic, strong) UIImageView *leftImgView;

/**
 文字介绍
 */
@property (nonatomic, strong) UILabel *describeTextLab;

/**
 物品数量
 */
@property (nonatomic, strong) UILabel *itemsLab;

/**
 总计
 */
@property (nonatomic, strong) UILabel *totalMoneyLab;

/**
 我要提货
 */
@property (nonatomic, strong) UIButton *pickUpGoodsBtn;

@property (nonatomic, strong) UIView *lineView;


/**
 委托交易
 */
@property (nonatomic, strong) UIButton *tradingBtn;

/**
 处理状态
 */
@property (nonatomic, strong) UIButton *statusBtn;

/**
 取消委托并取货（委托中）
 */
@property (nonatomic, strong) UIButton *cancelEntrustBtn;

//委托交易
@property (nonatomic, copy) void (^EntrustTradingBlock) (void);
//取消委托并提货
@property (nonatomic, copy) void (^CancelEntrustBlock) (void);
//我要提货
@property (nonatomic, copy) void (^PickUpBlock) (void);

- (void)loadCellWithOrderModel:(YQOrderModel *)orderModel;

@end

NS_ASSUME_NONNULL_END
