//
//  YQLogisticsCell.h
//  NewRetail
//
//  Created by yuqin on 2019/7/3.
//  Copyright © 2019 yuqin. All rights reserved.
//

#define YQ_LogisticsCell @"YQLogisticsCell"

#import <UIKit/UIKit.h>
#import "YQSeeExpressModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface YQLogisticsCell : UITableViewCell

/**
 圆圈
 */
@property (nonatomic, strong) UIImageView *roundImg;

/**
 物流状态
 */
@property (nonatomic, strong) UILabel *loginsticsStatusLab;

/**
 时间
 */
@property (nonatomic, strong) UILabel *timeLab;

/**
 竖线（上）
 */
@property (nonatomic, strong) UIView *verticalLineView;

/**
 竖线(下)
 */
@property (nonatomic, strong) UIView *downLineView;

- (void)loadCellWithModel:(YQSeeExpressModel *)model;

- (CGFloat)cellHeight;

@end

NS_ASSUME_NONNULL_END
