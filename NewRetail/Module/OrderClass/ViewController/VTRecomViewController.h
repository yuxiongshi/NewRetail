//
//  RecomViewController.h
//  VTMagicView
//
//  Created by tianzhuo on 14-11-13.
//  Copyright (c) 2014年 tianzhuo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VTRecomViewController : UIViewController

/**
 订单状态
 */
@property (nonatomic, copy) NSString *orderStatus;

/**
 市价订单/批发订单
 */
@property (nonatomic, copy) NSString *orderType;//默认为市价订单1
//@property (nonatomic, assign) NSInteger index;
//- (void)setupDataWithOrderStatus:(NSString *)orderStatus orderType:(NSString *)orderTye;

@end
