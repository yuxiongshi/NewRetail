//
//  YQOrderVC.m
//  NewRetail
//
//  Created by yuqin on 2019/7/2.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQOrderVC.h"
#import "YQMallHeadView.h"
#import "VTRecomViewController.h"

@interface YQOrderVC ()

@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) YQMallHeadView *orderHeadView;
@property (nonatomic, strong) NSString *chooseSelect;//判断点击了头部的那个按钮
@property (nonatomic, strong) MLMSegmentHead *segHead;
@property (nonatomic, strong) MLMSegmentScroll *segScroll;

@end

@implementation YQOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.chooseSelect = @"1";
//    self.titleArr = @[@"待发货",@"待收货",@"已收货"];
    [self.btnLeft setHidden:YES];
    [self.iconAndTitleLeftBtn setHidden:YES];
    [self setupSegmentView:@[@"待发货",@"待收货",@"已收货"]];
    
}

- (void)setupSegmentView:(NSArray *)titleArr {
    
    [self.headView addSubview:self.orderHeadView];
    [self.orderHeadView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(0);
        make.left.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, NavigationStatusBarHeight));
    }];
    
    _segHead = [[MLMSegmentHead alloc] initWithFrame:CGRectMake(0, NavigationStatusBarHeight, ScreenWidth, 44) titles:titleArr headStyle:SegmentHeadStyleLine layoutStyle:MLMSegmentLayoutDefault];
    _segHead.lineColor = kTheTitleBackgroundColor;
    _segHead.lineColor = MainRedTextColor;
    _segHead.lineHeight = 1.0;
    _segHead.selectColor = MainRedTextColor;
    _segHead.fontScale = 1.1;

    
    
    
    _segScroll = [[MLMSegmentScroll alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_segHead.frame), SCREEN_WIDTH, SCREEN_HEIGHT-CGRectGetMaxY(_segHead.frame)) vcOrViews:[self vcArr:titleArr.count]];
    _segScroll.loadAll = NO;
    WeakSelf(self);
    [MLMSegmentManager associateHead:_segHead withScroll:_segScroll completion:^{
        [self.view addSubview:weakSelf.segHead];
        [self.view addSubview:weakSelf.segScroll];
    }];
    
}

- (YQMallHeadView *)orderHeadView {
    if (!_orderHeadView) {
        _orderHeadView = [[YQMallHeadView alloc] init];
        _orderHeadView.backgroundColor = [UIColor clearColor];
        [_orderHeadView.leftBtn setTitle:@"市价订单" forState:UIControlStateNormal];
        [_orderHeadView.rightBtn setTitle:@"批发订单" forState:UIControlStateNormal];
        WeakSelf(self);
        _orderHeadView.SelectBlock = ^(NSInteger index) {
            weakSelf.chooseSelect = StringFromLongInt(index);
            if (index == 1) {
                weakSelf.titleArr = @[@"待发货",@"待收货",@"已收货"];
                
            }else {
                weakSelf.titleArr = @[@"待处理",@"委托中",@"交易成功",@"待发货",@"待收货",@"已收货"];
            }
            [weakSelf setupSegmentView:weakSelf.titleArr];
            DLog(@"chooseSelect = %@",weakSelf.chooseSelect);
//            [weakSelf.magicController.magicView reloadData];
        };
    }
    return _orderHeadView;
}

#pragma mark - 数据源
- (NSArray *)vcArr:(NSInteger)count {
    NSMutableArray *arr = [NSMutableArray array];
    for (NSInteger i = 0; i < count; i ++) {
        VTRecomViewController *vc = [VTRecomViewController new];
        if ([self.chooseSelect integerValue] == 1) {
            vc.orderStatus = StringFromLongInt(i+4);
        }else {
            vc.orderStatus = StringFromLongInt(i+1);
        }        
        vc.orderType = self.chooseSelect;
        [arr addObject:vc];
    }
    return arr;
}



@end
