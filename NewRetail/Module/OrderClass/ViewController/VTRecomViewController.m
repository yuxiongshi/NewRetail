//
//  RecomViewController.m
//  VTMagicView
//
//  Created by tianzhuo on 14-11-13.
//  Copyright (c) 2014年 tianzhuo. All rights reserved.
//

#import "VTRecomViewController.h"
#import "YQOrderDealCell.h"
#import "YQOrderModel.h"
#import "YQCheckLogisticsView.h"

@interface VTRecomViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) NSMutableArray *newsList;
@property (nonatomic, strong) UITableView *orderTableView;
@property (nonatomic, strong) YQCheckLogisticsView *checkView;
@property (nonatomic, strong) UIView *popView;

@end

@implementation VTRecomViewController
{
    NSInteger _pageStart;
    NSInteger _pageSize;
    NSString *_orderId;//查看物流
}

- (NSMutableArray *)newsList {
    if (!_newsList) {
        _newsList = [NSMutableArray array];
    }
    return _newsList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.automaticallyAdjustsScrollViewInsets = YES;
    _pageStart = 1;
    _pageSize = 10;
    
    //popView
    
    _popView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _popView.backgroundColor = [UIColor clearColor];
    
    [self setUpView];
    //下拉刷新
    self.orderTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [self setupDataWithOrderStatus:self.orderStatus orderType:self.orderType];
    }];
    //马上进入刷新状态
    [self.orderTableView.mj_header beginRefreshing];
    
}

- (void)setupDataWithOrderStatus:(NSString *)orderStatus orderType:(NSString *)orderTye{
    [self.newsList removeAllObjects];
    NSDictionary *parameters = @{@"pageStart":StringFromLongInt(_pageStart),
                                 @"pageSize":StringFromLongInt(_pageSize),
                                 @"orderType":self.orderType,
                                 @"orderStatus":self.orderStatus
                                 };
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/order/list") appendParameters:parameters successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            for (NSDictionary *dict in base.data[@"dataList"]) {
                YQOrderModel *model = [YQOrderModel mj_objectWithKeyValues:dict];
                [self.newsList addObject:model];
            }
            
            [self.orderTableView reloadData];
            [self.orderTableView.mj_header endRefreshing];
        }
    } failureBlock:^(NSError *error) {
        [self.orderTableView.mj_header endRefreshing];
    }];
}

- (void)setUpView {
    [self.view addSubview:self.orderTableView];
    [self.orderTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, TableView_Height_IPhoneX-FIT(100)));
    }];
}

- (UITableView *)orderTableView {
    if (!_orderTableView) {
        _orderTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _orderTableView.backgroundColor = TableViewBackGroundColor;
        _orderTableView.delegate = self;
        _orderTableView.dataSource = self;
        YQ_NoDataImg(_orderTableView);
        _orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [_orderTableView registerClass:[YQOrderDealCell class] forCellReuseIdentifier:YQ_OrderDealCell];
    }
    return _orderTableView;
}

YQ_ImageForEmptyDataSet(@"暂无订单", @"noDataImg_order");

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.newsList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger orderStatus = [self.orderStatus integerValue];
    if (orderStatus == 3 || orderStatus == 4 ) {
        return FIT(210);
    }
    return FIT(270);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(10);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //区分当前是什么订单 以及区分是什么状态
    
    YQOrderDealCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_OrderDealCell forIndexPath:indexPath];
    YQOrderModel *model = self.newsList[indexPath.section];
    [cell loadCellWithOrderModel:model];
    NSInteger orderStatus = [self.orderStatus integerValue];
    WeakSelf(self);
    if ([self.orderType integerValue] == 1) {
        //市价订单
        if (orderStatus == 4) {
            //交易成功
            [cell.statusBtn setTitle:@"正在安排发货" forState:UIControlStateNormal];
            [cell.statusBtn setImage:nil forState:UIControlStateNormal];
            cell.statusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cell.lineView setHidden:YES];
        }else if (orderStatus == 5) {
            [cell.statusBtn setHidden:YES];
            [cell.tradingBtn setHidden:NO];
            [cell.tradingBtn setTitle:@"查看物流" forState:UIControlStateNormal];
            _orderId = model.orderId;
            [cell.tradingBtn addTarget:self action:@selector(checkLogistics) forControlEvents:UIControlEventTouchUpInside];
        }else if(orderStatus == 6){
            [cell.statusBtn setHidden:YES];
            [cell.tradingBtn setHidden:NO];
            [cell.tradingBtn setTitle:@"查看物流" forState:UIControlStateNormal];
            _orderId = model.orderId;
            [cell.tradingBtn addTarget:self action:@selector(checkLogistics) forControlEvents:UIControlEventTouchUpInside];
        }
    }else {
        //批发订单
        if (orderStatus == 1) {
            //待处理
            [cell.statusBtn setHidden:YES];
            [cell.tradingBtn setHidden:NO];
            [cell.pickUpGoodsBtn setHidden:NO];
            NSDictionary *changedDic = @{@"orderId":model.orderId,
                                         @"orderStatus":@"2"
                                         };
            cell.EntrustTradingBlock = ^{
                [self actionClassWithDict:changedDic showMessage:@"委托成功" message:@"确定要委托平台交易吗?"];
            };
            
            NSDictionary *pickUpDict = @{@"orderId":model.orderId,
                                         @"orderStatus":@"4"
                                         };
            cell.PickUpBlock = ^{
                [self actionClassWithDict:pickUpDict showMessage:@"提货成功" message:@"确定要提货吗?"];
            };
            
        }else if (orderStatus == 2) {
            //委托中
            [cell.cancelEntrustBtn setHidden:NO];
            NSDictionary *cancelDic = @{@"orderId":model.orderId,
                                         @"orderStatus":@"4"
                                         };
            cell.CancelEntrustBlock = ^{
                [self actionClassWithDict:cancelDic showMessage:@"提货成功" message:@"确定要提货吗？"];
            };
        }else if (orderStatus == 3) {
            //交易成功
            [cell.statusBtn setTitle:@"交易成功" forState:UIControlStateNormal];
            [cell.statusBtn setImage:nil forState:UIControlStateNormal];
            cell.statusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cell.lineView setHidden:YES];
        }else if (orderStatus == 4) {
            //待发货
            //交易成功
            [cell.statusBtn setTitle:@"正在安排发货" forState:UIControlStateNormal];
            [cell.statusBtn setImage:nil forState:UIControlStateNormal];
            cell.statusBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
            [cell.lineView setHidden:YES];
        }else if (orderStatus == 5) {
            [cell.statusBtn setHidden:YES];
            [cell.tradingBtn setHidden:NO];
            [cell.tradingBtn setTitle:@"查看物流" forState:UIControlStateNormal];
            _orderId = model.orderId;
            [cell.tradingBtn addTarget:self action:@selector(checkLogistics) forControlEvents:UIControlEventTouchUpInside];
        }else {
//            [cell.statusBtn setHidden:YES];
//            [cell.lineView setHidden:YES];
            [cell.statusBtn setHidden:YES];
            [cell.tradingBtn setHidden:NO];
            [cell.tradingBtn setTitle:@"查看物流" forState:UIControlStateNormal];
            _orderId = model.orderId;
            [cell.tradingBtn addTarget:self action:@selector(checkLogistics) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    return cell;
}


- (void)checkLogistics {
    
    _checkView = [[YQCheckLogisticsView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    _checkView.backgroundColor = [UIColor clearColor];
    WeakSelf(self);
    _checkView.PopBlock = ^{
        [[HWPopTool sharedInstance] closeWithBlcok:^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    };
    [_popView addSubview:_checkView];
    [_checkView loadLogisticsDataWithOrderId:_orderId];
    
    //查看物流
    [HWPopTool sharedInstance].shadeBackgroundType = ShadeBackgroundTypeSolid;
    [HWPopTool sharedInstance].closeButtonType = ButtonPositionTypeRight;
    [[HWPopTool sharedInstance] showWithPresentView:_popView animated:YES];
}

- (void)closeAndBack {
    [[HWPopTool sharedInstance] closeWithBlcok:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
}


//事件集合
- (void)actionClassWithDict:(NSDictionary *)changedDic showMessage:(NSString *)showMessage message:(NSString *)message {
    [JXTAlertView showAlertViewWithTitle:@"温馨提示" message:message cancelButtonTitle:@"取消" otherButtonTitle:@"确定" cancelButtonBlock:^(NSInteger buttonIndex) {
        NSLog(@"cancel");
    } otherButtonBlock:^(NSInteger buttonIndex) {
        [SZHTTPSReqManager postRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/order/operation") appendParameters:nil bodyParameters:changedDic successBlock:^(id responseObject) {
            BaseModel *base = [BaseModel modelWithJson:responseObject];
            if (base.data == 0) {
                [self.newsList removeAllObjects];
                [LCProgressHUD showSuccess:showMessage];
                [self setupDataWithOrderStatus:self.orderStatus orderType:self.orderType];
            }else {
                [LCProgressHUD showFailure:base.msg];
            }
        } failureBlock:^(NSError *error) {
            
        }];
    }];
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
