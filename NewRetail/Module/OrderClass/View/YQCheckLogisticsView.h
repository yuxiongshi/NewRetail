//
//  YQCheckLogisticsView.h
//  NewRetail
//
//  Created by yuqin on 2019/7/2.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQCheckLogisticsView : UIView

/**
 是否签收
 */
@property (nonatomic, strong) UILabel *stateLab;

/**
 物流公司+快递单
 */
@property (nonatomic, strong) UILabel *logisticsCompanyLab;

/**
  客服电话
 */
@property (nonatomic, strong) UILabel *serverTele;
@property (nonatomic, strong) UITableView *showTableView;
@property (nonatomic, strong) UIButton *popBtn;
@property (nonatomic, copy) void (^PopBlock) (void);

- (void)loadLogisticsDataWithOrderId:(NSString *)orderId;

- (void)showView;
- (void)hideView;

@end

NS_ASSUME_NONNULL_END
