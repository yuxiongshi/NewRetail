//
//  YQCheckLogisticsView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/2.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQCheckLogisticsView.h"
#import "YQLogisticsCell.h"
#import "YQSeeExpressModel.h"
#import "YQCellHeightModel.h"

@interface YQCheckLogisticsView ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UIImageView *headImgView;
/**
 头像
 */
@property (nonatomic, strong) UIImageView *headIcon;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) NSMutableArray *cellHeightArr;

@end

@implementation YQCheckLogisticsView
{
//    NSString *_logisticCode;//快递单号
//    NSString *_logisticsCompany;//物流公司
//    NSString *_serviceTelephone;//客服电话
//    NSString *_state;//物流状态
    CGFloat _labelHeight;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setupShowView];
//        [self loadLogisticsData];
    }
    return self;
}

//获取物流信息
- (void)loadLogisticsDataWithOrderId:(NSString *)orderId {
    
    [SZHTTPSReqManager getRequestWithUrlString:YQ_joiningTogetherUrl(@"trade/order/seeExpress") appendParameters:@{@"orderId":orderId} successBlock:^(id responseObject) {
        BaseModel *base = [BaseModel modelWithJson:responseObject];
        if (base.code == 0) {
            //快递信息
            self.logisticsCompanyLab.text = [NSString stringWithFormat:@"%@：%@",base.data[@"logisticsCompany"],base.data[@"logisticCode"]];
            NSDictionary *dict = @{@"2":@"在途中",
                                   @"3":@"签收",
                                   @"4":@"问题件"
                                   };
            self.stateLab.text = [dict objectForKey:base.data[@"state"]];
            self.serverTele.text = [NSString stringWithFormat:@"客服电话：%@",base.data[@"serviceTelephone"]];
            
            for (NSDictionary *dict in base.data[@"trace"]) {
                YQSeeExpressModel *model = [YQSeeExpressModel mj_objectWithKeyValues:dict];
                [self.dataArr addObject:model];
                YQCellHeightModel *heightModel = [[YQCellHeightModel alloc] init];
                heightModel.AcceptStation = dict[@"AcceptStation"];
                heightModel.cellHeight = [UILabel getLabelHeightWithText:model.AcceptStation width:ScreenWidth-FIT(104) font:FIT(15)];
                [self.cellHeightArr addObject:heightModel];
            }
            
            [self.showTableView reloadData];
            NSLog(@"self.cellHeightArr = %@",self.cellHeightArr);
        }
    } failureBlock:^(NSError *error) {
        
    }];
}

- (void)setupShowView {
    [self addSubview:self.headImgView];
    [self.headImgView addSubview:self.headIcon];
    [self.headImgView addSubview:self.stateLab];
    [self.headImgView addSubview:self.logisticsCompanyLab];
    [self.headImgView addSubview:self.serverTele];
    [self addSubview:self.showTableView];
    [self addSubview:self.popBtn];
    
    [self.headImgView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(48));
        make.left.mas_equalTo(kMargin_left);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(110)));
    }];
    
    [self.headIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(FIT(20));
        make.left.mas_equalTo(FIT(20));
        make.size.mas_equalTo(CGSizeMake(FIT(68), FIT(68)));
    }];
    
    [self.stateLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.headIcon.mas_right).mas_equalTo(FIT(15));
        make.top.lessThanOrEqualTo(self.headIcon.mas_top);
    }];
    
    [self.logisticsCompanyLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.lessThanOrEqualTo(self.stateLab.mas_left);
        make.top.equalTo(self.stateLab.mas_bottom).mas_equalTo(FIT(12));
    }];
    
    [self.serverTele mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.lessThanOrEqualTo(self.stateLab.mas_left);
        make.top.equalTo(self.logisticsCompanyLab.mas_bottom).mas_equalTo(FIT(5));
    }];
    
    [self.showTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.equalTo(self.headImgView.mas_bottom).mas_equalTo(FIT(10));
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(360)));
    }];
    
    [self.popBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.showTableView.mas_bottom).mas_equalTo(kMargin_top);
        make.centerX.mas_equalTo(self.mas_centerX);
//        make.left.mas_equalTo(ScreenWidth/2.0-FIT(19));
        make.size.mas_equalTo(CGSizeMake(FIT(38), FIT(38)));
    }];
    
}

- (NSMutableArray *)dataArr {
    if (!_dataArr) {
        _dataArr = [NSMutableArray array];
    }
    return _dataArr;
}

- (NSMutableArray *)cellHeightArr {
    if (!_cellHeightArr) {
        _cellHeightArr = [NSMutableArray array];
    }
    return _cellHeightArr;
}

- (UIImageView *)headImgView {
    if (!_headImgView) {
        _headImgView = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"rect_sma")];
        _headImgView.layer.cornerRadius = 5;
        _headImgView.layer.masksToBounds = YES;
    }
    return _headImgView;
}

- (UIImageView *)headIcon {
    if (!_headIcon) {
        _headIcon = [YQViewFactory imageViewWithImage:YQ_IMAGE(@"courier")];
    }
    return _headIcon;
}

- (UILabel *)stateLab {
    if (!_stateLab) {
        _stateLab = [YQViewFactory labelWithTextColor:MainRedTextColor textAlignment:NSTextAlignmentLeft fontSize:kDescribeFont userBold:NO];
        _stateLab.text = @"已签收";
    }
    return _stateLab;
}

- (UILabel *)logisticsCompanyLab {
    if (!_logisticsCompanyLab) {
        _logisticsCompanyLab = [YQViewFactory labelWithTextColor:[UIColor grayColor] textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _logisticsCompanyLab.text = @"包子快递：12332435";
    }
    return _logisticsCompanyLab;
}

- (UILabel *)serverTele {
    if (!_serverTele) {
        _serverTele = [YQViewFactory labelWithTextColor:[UIColor grayColor] textAlignment:NSTextAlignmentLeft fontSize:FIT(12) userBold:NO];
        _serverTele.text = @"客服电话：12345687";
    }
    return _serverTele;
}

- (UITableView *)showTableView {
    if (!_showTableView) {
        _showTableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _showTableView.backgroundColor = WhiteColor;
        _showTableView.delegate = self;
        _showTableView.dataSource = self;
        [_showTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [_showTableView registerClass:[YQLogisticsCell class] forCellReuseIdentifier:YQ_LogisticsCell];
        _showTableView.layer.cornerRadius = 5;
        _showTableView.layer.masksToBounds = YES;
    }
    return _showTableView;
}

- (UIButton *)popBtn {
    if (!_popBtn) {
        _popBtn = [YQViewFactory buttonWithImage:YQ_IMAGE(@"close") target:self sel:@selector(closeAndBack)];
    }
    return _popBtn;
}

- (void)closeAndBack {
    if (self.PopBlock) {
        self.PopBlock();
    }
}

#pragma mark - delegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    return _labelHeight;
    YQCellHeightModel *model = self.cellHeightArr[indexPath.row];
    NSLog(@"model.cellHeight = %2.f",model.cellHeight);
    return model.cellHeight+FIT(70);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return FIT(44);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YQLogisticsCell *cell = [tableView dequeueReusableCellWithIdentifier:YQ_LogisticsCell forIndexPath:indexPath];
    YQSeeExpressModel *model = self.dataArr[indexPath.row];
    [cell loadCellWithModel:model];
    if (indexPath.row == 0) {
        [cell.verticalLineView setHidden:YES];
        cell.roundImg.image = YQ_IMAGE(@"round_red");
        cell.loginsticsStatusLab.textColor = MainRedTextColor;
    }
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *backView = [[UIView alloc] init];
    backView.backgroundColor = WhiteColor;
    UILabel *topLab = [YQViewFactory labelWithTextColor:kTheTitleBackgroundColor textAlignment:NSTextAlignmentLeft fontSize:FIT(15) userBold:NO];
    topLab.text = @"物流跟踪";
    [backView addSubview:topLab];
    [topLab mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(kMargin_left);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth-2*kMargin_left, FIT(44)));
    }];
    UIView *lineView = [[UIView alloc] init];
    lineView.backgroundColor = UIColorFromRGB(0xf5f5f5);
    [backView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(0);
        make.bottom.mas_equalTo(-2);
        make.size.mas_equalTo(CGSizeMake(ScreenWidth, 1));
    }];
    return backView;
}

- (void)showView {
    self.hidden = NO;
}

- (void)hideView {
    self.hidden = YES;
}

@end
