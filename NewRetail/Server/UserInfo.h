//
//  UserInfo.h
//  99SVR
//
//  Created by xia zhonglin  on 12/9/15.
//  Copyright © 2015 xia zhonglin . All rights reserved.
//

#import "BaseModel.h"

@interface UserInfo : BaseModel
DEFINE_SINGLETON_FOR_HEADER(UserInfo)

@property (nonatomic,copy) NSString *gbkBalance;
@property (nonatomic,copy) NSString *phoneNum;
@property (nonatomic,copy) NSString *referralCode;
@property (nonatomic,copy) NSString *gbkFreezeBalance;
@property (nonatomic,copy) NSString *gbkCnyRate;
@property (nonatomic,copy) NSString *avatar;
@property (nonatomic,copy) NSString *createTime;
@property (nonatomic,copy) NSString *loginId;//登录名
@property (nonatomic,copy) NSString *isTradePassword;//是否设置交易密码
@property (nonatomic,copy) NSString *authStatus;//实名认证状态（0：未认证，1：认证中，2：认证失败，3，认证通过）

//登录返回
@property (nonatomic,copy) NSString * sessionId;
@property (nonatomic,copy) NSString * userId;
@property (nonatomic,copy) NSString *loginName;
@property (nonatomic,copy) NSString *nickName;

@property (nonatomic,copy) NSString * token;//h5登录token

@property (nonatomic,copy) NSString *registerTime;
@property (nonatomic,copy) NSString *freeBalance;
/**
 *  是否登录
 */
@property (nonatomic,assign) BOOL bIsLogin;

- (void)updateUserInfo:(UserInfo *)info;
- (void)updateUserSecurityInfo:(NSDictionary *)dic;
- (NSDictionary *)getHeaderDic;
+ (BOOL)isLogin;

@end

