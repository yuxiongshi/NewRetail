//
//   SZHttpsService.h
//   GBKTrade
//
//   Created by LionIT on 22/03/2018.
//   Copyright © 2018 LionIT. All rights reserved.
//

#import "BaseService.h"

@interface SZHttpsService : BaseService

DEFINE_SINGLETON_FOR_HEADER(SZHttpsService)

#pragma mark-  验证码（登录后）
-(RACSignal*)signalRequestMsgCodeTokenWithParameters:(NSDictionary*)parameters;

#pragma mark-  短信验证码（登录前)
-(RACSignal*)signalRequestMsgCodeWithParameters:(NSDictionary*)parameters;

#pragma mark - 修改登录密码
-(RACSignal*)signalModifyPasswordWithParameters:(NSDictionary*)parameters;

#pragma mark - 退出登录
-(RACSignal*)signalLoginOut;


#pragma mark - 通过手机修改登录密码
-(RACSignal*)signalSetLoginPwdWithParameters:(NSDictionary*)parameters;

#pragma mark - 修改密码
-(RACSignal*)signalModifyPasswordWithNewPwd:(NSString*)newPwd  originPwd:(NSString*)originPwd phoneCode:(NSString*)phoneCode pwdType:(NSString*)pwdType reNewPwd:(NSString*)reNewPwd totpCode:(NSString*)totpCode;

#pragma mark - 获取用户基本信息
-(RACSignal*)signalRequestGetSecurityInfoParameter:(id)parameters;

#pragma mark - 注册
- (void)registerAccount:(NSDictionary *)parDict success:(successResponseBlock)successBlock fail:(failResponseBlock)failBlock;

#pragma mark- 获取游戏详情
-(RACSignal*)signalRequestGameInfoWithParameter:(id)parameters;

#pragma mark- 获取游戏结果
-(RACSignal*)signalRequestGameResultWithParameter:(id)parameters;

#pragma mark- 获取用户投注进出场列表
-(RACSignal*)signalRequestBetInfoListWithParameter:(id)parameters;

#pragma mark- 下注
-(RACSignal*)signalRequestGameBetWithParameter:(id)parameters;

#pragma mark - 登录
- (void)loginApp:(NSDictionary *)parDict success:(successResponseBlock)successBlock fail:(failResponseBlock)failBlock ;

#pragma mark-  用户相互转账
-(RACSignal*)signalRequestTransferFundsWithParameters:(NSDictionary*)parameters;


#pragma mark-  查询往期记录
-(RACSignal*)signalRequestPastRecordWithParameters:(NSDictionary*)parameters;

#pragma mark-  查询往期明细
-(RACSignal*)signalRequestPastRecordDetailWithParameters:(NSDictionary*)parameters;

#pragma mark-  查询资产转账记录
-(RACSignal*)signalRequestFundsRecordWithParameters:(NSDictionary*)parameters;

#pragma mark- 修改昵称
-(RACSignal*)signalRequestModifyNickNameWithParameters:(NSDictionary*)parameters;

#pragma mark- 版本更新
-(RACSignal*)signalRequestAppUpdateWithParameters:(NSDictionary*)parameters;

#pragma mark- 查看游戏公告
-(RACSignal*)signalRequestNoticeWithParameters:(NSDictionary*)parameters;

#pragma mark- 查看游戏说明
-(RACSignal*)signalRequestGameIntroduceWithParameters:(NSDictionary*)parameters;

#pragma mark - 关于我们
-(RACSignal*)signalRequestAboutUsWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取汇率
-(RACSignal*)signalRequestGBKRateWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取充币地址
-(RACSignal*)signalRequestDepositAddressWithParameters:(NSDictionary*)parameters;

#pragma mark - 保存充币信息
-(RACSignal*)signalRequestSaveDepositWithParameters:(NSDictionary*)parameters;

#pragma mark - 标记已充值
-(RACSignal*)signalRequestMarkPaymentWithParameters:(NSDictionary*)parameters;

#pragma mark - 充币取消订单
-(RACSignal*)signalRequestMarkCancelWithParameters:(NSDictionary*)parameters;

#pragma mark - 充币列表
-(RACSignal*)signalRequestDepositListWithParameters:(NSDictionary*)parameters;

#pragma mark - 保存提币信息
-(RACSignal*)signalRequestSaveWithdrawWithParameters:(NSDictionary*)parameters;

#pragma mark - 标记提币信息取消
-(RACSignal*)signalRequestWithdrawMarkCancelWithParameters:(NSDictionary*)parameters;

#pragma mark - 提币列表
-(RACSignal*)signalRequestwithdrawListWithParameters:(NSDictionary*)parameters;


#pragma mark-  我的分销
-(RACSignal*)signalRequestMyRecommendWithParameters:(NSDictionary*)parameters;

#pragma mark-  是否开启模拟场
-(RACSignal*)signalRequestIsOpenSimulationAWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取买卖币支持付款方式
-(RACSignal*)signalRequestPayTypeWithParameters:(NSDictionary*)parameters;

#pragma mark - 支付宝买卖币
-(RACSignal*)signalRequestTradeByAlipayWithParameters:(NSDictionary*)parameters;

#pragma mark - 银行卡买卖币
-(RACSignal*)signalRequestTradeByBankWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取买卖币列表
-(RACSignal*)signalRequestBuySellListWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取买币详情
-(RACSignal*)signalRequestBuyDetailWithParameters:(NSDictionary*)parameters;

#pragma mark - 获取卖币详情
-(RACSignal*)signalRequestSellDetailWithParameters:(NSDictionary*)parameters;

#pragma mark - 标记已付款 待付款取消订单 确认中取消订单
-(RACSignal*)signalRequestTradeOrderOperationWithParameters:(NSDictionary*)parameters;

#pragma mark -支持的银行卡类型列表
-(RACSignal*)signalRequestBankListWithParameters:(NSDictionary*)parameters;

#pragma mark-  兑币额度记录
-(RACSignal*)signalRequestFundsLimitRecordWithParameters:(NSDictionary*)parameters;

#pragma mark - 支付设置
- (RACSignal *)signalRequestpaySettingWithParamters:(NSDictionary *)parameters;




#pragma mark - 提币短信验证码
-(RACSignal*)signalRequestWithdrawSecurityCodeWithPhone:(NSString*)phone areaCode:(NSString*)areaCode securityCodeType:(NSString*)securityCodeType;
#pragma mark - 邮箱验证码
-(RACSignal*)signalRequestEmailCodeWithEmail:(NSString*)email securityCodeType:(NSString*)securityCodeType;
#pragma mark - 修改密码
-(RACSignal*)signalModifyPasswordWithNewPwd:(NSString*)newPwd  originPwd:(NSString*)originPwd phoneCode:(NSString*)phoneCode pwdType:(NSString*)pwdType reNewPwd:(NSString*)reNewPwd totpCode:(NSString*)totpCode;
#pragma mark - 退出登录
-(RACSignal*)signalLoginOut;
#pragma mark - 绑定手机
-(RACSignal*)signalBindPhoneWithAreaCode:(NSString*)areaCode phone:(NSString*)phone newCode:(NSString*)newCode ;
#pragma mark - 验证忘记密码
-(RACSignal*)signalcheckSecurityCodeWithPhone:(NSString*)phone  msgCode:(NSString*)msgCode;
#pragma mark - 验证忘记密码，邮箱
-(RACSignal*)signalcheckSecurityCodeWithEmail:(NSString*)Email  msgCode:(NSString*)msgCode;
#pragma mark - 通过手机修改登录密码
-(RACSignal*)signalSetLoginPwdWithPhone:(NSString*)phone  newPassword:(NSString*)newPassword msgCode:(NSString*)msgCode;
#pragma mark - 获取用户基本信息
-(RACSignal*)signalRequestGetSecurityInfoParameter:(id)parameters;
#pragma mark-上传身份认证图片
-(RACSignal*)signalRequestUploadIDPhotosWithParameter:(id)parameters;
#pragma mark- 提交身份认证信息
-(RACSignal*)signalRequestCommitIDInfoWithParameter:(id)parameters;

#pragma mark获取锁仓 币币 C2C账户总金额
-(RACSignal*)signalRequestWalletTotalParameter:(id)parameters;
#pragma mark-  获取C2C资产列表
-(RACSignal*)signalRequestGetC2CPropertyListParameter:(id)parameters;
#pragma mark-  获取C2C资产账户明细列表
-(RACSignal*)signalRequestGetC2CPropertyDetailParameter:(id)parameters;
#pragma mark获取锁仓币中列表
-(RACSignal*)signalRequestGetScPropertyListParameter:(id)parameters;
#pragma mark获取锁仓账户明细列表
-(RACSignal*)signalRequestGetScPropertyDetailParameter:(id)parameters;
#pragma mark - 获取财产列表
-(RACSignal*)signalRequestPropertyListWithParameter:(id)parameters;
#pragma mark - 获取财务记录明细
-(RACSignal*)signalRequestGetPropertyRecordsParameter:(id)parameters;
#pragma mark-  币币与C2C资金相互划转
-(RACSignal*)signalRequestC2CBBTransfersParameter:(id)parameters;
#pragma mark-  获取用户币种余额
-(RACSignal*)signalRequestBBPropertyListWithParameter:(id)parameters;

#pragma mark - 获取虚拟币充值地址
-(RACSignal*)signalRequestPropertyAddressWithParameter:(id)parameters;
#pragma mark - 手动获取虚拟货币充值地址
-(RACSignal*)signalRequestManualPropertyAddressWithParameter:(id)parameters;
#pragma mark - 虚拟货币提现
-(RACSignal*)signalRequestPropertyDrawRtcWithParameter:(id)parameters;
#pragma mark - 虚拟货币提现提交
-(RACSignal*)signalRequestCommitWithdrawWithAddress:(NSString*)address withdrawAddr:(NSString*)withdrawAddr withdrawAmount:(NSString*)withdrawAmount tradePwd:(NSString*)tradePwd googleCode:(NSString*)googleCode phoneCode:(NSString*)phoneCode symbol:(NSString*)symbol;
#pragma mark - 获取币种及地址数目列表
-(RACSignal*)signalRequestGetCoinListWithParameter:(id)parameters;
#pragma mark - 添加提币地址
-(RACSignal*)signalRequestAddAddressWithAddress:(NSString*)withdrawAddr  googleCode:(NSString*)googleCode phoneCode:(NSString*)phoneCode symbol:(NSString*)symbol addressRemark:(NSString*)withdrawRemark;
#pragma mark - 删除提币地址
-(RACSignal*)signalRequestdeleteAddressWithAddressId:(NSString*)addressId;
#pragma mark - 获取提币地址列表
-(RACSignal*)signalRequestAddressDetailWithSymbol:(NSString*)symbol;


#pragma mark - 推广奖励
-(RACSignal*)signalRequestPromotionRecordsParameter:(id)parameters;
#pragma mark - 返佣记录
-(RACSignal*)signalRequestCommissionRecordsParameter:(id)parameters;
#pragma mark - 我的推荐
-(RACSignal*)signalRequestMineRecommendRecordsParameter:(id)parameters;


#pragma mark - H5登录
-(RACSignal*)signalRequestC2CH5LoginParameter:(id)parameters;

#pragma mark - 广告
- (NSMutableArray *)getWithRequestParameter:(id)parameters withNumber:(NSString *)num withPage:(NSString *)page;

@end
