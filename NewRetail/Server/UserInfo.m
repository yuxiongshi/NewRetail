//
//  UserInfo.m
//  99SVR
//
//  Created by xia zhonglin  on 12/9/15.
//  Copyright © 2015 xia zhonglin . All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
DEFINE_SINGLETON_FOR_CLASS(UserInfo)

- (void)updateUserInfo:(UserInfo *)info {
    self.userId = info.userId;
    self.sessionId = info.sessionId;
    self.loginName= info.loginName;
    //应该存储本地
}
-(NSString *)loginName{
//    return [_appLoginName stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    return _loginName;
}

-(NSString *)phoneNum{
    
    if ([_phoneNum isMobliePhone]) {
       return [_phoneNum stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    return _phoneNum;
    
}

- (void)updateUserSecurityInfo:(NSDictionary *)dic {
    [self mj_setKeyValues:dic];
}
- (NSDictionary *)getHeaderDic {
    NSDictionary * dic = [NSDictionary dictionaryWithObjectsAndKeys:self.userId,@"userId",self.sessionId,@"sessionId", nil];
    return dic;
}

+ (BOOL)isLogin {
    return [UserInfo sharedUserInfo].bIsLogin;
}

@end

