//
//  UILabel+Settings.h
//  GBKTrade
//
//  Created by sumrain on 2018/6/22.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface HBTextAttributeModel : NSObject

@property (nonatomic,strong) UIFont* font;
@property (nonatomic,strong) UIColor* foregroundColor;
@property (nonatomic,strong) UIColor* backgroundColor;

@property (nonatomic,copy)   NSString* text;
@property (nonatomic,strong) NSNumber* italicsDegrees;

@end

@interface UILabel (Settings)

-(void)setLabelParagraphLineSpace:(CGFloat) space;
-(void)setLabelParagraphLineSpace:(CGFloat) space  textAlignment:(NSTextAlignment) textAlignment;
-(CGFloat)getLabelParagraphStyleHeightWithWidth:(CGFloat)width;

-(void)setAttributedTextWithBeforeString:(NSString*)beforeString beforeColor:(UIColor*) beforeColor beforeFont:(UIFont*) beforeFont afterString:(NSString*)afterString afterColor:(UIColor*) afterColor afterFont:(UIFont*) afterFont;

-(void)setAttributedTextColorWithBeforeString:(NSString*)beforeString beforeColor:(UIColor*) beforeColor afterString:(NSString*)afterString afterColor:(UIColor*) afterColor;

-(void)setAttributedTextFontWithBeforeString:(NSString*)beforeString beforeFont:(UIFont*) beforeFont afterString:(NSString*)afterString afterFont:(UIFont*) afterFont;

-(void)setAttributeWithsAttributeModelsArr:(NSArray*) array;
@end
