//
//  UILabel+YQLoadLabelHeightAndWidth.h
//  NewRetail
//
//  Created by yuqin on 2019/6/30.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UILabel (YQLoadLabelHeightAndWidth)

+ (CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font: (CGFloat)font;

+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(CGFloat)font;

@end

NS_ASSUME_NONNULL_END
