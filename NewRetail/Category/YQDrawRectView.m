//
//  YQDrawRectView.m
//  NewRetail
//
//  Created by yuqin on 2019/7/12.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "YQDrawRectView.h"

@implementation YQDrawRectView

- (void)drawRect:(CGRect)rect {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:rect byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(8,8)];//圆角大小
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc] init];
    maskLayer.frame = rect;
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}


@end
