//
//  UIViewController+MBProgressHUD.m
//  scale
//
//  Created by 何志行 on 16/12/18.
//  Copyright © 2016年 gretta. All rights reserved.
//

#import "UIViewController+MBProgressHUD.h"
@implementation UIViewController (MBProgressHUD)


-(void)hideMBProgressHUD{
   [MBProgressHUD hideHUDForView:self.view animated:YES];
    
}
+(void)hideMBProgressHUD{
    [MBProgressHUD hideHUDForView:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];
    
}

-(void)showLoadingMBProgressHUD{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view ];
    hud.userInteractionEnabled = YES;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.numberOfLines=0;
    //修改样式，否则等待框背景色将为半透明
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    //设置菊花框为白色
    [[UIActivityIndicatorView appearanceWhenContainedInInstancesOfClasses:@[[MBProgressHUD class]]] setColor:[UIColor whiteColor]];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
}
+(void)showLoadingMBProgressHUD{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:[UIApplication sharedApplication].delegate.window.rootViewController.view];
    hud.userInteractionEnabled = YES;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.numberOfLines=0;
    //修改样式，否则等待框背景色将为半透明
//    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    //设置菊花框为白色
    [[UIActivityIndicatorView appearanceWhenContainedInInstancesOfClasses:@[[MBProgressHUD class]]] setColor:[UIColor whiteColor]];
    [[UIApplication sharedApplication].delegate.window.rootViewController.view  addSubview:hud];
    [hud showAnimated:YES];
}

-(void)showPromptHUDWithTitle:(NSString *)title{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.text = title;
    hud.label.numberOfLines=0;
    // Move to bottm center.
//    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);

    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}
+(void)showPromptHUDWithTitle:(NSString *)title{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.numberOfLines=0;
    hud.label.text = title;
    // Move to bottm center.
//    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}


-(void)showErrorHUDWithTitle:(NSString *)title{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.text = title;
    hud.label.numberOfLines=0;
    // Move to bottm center.
//    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];

}
+(void)showErrorHUDWithTitle:(NSString *)title{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[UIApplication sharedApplication].delegate.window.rootViewController.view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.text = title;
    hud.label.numberOfLines=0;
    // Move to bottm center.
    //    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);

    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}



-(void)showLoadingMBProgressHUDToView:(UIView*) view{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:view ];
    hud.userInteractionEnabled = NO;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.numberOfLines=0;
    //修改样式，否则等待框背景色将为半透明
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    //设置菊花框为白色
    [[UIActivityIndicatorView appearanceWhenContainedInInstancesOfClasses:@[[MBProgressHUD class]]] setColor:[UIColor whiteColor]];
    [self.view addSubview:hud];
    [hud showAnimated:YES];
    
}


-(void)showPromptHUDToView:(UIView*) view title:(NSString *)title {
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.text = title;
    hud.label.numberOfLines=0;
    
    // Move to bottm center.
    //    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
}



-(void)showErrorHUDToView:(UIView*) view title:(NSString *)title{
    
    [self hideMBProgressHUD];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
    // Set the text mode to show only text.
    hud.mode = MBProgressHUDModeText;
    hud.label.textColor=[UIColor whiteColor];
    hud.label.text = title;
    hud.label.numberOfLines=0;
    // Move to bottm center.
    //    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    //设置等待框背景色为黑色
    hud.bezelView.backgroundColor =UIColorFromRGBWithAlpha(0X000000, 0.8);
    hud.removeFromSuperViewOnHide = YES;
    [hud hideAnimated:YES afterDelay:1.5f];
    
}


@end
