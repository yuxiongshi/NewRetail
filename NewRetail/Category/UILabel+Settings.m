//
//  UILabel+Settings.m
//  GBKTrade
//
//  Created by sumrain on 2018/6/22.
//  Copyright © 2018年 LionIT. All rights reserved.
//

#import "UILabel+Settings.h"


@implementation HBTextAttributeModel



@end

@implementation UILabel (Settings)

-(void)setLabelParagraphLineSpace:(CGFloat) space {
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paraStyle.alignment = NSTextAlignmentCenter;
    paraStyle.lineSpacing =  space; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.5f
                          };
    
//    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:self.text attributes:dic];
    NSMutableAttributedString* attributeStr=[[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    [attributeStr addAttributes:dic range:NSMakeRange(0, self.text.length)];
    self.attributedText = attributeStr;
}

-(void)setLabelParagraphLineSpace:(CGFloat) space  textAlignment:(NSTextAlignment) textAlignment{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paraStyle.alignment = textAlignment;
    paraStyle.lineSpacing =  space; //设置行间距
    paraStyle.hyphenationFactor = 1.0;
    paraStyle.firstLineHeadIndent = 0.0;
    paraStyle.paragraphSpacingBefore = 0.0;
    paraStyle.headIndent = 0;
    paraStyle.tailIndent = 0;
    //设置字间距 NSKernAttributeName:@1.5f
    NSDictionary *dic = @{NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.5f
                          };
    
    //    NSAttributedString *attributeStr = [[NSAttributedString alloc] initWithString:self.text attributes:dic];
    NSMutableAttributedString* attributeStr=[[NSMutableAttributedString alloc]initWithAttributedString:self.attributedText];
    [attributeStr addAttributes:dic range:NSMakeRange(0, self.text.length)];
    self.attributedText = attributeStr;
}

-(CGFloat)getLabelParagraphStyleHeightWithWidth:(CGFloat)width {
    
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    
    paraStyle.lineBreakMode = NSLineBreakByWordWrapping;
    
    paraStyle.alignment = NSTextAlignmentLeft;
    
    paraStyle.lineSpacing = FIT(5);
    
    paraStyle.hyphenationFactor = 1.0;
    
    paraStyle.firstLineHeadIndent = 0.0;
    
    paraStyle.paragraphSpacingBefore = 0.0;
    
    paraStyle.headIndent = 0;
    
    paraStyle.tailIndent = 0;
    
    NSDictionary *dic = @{NSFontAttributeName:self.font, NSParagraphStyleAttributeName:paraStyle, NSKernAttributeName:@0.5f
                          };
    
    CGSize size = [self.text boundingRectWithSize:CGSizeMake(width, ScreenHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    
    return size.height;
    
}

-(void)setAttributedTextWithBeforeString:(NSString*)beforeString beforeColor:(UIColor*) beforeColor beforeFont:(UIFont*) beforeFont afterString:(NSString*)afterString afterColor:(UIColor*) afterColor afterFont:(UIFont*) afterFont
{
    
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:self.text];
    [content addAttribute:NSForegroundColorAttributeName value:beforeColor range:NSMakeRange(0, beforeString.length)];
    [content addAttribute:NSForegroundColorAttributeName value:afterColor range:NSMakeRange(beforeString.length, afterString.length)];
    [content addAttribute:NSFontAttributeName value:beforeFont range:NSMakeRange(0, beforeString.length)];
    [content addAttribute:NSFontAttributeName value:afterFont range:NSMakeRange(beforeString.length, afterString.length)];
    self.attributedText=content;
}

-(void)setAttributedTextColorWithBeforeString:(NSString*)beforeString beforeColor:(UIColor*) beforeColor afterString:(NSString*)afterString afterColor:(UIColor*) afterColor
{
    
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:self.text];
    [content addAttribute:NSForegroundColorAttributeName value:beforeColor range:NSMakeRange(0, beforeString.length)];
    [content addAttribute:NSForegroundColorAttributeName value:afterColor range:NSMakeRange(beforeString.length, afterString.length)];
    self.attributedText=content;
}

-(void)setAttributedTextFontWithBeforeString:(NSString*)beforeString beforeFont:(UIFont*) beforeFont afterString:(NSString*)afterString afterFont:(UIFont*) afterFont
{
    
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:self.text];
    [content addAttribute:NSFontAttributeName value:beforeFont range:NSMakeRange(0, beforeString.length)];
    [content addAttribute:NSFontAttributeName value:afterFont range:NSMakeRange(beforeString.length, afterString.length)];
    self.attributedText=content;
}

-(void)setAttributeWithsAttributeModelsArr:(NSArray*) array{
    
    NSMutableAttributedString *content = [[NSMutableAttributedString alloc] initWithString:self.text];
    
    for (HBTextAttributeModel* attrituteModel in array) {
        
      
        if (isEmptyObject(attrituteModel.text)) {
            continue;
        }
        NSArray* ranges=[self rangeOfSubString:attrituteModel.text inString:self.text];
        
        for (NSString* rangeStr in ranges) {
            NSRange range=NSRangeFromString(rangeStr);
            if (!isEmptyObject(attrituteModel.foregroundColor)) {
                [content addAttribute:NSForegroundColorAttributeName value:attrituteModel.foregroundColor range:range];
            } if (!isEmptyObject(attrituteModel.backgroundColor)) {
                [content addAttribute:NSBackgroundColorAttributeName value:attrituteModel.backgroundColor range:range];
            } if (!isEmptyObject(attrituteModel.font)) {
                [content addAttribute:NSFontAttributeName value:attrituteModel.font range:range];
            } if (!isEmptyObject(attrituteModel.italicsDegrees)) {
                [content addAttribute:NSObliquenessAttributeName value:attrituteModel.italicsDegrees range:range];
            }
            
            
           
        }
        
//        NSRange range = [self.text rangeOfString:attrituteModel.text];
//        if (range.location != NSNotFound) {
//            if (!isEmptyObject(attrituteModel.foregroundColor)) {
//                [content addAttribute:NSForegroundColorAttributeName value:attrituteModel.foregroundColor range:range];
//            } if (!isEmptyObject(attrituteModel.backgroundColor)) {
//                [content addAttribute:NSBackgroundColorAttributeName value:attrituteModel.backgroundColor range:range];
//            } if (!isEmptyObject(attrituteModel.font)) {
//                [content addAttribute:NSFontAttributeName value:attrituteModel.font range:range];
//            }
//        }else{
//            NSLog(@"Not Found");
//        }
    }
    self.attributedText=content;


}

- (NSArray *)rangeOfSubString:(NSString *)subStr inString:(NSString *)string {
    NSMutableArray *rangeArray = [NSMutableArray array];
    NSString *string1 = [string stringByAppendingString:subStr];
    NSString *temp;
    for (int i = 0; i < string.length; i ++) {
        
        temp = [string1 substringWithRange:NSMakeRange(i, subStr.length)];
        
        if ([temp isEqualToString:subStr]){
            NSRange range = {i,subStr.length};
            [rangeArray addObject:NSStringFromRange(range)];
        }
        
    }return rangeArray;
    
}


@end
