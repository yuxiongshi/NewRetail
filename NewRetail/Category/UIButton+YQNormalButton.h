//
//  UIButton+YQNormalButton.h
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIButton (YQNormalButton)

- (void)setBtnWithTitle:(NSString *)title withBackGroundColor:(NSString *)color;

//设置layer
- (void)setNormalBtnWithBorderColor:(NSString *)color withcornerRadius:(CGFloat)cornerRadius;

@end

NS_ASSUME_NONNULL_END
