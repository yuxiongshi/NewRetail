//
//  UILabel+YQLoadLabelHeightAndWidth.m
//  NewRetail
//
//  Created by yuqin on 2019/6/30.
//  Copyright © 2019 yuqin. All rights reserved.
//

#import "UILabel+YQLoadLabelHeightAndWidth.h"

@implementation UILabel (YQLoadLabelHeightAndWidth)


+ (CGFloat)getLabelHeightWithText:(NSString *)text width:(CGFloat)width font: (CGFloat)font {
    CGRect rect = [text boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil]; return rect.size.height;
    
}

+ (CGFloat)getWidthWithText:(NSString *)text height:(CGFloat)height font:(CGFloat)font{
    CGRect rect = [text boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:font]} context:nil]; return rect.size.width;
    
}


@end
