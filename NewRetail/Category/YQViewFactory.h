//
//  YQViewFactory.h
//  GBKTrade
//
//  Created by yuqin on 2019/5/29.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YQViewFactory : NSObject

@end

@interface YQViewFactory (UILabel)

+ (UILabel *)labelWithTextColor:(UIColor *)textColor
                  textAlignment:(NSTextAlignment)textAlignment
                       fontSize:(CGFloat)fontSize userBold:(BOOL)userBold;

@end

@interface YQViewFactory (UIButton)

+ (UIButton *)buttonWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
                     fontSize:(CGFloat)fontSize
                     userBold:(BOOL)userBold
                       target:(id _Nullable)target
                          sel:(SEL)sel;

+ (UIButton *)buttonWithImage:(UIImage *_Nullable)image
                       target:(id _Nullable)target
                          sel:(SEL)sel;

+ (UIButton *)buttonWithTitle:(NSString *_Nullable)title
                        image:(UIImage *_Nullable)image
                   titleColor:(UIColor *)titleColor
                     fontSize:(CGFloat)fontSize
                     userBold:(BOOL)userBold
                       target:(id _Nullable)target
                          sel:(SEL)sel;

@end

@interface YQViewFactory (UITextField)

+ (UITextField *)textFieldWithPlaceholderText:(NSString *_Nullable)placeholerText textColor:(UIColor *)textColor fontSize:(CGFloat)fontSize userBold:(BOOL)userBold;

@end

@interface YQViewFactory (UIImageView)

+ (UIImageView *)imageViewWithImage:(UIImage *_Nullable)image;
+ (UIImageView *)imageViewWithImage:(UIImage *_Nullable)image target:(id _Nullable)target sel:(SEL _Nullable)sel;

@end


NS_ASSUME_NONNULL_END
