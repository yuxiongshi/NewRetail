//
//  UIButton+YQNormalButton.m
//  GBKTrade
//
//  Created by admin on 2019/4/28.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "UIButton+YQNormalButton.h"

@implementation UIButton (YQNormalButton)

- (void)setBtnWithTitle:(NSString *)title withBackGroundColor:(NSString *)color {
    [self setTitle:title forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor colorWithHexString:color]];
    [self setTitleColor:WhiteColor forState:UIControlStateNormal];
}

- (void)setNormalBtnWithBorderColor:(NSString *)color withcornerRadius:(CGFloat)cornerRadius{
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor colorWithHexString:color].CGColor;
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}


@end
