//
//  YQViewFactory.m
//  GBKTrade
//
//  Created by yuqin on 2019/5/29.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import "YQViewFactory.h"

@implementation YQViewFactory

@end

@implementation YQViewFactory (UILabel)

+ (UILabel *)labelWithTextColor:(UIColor *)textColor
                  textAlignment:(NSTextAlignment)textAlignment
                       fontSize:(CGFloat)fontSize
                       userBold:(BOOL)userBold {
    UILabel * label = [[UILabel alloc] init];
    label.textColor = textColor;
    label.textAlignment = textAlignment;
    UIFont * font = nil;
    if (userBold) { font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize]; }
    else { font = [UIFont systemFontOfSize:fontSize]; }
    label.font = font;
    return label;
}

@end

@implementation YQViewFactory (UIButton)

+ (UIButton *)buttonWithTitle:(NSString *)title
                   titleColor:(UIColor *)titleColor
                     fontSize:(CGFloat)fontSize
                     userBold:(BOOL)userBold
                       target:(id)target sel:(SEL)sel
{
    return [self buttonWithTitle:title image:nil titleColor:titleColor fontSize:fontSize userBold:userBold target:target sel:sel];
}

+ (UIButton *)buttonWithImage:(UIImage *)image target:(id)target sel:(SEL)sel
{
    return [self buttonWithTitle:nil image:image titleColor:[UIColor whiteColor] fontSize:17.f userBold:NO target:target sel:sel];
}

+ (UIButton *)buttonWithTitle:(NSString *)title image:(UIImage *)image titleColor:(UIColor *)titleColor fontSize:(CGFloat)fontSize userBold:(BOOL)userBold target:(id)target sel:(SEL)sel
{
    UIButton * button = [[UIButton alloc] init];
    [button setImage:image forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
    UIFont * font = nil;
    if (userBold) { font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize]; }
    else { font = [UIFont systemFontOfSize:fontSize]; }
    button.titleLabel.font = font;
    if (target && [target respondsToSelector:sel])
    {
        [button addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    }
    return button;
}
@end

@implementation YQViewFactory  (UITextField)

+ (UITextField *)textFieldWithPlaceholderText:(NSString *)placeholerText textColor:(UIColor *)textColor fontSize:(CGFloat)fontSize userBold:(BOOL)userBold
{
    UITextField * textField = [[UITextField alloc] init];
    textField.placeholder = placeholerText;
    textField.textColor = textColor;
    UIFont * font = nil;
    if (userBold) { font = [UIFont fontWithName:@"Helvetica-Bold" size:fontSize]; }
    else { font = [UIFont systemFontOfSize:fontSize]; }
    textField.font = font;
    textField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0.f, 0.f, 15.f, 1.f)];
    textField.rightView = [[UIView alloc] initWithFrame:CGRectMake(0., 0.f, 15.f, 1.f)];
//    textField.layer.cornerRadius = 5.f;
//    textField.borderStyle = UITextBorderStyleRoundedRect;
    
    return textField;
}

@end

@implementation YQViewFactory (UIImageView)

+ (UIImageView *)imageViewWithImage:(UIImage *)image
{
    return [self imageViewWithImage:image target:nil sel:nil];
}

+ (UIImageView *)imageViewWithImage:(UIImage *)image target:(id)target sel:(SEL)sel
{
    UIImageView * imageView = [[UIImageView alloc] init];
    imageView.image = image;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    if (target && [target respondsToSelector:sel])
    {
        imageView.userInteractionEnabled = YES;
        UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:target action:sel];
        [imageView addGestureRecognizer:tap];
    }
    return imageView;
}

@end
