//
//  LimitTextFieldNum.h
//  GBKTrade
//
//  Created by admin on 2019/4/22.
//  Copyright © 2019 LionIT. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LimitTextFieldNum : NSObject

//判断密码是否是8-16的英文+数字结合
+(BOOL)judgePassWordLegal:(NSString *)pass;

+ (void)limitTextFieldContentNumWith:(UITextField *)textField withMaxNum:(NSInteger)kMaxLength;

//判断姓名格式是否正确
+ (BOOL)validateUserName:(NSString *)name;

//银行卡号是否正确
+ (BOOL)isBankCard:(NSString *)cardNumber;

//判断手机号码是否正确
+ (BOOL)validateCellPhoneNumber:(NSString *)cellNum;

@end

NS_ASSUME_NONNULL_END
