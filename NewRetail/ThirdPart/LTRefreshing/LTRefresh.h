//
//  LTRefresh.h
//  WeiYouDuoBao
//
//  Created by 高刘通 on 17/5/5.
//  Copyright © 2017年 LT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define LTREFRESH [LTRefresh shareRefresh]
#define HEADER_BEGIN_REFRESH [[LTRefresh shareRefresh] beginRefreshing];
#define HEADER_END_REFRESHING [[LTRefresh shareRefresh] headerEndRefreshing];
#define FOOTER_END_REFRESHING [[LTRefresh shareRefresh] footerEndRefreshing];
#define FOOTER_END_REFRESHINGWITHNOMOREDATA [[LTRefresh shareRefresh] footerEndRefreshingWithNoMoreData];

typedef NS_ENUM(NSUInteger, RefreshType) {
    RefreshTypeHeader, //只有下拉刷新
    RefreshTypeFooter, //只有上拉刷新样式1
    RefreshTypeNormalFooter, //只有上拉刷新样式2
    RefreshTypeHeaderAndFooter,//上下拉刷新样式1
    RefreshTypeHeaderAndNormalFooter//上下拉刷新样式2
};;

@interface LTRefresh : NSObject

+(instancetype)shareRefresh;

/**
 上下拉刷新调用方式

 @param scrollView scrollView
 @param refreshType 刷新类型
 @param headerRefresh 下拉回调
 @param footerRefresh 上拉回调
 */
-(void)refreshView:(UIScrollView *)scrollView refreshType:(RefreshType)refreshType headerRefreshBlock:(void(^)())headerRefresh footerRefreshBlock:(void(^)())footerRefresh;


/**
 开始刷新
 */
-(void)beginRefreshing;


/**
 下拉结束刷新
 */
-(void)headerEndRefreshing;



/**
 上拉结束刷新
 */
-(void)footerEndRefreshing;


/**
 上拉结束刷新 没有更多数据
 */
-(void)footerEndRefreshingWithNoMoreData;

@end
