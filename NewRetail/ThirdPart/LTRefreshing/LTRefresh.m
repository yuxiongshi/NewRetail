//
//  LTRefresh.m
//  WeiYouDuoBao
//
//  Created by 高刘通 on 17/5/5.
//  Copyright © 2017年 LT. All rights reserved.
//

#import "LTRefresh.h"
#import <MJRefresh/MJRefresh.h>

@implementation LTRefresh

static NSString * const scrollKey= @"scrollKey";
static NSString * const headerBlockKey= @"headerBlockKey";
static NSString * const footerBlockKey= @"footerBlockKey";
static LTRefresh *refresh = nil;

+(instancetype)shareRefresh{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        refresh = [[self alloc] init];
    });
    return refresh;
}

-(void)refreshView:(UIScrollView *)scrollView refreshType:(RefreshType)refreshType headerRefreshBlock:(void (^)())headerRefresh footerRefreshBlock:(void (^)())footerRefresh
{
    objc_setAssociatedObject(self, &scrollKey, scrollView, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, &headerBlockKey, headerRefresh, OBJC_ASSOCIATION_COPY);
    objc_setAssociatedObject(self, &footerBlockKey, footerRefresh, OBJC_ASSOCIATION_COPY);

    switch (refreshType) {
        case RefreshTypeHeader:
            [self headerRefreshMethod];
            break;
            
        case RefreshTypeFooter:
            [self footerRefreshMethod];
            break;
            
        case RefreshTypeNormalFooter:
            [self footerNomalRefreshMethod];
            break;
            
        case RefreshTypeHeaderAndFooter:
            [self headerRefreshMethod];
            [self footerRefreshMethod];
            break;
            
        case RefreshTypeHeaderAndNormalFooter:
            [self headerRefreshMethod];
            [self footerNomalRefreshMethod];
            break;
        default:break;
    }
}

-(void)headerRefreshMethod
{
    void (^headerBlock)() = ^{};
    headerBlock = objc_getAssociatedObject(self, &headerBlockKey);
    [self scrollView].mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (headerBlock)
        {
            headerBlock();
        }
    }];
}

-(void)footerRefreshMethod
{
    void (^footerBlock)() = ^{};
    footerBlock = objc_getAssociatedObject(self, &footerBlockKey);
    [self scrollView].mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (footerBlock)
        {
            footerBlock();
        }
    }];
}

-(void)footerNomalRefreshMethod
{
    void (^footerBlock)() = ^{};
    footerBlock = objc_getAssociatedObject(self, &footerBlockKey);
    [self scrollView].mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        if (footerBlock)
        {
            footerBlock();
        }
    }];
}

-(void)beginRefreshing
{
    [[self scrollView].mj_header beginRefreshing];
}

-(void)headerEndRefreshing
{
    [[self scrollView].mj_header endRefreshing];
}

-(void)footerEndRefreshing
{
    [[self scrollView].mj_footer endRefreshing];
}

-(void)footerEndRefreshingWithNoMoreData
{
    [[self scrollView].mj_footer endRefreshingWithNoMoreData];
}

-(UIScrollView *)scrollView
{
    return (UIScrollView *)objc_getAssociatedObject(self, &scrollKey);
}

@end
