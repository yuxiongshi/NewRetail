//
//  GuideViewController.m
//  RackProject
//
//  Created by Rain on 2019/1/24.
//  Copyright © 2019年 Rain. All rights reserved.
//

#define SCREEN_WIDTH                         [[UIScreen mainScreen] bounds].size.width
#define SCREEN_HEIGHT                        [[UIScreen mainScreen] bounds].size.height

#import "GuideViewController.h"

@interface GuideViewController ()<UIScrollViewDelegate>
@property(nonatomic,strong)UIScrollView *scrollView;
@property(nonatomic,strong)UIImageView *imageView;
@property(nonatomic,strong)UIPageControl *pageControl;

@end

@implementation GuideViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self constructUI];
    
}

- (void)constructUI
{
    self.view.backgroundColor = [UIColor whiteColor];
//    if (@available(iOS 11.0, *)) {
//        self.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        // Fallback on earlier versions
//    }
    NSArray *scrollArray;
    //判断机型
    if (HBISIphoneX) {
        //是iphoneX以上机型
        scrollArray = [NSArray arrayWithObjects:@"1", @"2",@"3",@"4", nil];
    }else {
        scrollArray = [NSArray arrayWithObjects:@"1-2", @"2-2",@"3-2",@"4-2", nil];
    }
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    self.scrollView.contentSize = CGSizeMake(SCREEN_WIDTH * scrollArray.count, SCREEN_HEIGHT );
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.bounces = NO;
    [self.view addSubview:self.scrollView];
    
    for (int i = 0; i < scrollArray.count; i++) {
        
        self.imageView = [[UIImageView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH * i, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
        self.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@",scrollArray[i]]];
//        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [self.scrollView addSubview:self.imageView];
        if (i == 3) {
            self.imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTitle:@"立即体验" forState:UIControlStateNormal];
            [button setTitleColor:[UIColor clearColor] forState:UIControlStateNormal];
//            button.layer.cornerRadius = 15;
            [button setBackgroundColor:[UIColor clearColor]];
            button.frame = CGRectMake(SCREEN_WIDTH / 2 - 60, SCREEN_HEIGHT - 90, FIT(120), FIT(70));
            [button addTarget:self action:@selector(actionEnd) forControlEvents:UIControlEventTouchUpInside];
            [self.imageView addSubview:button];
        }
        
    }

}

- (void)actionEnd
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"firstLaunch"];
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"everLaunch"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
//    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
//    TabBarController *tabVc = [[TabBarController alloc] init];
//    TheAppDel.rootTabBarController = tabVc;
//    delegate.window.rootViewController = tabVc;
    YQLoginVC *loginVC = [YQLoginVC new];
    GFNavigationController * loginNav = [[GFNavigationController alloc] initWithRootViewController:loginVC];
    AppDelegate *appDelegates = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appDelegates.window.rootViewController = loginNav;

}

#pragma mark ScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGPoint offset = scrollView.contentOffset;
    UIPageControl *page = (UIPageControl *)[self.view viewWithTag:1000];
    page.currentPage = offset.x / SCREEN_WIDTH;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
